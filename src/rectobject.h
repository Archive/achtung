/*
 * rectobject.h: Implementation of the rectangle object
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_RECTOBJECT_H
#define __ACHTUNG_RECTOBJECT_H

#include <libgnome/gnome-defs.h>

#include "reobject.h"

BEGIN_GNOME_DECLS

typedef struct _RectObject      RectObject;
typedef struct _RectObjectClass RectObjectClass;

#define RECTOBJECT_TYPE            (rectobject_get_type ())
#define RECTOBJECT(obj)            (GTK_CHECK_CAST((obj), RECTOBJECT_TYPE, \
				    RectObject))
#define RECTOBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                    RECTOBJECT_TYPE, RectObjectClass))
#define IS_RECTOBJECT(obj)         (GTK_CHECK_TYPE ((obj), RECTOBJECT_TYPE))
#define IS_RECTOBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                    RECTOBJECT_TYPE))

struct _RectObject {
	REObject parent_object;
};
	
struct _RectObjectClass {
	REObjectClass parent_class;
};

/* Standard object functions */
GtkType rectobject_get_type(void);

/* Create a new RectObject */
AchObject *rectobject_new(Slide *slide);

/* Destroy a RectObject */
void rectobject_destroy(GtkObject *object);

#endif
