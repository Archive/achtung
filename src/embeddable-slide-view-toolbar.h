/*
 * embeddable-slide-view-toolbar.h: Toolbar and Menu Item interface
 *				    for the EmbeddableSlideView class
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Mike Kestner, 
				and Helix Code, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@off.net>
 *	    Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __ACHTUNG_EMBEDDABLE_SLIDE_VIEW_TOOLBAR_H
#define __ACHTUNG_EMBEDDABLE_SLIDE_VIEW_TOOLBAR_H

#include "achobject.h"
#include "embeddable-slide-view.h"

void embeddable_slide_view_toolbar_unpress(EmbeddableSlideView *view);
void embeddable_slide_view_toolbar_attach_text_handlers(
	EmbeddableSlideView *view, CanvasObject *cobj);
void embeddable_slide_view_toolbar_update(EmbeddableSlideView *view,
					  AchObject *obj);

void embeddable_slide_view_create_ui_component(EmbeddableSlideView *view);

void embeddable_slide_view_update_verb_sensitivity(EmbeddableSlideView *view);

#endif
