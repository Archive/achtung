/*
 * achobject.h: Implementation of the base AchObject class
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Helix Code, Inc., 
 *                          the Puffin Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@linuxcare.com>
 *
 */

#ifndef __ACHTUNG_ACHOBJECT_H
#define __ACHTUNG_ACHOBJECT_H

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

typedef struct _AchObject         AchObject;
typedef struct _AchObjectClass    AchObjectClass;

#include "slide.h"

#define ACHOBJECT_TYPE            (achobject_get_type ())
#define ACHOBJECT(obj)            (GTK_CHECK_CAST((obj), ACHOBJECT_TYPE, \
				   AchObject))
#define ACHOBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                   ACHOBJECT_TYPE, AchObjectClass))
#define IS_ACHOBJECT(obj)         (GTK_CHECK_TYPE ((obj), ACHOBJECT_TYPE))
#define IS_ACHOBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                   ACHOBJECT_TYPE))

struct _AchObject {
	GtkObject parent_object;

	/* Coordinates */
	double x1, y1, x2, y2;

	/* Parent slide of the object */
	Slide *slide;

	/* Abstract method for clipboard duplication functions */
	AchObject *(* clone) (AchObject *achobject, Slide *slide);
};
	
struct _AchObjectClass {
	GtkObjectClass parent_class;

	/* Called when the object gets slide focus */
	void (* got_focus) (AchObject *achobject);
	/* Called when the object loses slide focus */
	void (* lost_focus) (AchObject *achobject);
};

/* Standard object functions */
GtkType achobject_get_type(void);

/* Create a new AchObject */
AchObject *achobject_new(Slide *slide);

/* Place the focus on an AchObject */
void achobject_focus(AchObject *obj);

/* Notify that the object has been changed */
void achobject_changed(AchObject *obj);

/* Set new dimensions for an AchObject */
void achobject_set_dimensions(AchObject *obj, double x1, double y1, double x2,
			      double y2);

/* Move an AchObject an incremental distance */
void achobject_move(AchObject *obj, gdouble dx, gdouble dy);

#endif
