/*
 * view.c: Implementation of the base View class
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */
#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "view.h"
#include "slideshow.h"
#include "slide-view.h"

static void view_class_init(ViewClass *klass);
static void view_init(GtkObject *object);

/* Callbacks */
static GtkWidget *view_activate(View *view);
static void view_deactivate(View *view);
static void view_got_focus(View *view);
static void view_lost_focus(View *view);
static void view_sensitize_menu_options(View *view);

static GtkObjectClass *parent_class;

GtkType
view_get_type(void)
{
	static GtkType view_type = 0;

	if (!view_type) {
		static const GtkTypeInfo view_info = {
			"View",
			sizeof(View),
			sizeof(ViewClass),
			(GtkClassInitFunc) view_class_init,
			(GtkObjectInitFunc) view_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		view_type = gtk_type_unique(gtk_object_get_type(),
					    &view_info);
	}

	return view_type;
} /* view_get_type */

/* Class initialization function for View */
static void
view_class_init(ViewClass *klass)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) klass;
	
	parent_class = gtk_type_class(gtk_object_get_type());

	object_class->destroy = view_destroy;
} /* view_class_init */

/* View intialization function */
static void
view_init(GtkObject *object)
{
	View *view;

	g_return_if_fail(object);

	view = VIEW(object);

	view->window = NULL;
	view->slideshow = NULL;
	view->current_slide = NULL;

	view->update = NULL;
	view->activate = view_activate;
	view->deactivate = view_deactivate;
	view->got_focus = view_got_focus;
	view->lost_focus = view_lost_focus;
	view->sensitize_menu_options = view_sensitize_menu_options;
} /* view_init */

View *
view_new(void)
{
	View *view;

	view = gtk_type_new(view_get_type());

	return view;
} /* view_new */

void
view_destroy(GtkObject *object)
{
	g_return_if_fail(object);

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy) (object);
} /* view_destroy */

void
view_set_slideshow(View *view, SlideShow *ss)
{
	view->slideshow = ss;
	view_set_slide_idx(VIEW(view), 0);
} /* view_set_slideshow */

void
view_set_slide(View *view, Slide *slide)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	g_return_if_fail(slide);

	if (view->current_slide)
		(* view->lost_focus) (view);

	view->current_slide = slide;

	(* view->got_focus) (view);

	window_sensitize_navigation_items(view->window);

	ACHTUNG_EXIT;
} /* view_set_slide */

void
view_set_slide_idx(View *view, gint idx)
{
	SlideShow *ss;
	Slide *slide;

	g_return_if_fail(view);
	ss = view->slideshow;
	g_return_if_fail(ss);

	slide = slideshow_get_slide(ss, idx);
	g_return_if_fail(slide);

	view_set_slide(view, slide);
} /* view_set_slide_idx */

void
view_remove_slide(Slide *slide, View *view)
{
	gint idx;
	SlideShow *ss;
	Slide *new_slide;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	g_return_if_fail(slide);
	g_return_if_fail(slide == view->current_slide);
	ss = view->slideshow;
	g_return_if_fail(ss);

	idx = slideshow_get_index(ss, slide);

	g_return_if_fail(idx >= 0);

	new_slide = slideshow_get_slide(ss, idx + 1);

	if(!new_slide && idx > 0)
		new_slide = slideshow_get_slide(ss, idx - 1);

	if(slide) {
		view_set_slide(view, new_slide);
	}

	ACHTUNG_EXIT;
}

static GtkWidget *
view_activate(View *view)
{
	g_warning("View base class doesn't have default behavior. View activated\n");
	
	return NULL;
} /* view_activate */

static void
view_deactivate(View *view)
{
	g_warning("View base class doesn't have default behavior. View deactivated.\n");
} /* view_deactivate */

static void
view_got_focus(View *view)
{
	g_warning("View base class doesn't have default behavior. View %p got focus\n", view);
} /* view_got_focus */

static void
view_lost_focus(View *view)
{
	g_warning("View base class doesn't have default behavior. View %p lost focus\n", view);
} /* view_lost_focus */

static void
view_sensitize_menu_options(View *view)
{
	g_warning("View base class doesn't have default behavior. Vieww %p is sensitizing menu options\n", view);
} /* view_sensitize_menu_options */
