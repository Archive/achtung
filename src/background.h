/*
 * background.h: Manages the slide's background, be it image or solid color.
 *
 * Copyright (C) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_BACKGROUND_H
#define __ACHTUNG_BACKGROUND_H

#include <libgnome/gnome-defs.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "embeddable-slide-view.h"

#include "slide.h"

void background_set_color(Slide *slide, guint32 color);
void background_set_image_center(Slide *slide, GdkPixbuf *pixbuf);
void background_set_image_scale(Slide *slide, GdkPixbuf *pixbuf);
void background_set_image_tile(Slide *slide, GdkPixbuf *pixbuf);
GnomeCanvasItem *background_canvas_item(EmbeddableSlideView *view, 
					GnomeCanvasGroup *group);
#endif /* __ACHTUNG_BACKGROUND_H */
