#ifndef __ACHTUNG_FILE_H
#define __ACHTUNG_FILE_H

#include <glib.h>

#include "slideshow.h"

#define FILE_LOAD 0
#define FILE_SAVE 1

typedef gboolean (*FileFormatProbe)(const char *filename);
typedef gboolean (*FileFormatOpen) (SlideShow *ss, const char *filename);
typedef gboolean (*FileFormatSave) (SlideShow *ss, const char *filename);

typedef struct _FileOpener FileOpener;
typedef struct _FileSaver FileSaver;
typedef struct _FileFormat FileFormat;

struct _FileFormat {
	int open_priority;
	char *format_description;
	char *extension;
	FileFormatProbe probe;
	FileFormatOpen open;
	FileFormatSave save;
};

void file_format_register(int priority, const char *desc, 
			  const char *extension, FileFormatProbe probe_fn,
			  FileFormatOpen open_fn, FileFormatSave save_fn,
			  gboolean is_default);
void file_format_unregister(FileFormatProbe probe, FileFormatOpen open,
			    FileFormatSave save);
gboolean slideshow_load_from_file(SlideShow *ss, const char *filename);
gboolean slideshow_save_to_file(SlideShow *ss, const char *filename);
char *file_selector(SlideShow *ss, const char *title, gboolean type);

#endif /* __ACHTUNG_FILE_H */
