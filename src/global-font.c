/* global-font.c: What will becoming global-font handling
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *
 */

#include <gnome.h>
#include <libgnomeprint/gnome-font.h>
#include "global-font.h"

GList *global_font_list = NULL;

int global_font_sizes[] = {
	4, 8, 9, 10, 11, 12, 14, 16, 18,
	20, 22, 24, 26, 28, 36, 48, 72,
	0
};

void
global_font_prepare_list(void)
{
	GList *fonts;

	fonts = gnome_font_family_list();

	while (fonts) {
		global_font_list = g_list_insert_sorted(global_font_list,
							g_strdup(fonts->data),
							(GCompareFunc) 
							g_strcasecmp);
		fonts = fonts->next;
	}
	
	gnome_font_family_list_free(fonts);
} /* global_font_prepare_list */

int
global_font_index(const char *font_name)
{
	GList *l;
	int index = 0;

	g_return_val_if_fail(font_name, -1);

	l = global_font_list;
	while (l) {
		if (g_strcasecmp(font_name, l->data) == 0)
			return index;
		index++;
		l = l->next;
	}

	return -1;
} /* global_font_index */
