/*
 * sort-view.h: Interface for the sort view
 *
 * Copyright (c) 2000 Joe Shaw, Mike Kestner and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 * 	   Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __ACHTUNG_SORTVIEW_H
#define __ACHTUNG_SORTVIEW_H

#include <libgnome/gnome-defs.h>
#include "idl/Achtung.h"

BEGIN_GNOME_DECLS

typedef struct _SortView         SortView;
typedef struct _SortViewClass    SortViewClass;

#include "view.h"

#define SORT_VIEW_TYPE            (sort_view_get_type ())
#define SORT_VIEW(obj)            (GTK_CHECK_CAST((obj), SORT_VIEW_TYPE, \
			           SortView))
#define SORT_VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                   SORT_VIEW_TYPE, SortViewClass))
#define IS_SORT_VIEW(obj)         (GTK_CHECK_TYPE ((obj), SORT_VIEW_TYPE))
#define IS_SORT_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                   SORT_VIEW_TYPE))

struct _SortView {
	View parent_view;

	BonoboClientSite 		*client_site;
	GNOME_Achtung_Slideshow 	server;
	GList				*minislides;

	GnomeCanvas 		*canvas;
	gint			cols;
	GnomeCanvasItem 	*highlight;

	GnomeCanvasItem 	*target;
	gint			target_idx;

	gboolean		deleting;
};

struct _SortViewClass {
	ViewClass parent_class;
};

/* Standard object functions */
GtkType sort_view_get_type(void);

/* Create a new NotesView */
SortView *sort_view_new(Window *win, Slide *slide);

#endif
