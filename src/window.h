/*
 * window.h: Implementation of the Window class
 *
 * Copyright (c) 2000 Phil Schwan and Linuxcare, Inc.
 *
 * Author: Phil Schwan <pschwan@linuxcare.com>
 *
 */

#ifndef __ACHTUNG_WINDOW_H
#define __ACHTUNG_WINDOW_H

#include <libgnome/gnome-defs.h>

#include "config.h"
#include <bonobo.h>

BEGIN_GNOME_DECLS

extern GList *open_windows;

typedef struct _Window          Window;
typedef struct _WindowClass     WindowClass;

#include "slide.h"
#include "view.h"

#define WINDOW_TYPE             (window_get_type ())
#define WINDOW(obj)             (GTK_CHECK_CAST((obj), WINDOW_TYPE, Window))
#define WINDOW_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), \
                                 WINDOW_TYPE, WindowClass))
#define IS_WINDOW(obj)          (GTK_CHECK_TYPE ((obj), WINDOW_TYPE))
#define IS_WINDOW_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), WINDOW_TYPE))

struct _Window {
	GtkObject parent_object;

	/* List of all views in the window */
	GList *views;

	/* The View with focus */
	View *active_view;

	/* Widgets and other private data */
	struct {
		GtkWidget *view_area;
		GtkWidget *statusbar;

		GList *panes;
	} priv;

	/* Bonobo resources */
	BonoboItemContainer 	*container;
	BonoboUIContainer 	*ui_container;
	BonoboUIComponent 	*ui_component;
	BonoboWindow 	  	*window;
};

struct _WindowClass {
	GtkObjectClass parent_class;
};

/* Standard object functions */
GtkType window_get_type(void);

Window *window_new(void);
void window_destroy(Window *window);

SlideShow * window_get_active_slideshow(Window *win);
View * window_get_active_view(Window *win);
void window_set_view(Window *win, View *view);
void window_remove_view(Window *win, View *view);
void window_set_statusbar(Window *win, const char *text);
void window_dialog_reparent(Window *win, GnomeDialog *dlg);
gint window_dialog_run(Window *win, GnomeDialog *dlg);
void window_sensitize_navigation_items(Window *win);
#endif
