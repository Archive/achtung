/*
 * lineobject.c: Implementation of the line and arrow objects
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "achobject.h"
#include "lineobject.h"
#include "slide.h"

static void lineobject_class_init(LineObjectClass *klass);
static void lineobject_init(GtkObject *object);
static void lineobject_set_arg(GtkObject *object, GtkArg *arg, guint arg_id);
static void lineobject_get_arg(GtkObject *object, GtkArg *arg, guint arg_id);

/* Object argument IDs */
enum {
	ARG_0,
	ARG_COLOR
};

static AchObjectClass *parent_class;

GtkType
lineobject_get_type(void)
{
	static GtkType lineobject_type = 0;

	if (!lineobject_type) {
		static const GtkTypeInfo lineobject_info = {
			"LineObject",
			sizeof(LineObject),
			sizeof(LineObjectClass),
			(GtkClassInitFunc) lineobject_class_init,
			(GtkObjectInitFunc) lineobject_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		lineobject_type = gtk_type_unique(achobject_get_type(),
						  &lineobject_info);
	}

	return lineobject_type;
} /* lineobject_get_type */

/* Class initialization function for LineObject */
static void
lineobject_class_init(LineObjectClass *klass)
{
	GtkObjectClass *object_class;
	AchObjectClass *achobject_class;

	object_class = (GtkObjectClass *) klass;
	achobject_class = (AchObjectClass *) klass;
	
	parent_class = gtk_type_class(achobject_get_type());

	gtk_object_add_arg_type("LineObject::color",
				GTK_TYPE_INT, GTK_ARG_READWRITE, ARG_COLOR);

	object_class->set_arg = lineobject_set_arg;
	object_class->get_arg = lineobject_get_arg;
} /* lineobject_class_init */

/* LineObject intialization function */
static void
lineobject_init(GtkObject *object)
{
	AchObject *aobj;
	LineObject *obj;

	g_return_if_fail(object);

	aobj = ACHOBJECT(object);
	obj = LINEOBJECT(object);

	aobj->x1 = 0.0;
	aobj->y1 = 0.0;
	aobj->x2 = 0.0;
	aobj->y2 = 0.0;
	aobj->slide = NULL;

	obj->color = ~0;
	obj->width = 1.0;
	obj->arrow = 0;
} /* lineobject_init */

AchObject *
lineobject_new(Slide *slide, gboolean is_arrow)
{
	LineObject *obj;

	g_return_val_if_fail(slide, NULL);
	g_return_val_if_fail(IS_SLIDE(slide), NULL);
	
	obj = gtk_type_new(lineobject_get_type());

	obj->arrow = is_arrow;
	ACHOBJECT(obj)->slide = slide;
	if (slide->object_added)
		(* slide->object_added) (slide, ACHOBJECT(obj));
	obj->color = slide->slideshow->defaults.line_color;
	achobject_changed(ACHOBJECT(obj));

	return ACHOBJECT(obj);
} /* lineobject_new */

static void
lineobject_set_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
	LineObject *obj;

	g_return_if_fail(object);
	g_return_if_fail(IS_LINEOBJECT(object));

	obj = LINEOBJECT(object);

	switch (arg_id) {
	case ARG_COLOR:
		obj->color = GTK_VALUE_INT(*arg);
		break;

	default:
		return;
	}

	achobject_changed(ACHOBJECT(obj));
} /* lineobject_set_arg */

static void
lineobject_get_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
	LineObject *obj;

	g_return_if_fail(object);
	g_return_if_fail(IS_LINEOBJECT(object));

	obj = LINEOBJECT(object);

	switch (arg_id) {
	case ARG_COLOR:
		GTK_VALUE_INT(*arg) = obj->color;
		break;

	default:
		arg->type = GTK_TYPE_INVALID;
		break;
	}
} /* lineobject_get_arg */
