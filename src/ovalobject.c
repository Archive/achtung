/*
 * ovalobject.c: Implementation of the oval object
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "achobject.h"
#include "reobject.h"
#include "ovalobject.h"
#include "slide.h"
#include "slideshow.h"

static void ovalobject_class_init(OvalObjectClass *klass);
static void ovalobject_init(GtkObject *object);

static REObjectClass *parent_class;

GtkType
ovalobject_get_type(void)
{
	static GtkType ovalobject_type = 0;

	if (!ovalobject_type) {
		static const GtkTypeInfo ovalobject_info = {
			"OvalObject",
			sizeof(OvalObject),
			sizeof(OvalObjectClass),
			(GtkClassInitFunc) ovalobject_class_init,
			(GtkObjectInitFunc) ovalobject_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		ovalobject_type = gtk_type_unique(reobject_get_type(),
						  &ovalobject_info);
	}

	return ovalobject_type;
} /* ovalobject_get_type */

/* Class initialization function for OvalObject */
static void
ovalobject_class_init(OvalObjectClass *klass)
{
	GtkObjectClass *object_class;
	AchObjectClass *achobject_class;
	REObjectClass *re_class;

	object_class = (GtkObjectClass *) klass;
	achobject_class = (AchObjectClass *) klass;
	re_class = (REObjectClass *) klass;
	
	parent_class = gtk_type_class(reobject_get_type());
} /* ovalobject_class_init */

/* OvalObject intialization function */
static void
ovalobject_init(GtkObject *object)
{
	AchObject *aobj;
	REObject *robj;

	g_return_if_fail(object);

	aobj = ACHOBJECT(object);
	robj = REOBJECT(object);

	aobj->x1 = 0.0;
	aobj->y1 = 0.0;
	aobj->x2 = 0.0;
	aobj->y2 = 0.0;
	aobj->slide = NULL;

	robj->fill_color = 0xff;
	robj->outline_color = 0xff;
} /* ovalobject_init */

AchObject *
ovalobject_new(Slide *slide)
{
	OvalObject *obj;

	g_return_val_if_fail(slide, NULL);
	g_return_val_if_fail(IS_SLIDE(slide), NULL);
	
	obj = gtk_type_new(ovalobject_get_type());

	ACHOBJECT(obj)->slide = slide;
	if (slide->object_added)
		(* slide->object_added) (slide, ACHOBJECT(obj));
	REOBJECT(obj)->fill_color = slide->slideshow->defaults.fill_color;
	REOBJECT(obj)->outline_color = slide->slideshow->defaults.line_color;

	achobject_changed(ACHOBJECT(obj));

	return ACHOBJECT(obj);
} /* ovalobject_new */
