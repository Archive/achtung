/*
 * imageobject.c: Implementation of the image object
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "achobject.h"
#include "imageobject.h"
#include "slide.h"

static void imageobject_class_init(ImageObjectClass *klass);
static void imageobject_init(GtkObject *object);
static void imageobject_set_arg(GtkObject *object, GtkArg *arg, guint arg_id);
static void imageobject_get_arg(GtkObject *object, GtkArg *arg, guint arg_id);

/* Object argument IDs */
enum {
	ARG_0,
	ARG_FILENAME,
	ARG_PIXBUF
};

static AchObjectClass *parent_class;

GtkType
imageobject_get_type(void)
{
	static GtkType imageobject_type = 0;

	if (!imageobject_type) {
		static const GtkTypeInfo imageobject_info = {
			"ImageObject",
			sizeof(ImageObject),
			sizeof(ImageObjectClass),
			(GtkClassInitFunc) imageobject_class_init,
			(GtkObjectInitFunc) imageobject_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		imageobject_type = gtk_type_unique(achobject_get_type(),
						  &imageobject_info);
	}

	return imageobject_type;
} /* imageobject_get_type */

/* Class initialization function for ImageObject */
static void
imageobject_class_init(ImageObjectClass *klass)
{
	GtkObjectClass *object_class;
	AchObjectClass *achobject_class;

	object_class = (GtkObjectClass *) klass;
	achobject_class = (AchObjectClass *) klass;
	
	parent_class = gtk_type_class(achobject_get_type());

	gtk_object_add_arg_type("ImageObject::filename",
				GTK_TYPE_STRING, GTK_ARG_READWRITE, 
				ARG_FILENAME);
	gtk_object_add_arg_type("ImageObject::pixbuf",
				GTK_TYPE_POINTER, GTK_ARG_READWRITE, 
				ARG_PIXBUF);

	object_class->set_arg = imageobject_set_arg;
	object_class->get_arg = imageobject_get_arg;
} /* imageobject_class_init */

/* ImageObject intialization function */
static void
imageobject_init(GtkObject *object)
{
	AchObject *aobj;
	ImageObject *obj;

	g_return_if_fail(object);

	aobj = ACHOBJECT(object);
	obj = IMAGEOBJECT(object);

	aobj->x1 = 0.0;
	aobj->y1 = 0.0;
	aobj->x2 = 0.0;
	aobj->y2 = 0.0;
	aobj->slide = NULL;

	obj->pixbuf = NULL;
} /* imageobject_init */

AchObject *
imageobject_new(Slide *slide)
{
	ImageObject *obj;

	g_return_val_if_fail(slide, NULL);
	g_return_val_if_fail(IS_SLIDE(slide), NULL);
	
	obj = gtk_type_new(imageobject_get_type());

	ACHOBJECT(obj)->slide = slide;
	if (slide->object_added)
		(* slide->object_added) (slide, ACHOBJECT(obj));
	achobject_changed(ACHOBJECT(obj));

	return ACHOBJECT(obj);
} /* imageobject_new */

AchObject *
imageobject_new_with_pixbuf(Slide *slide, GdkPixbuf *pixbuf)
{
	AchObject *obj;

	obj = imageobject_new(slide);
	IMAGEOBJECT(obj)->pixbuf = pixbuf;

	return obj;
} /* imageobject_new_with_pixbuf */

static void
imageobject_set_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
	ImageObject *obj;

	g_return_if_fail(object);
	g_return_if_fail(IS_IMAGEOBJECT(object));

	obj = IMAGEOBJECT(object);

	switch (arg_id) {
	case ARG_FILENAME:
		obj->filename = g_strdup(GTK_VALUE_STRING(*arg));
		break;

	case ARG_PIXBUF:
		obj->pixbuf = GTK_VALUE_POINTER(*arg);
		break;

	default:
		return;
	}

	achobject_changed(ACHOBJECT(obj));
} /* imageobject_set_arg */

static void
imageobject_get_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
	ImageObject *obj;

	g_return_if_fail(object);
	g_return_if_fail(IS_IMAGEOBJECT(object));

	obj = IMAGEOBJECT(object);

	switch (arg_id) {
	case ARG_FILENAME:
		GTK_VALUE_STRING(*arg) = g_strdup(obj->filename);
		break;

	case ARG_PIXBUF:
		GTK_VALUE_POINTER(*arg) = obj->pixbuf;
		break;

	default:
		arg->type = GTK_TYPE_INVALID;
		break;
	}
} /* imageobject_get_arg */
