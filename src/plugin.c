/*
 * plugin.c: Support for dynamically loaded Achtung plugin components.
 *
 * This file was unceremoniously ripped from Gnumeric and modified for
 * Achtung.
 *
 * Copyright (c) 1999 Tom Dyas
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Authors: Tom Dyas <tdyas@romulus.rutgers.edu>
 *          Joe Shaw <joe@helixcode.com>
 *
 */

#include <dirent.h>
#include <stdlib.h>
#include <string.h>

#include <libgnome/libgnome.h>
#include "achtung.h"
#include "plugin.h"

GList *plugin_list = NULL;

PluginData *
plugin_load(const gchar *filename)
{
	PluginData *pd;

	g_return_val_if_fail(filename != NULL, NULL);

	pd = g_new0(PluginData, 1);
	pd->handle = g_module_open(filename, 0);
	if (!pd->handle) {
		g_warning("%s: unable to load plugin: %s\n", filename, 
			  g_module_error());
		g_free(pd);
		return NULL;
	}

	if (!g_module_symbol(pd->handle, "init_plugin", 
			     (gpointer *) &pd->init_plugin)) {
		g_warning("%s: plugin must contain init_plugin function\n",
			  filename);
		g_module_close(pd->handle);
		g_free(pd);
		return NULL;
	}
	
	if (pd->init_plugin(pd) < 0) {
		g_warning("%s: init_plugin returned error", filename);
		g_module_close(pd->handle);
		g_free(pd);
		return NULL;
	}

	plugin_list = g_list_append(plugin_list, pd);
	return pd;
} /* plugin_load */

void
plugin_unload (PluginData *pd)
{
	g_return_if_fail (pd != NULL);
        
        if (pd->can_unload && !pd->can_unload(pd)) {
		g_warning("Plugin still in use\n");
                return;
        }
        
        if (pd->cleanup_plugin)
                pd->cleanup_plugin(pd);
	
	plugin_list = g_list_remove(plugin_list, pd);
	g_module_close(pd->handle);
	g_free (pd);
} /* plugin_unload */

gboolean
plugin_supported(void)
{
	return g_module_supported();
} /* plugin_supported */

static void
plugin_load_plugins_in_dir(char *directory)
{
	DIR *d;
	struct dirent *e;

	if ((d = opendir(directory)) == NULL) {
		g_warning("Unable to open directory \"%s\"", directory);
		return;
	}

	while ((e = readdir(d)) != NULL) {
		char *p;

		if ((p = strrchr (e->d_name, '.')) &&
		    !strcmp (p, ".so")) {
			char *plugin_name;
			plugin_name = g_strconcat(directory, e->d_name, NULL);
			plugin_load(plugin_name);
			g_free(plugin_name);
		}
	}
	closedir(d);
} /* plugin_load_plugins_in_dir */

static void
load_all_plugins(void)
{
	char *plugin_dir;
	char *home_dir = g_get_home_dir ();

	/* Load the user plugins */
	plugin_dir = g_strconcat(home_dir ? home_dir : "", 
				 "/.achtung/plugins/", NULL);
	plugin_load_plugins_in_dir(plugin_dir);
	g_free(plugin_dir);
	g_free(home_dir);

	/* Load the system plugins */
	plugin_dir = gnome_unconditional_libdir_file("achtung/plugins/");
	plugin_load_plugins_in_dir(plugin_dir);
	g_free(plugin_dir);
} /* load_all_plugins */

void
plugins_init(void)
{
	if (!plugin_supported())
		return;

	load_all_plugins();
} /* plugins_init */
