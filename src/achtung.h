/*
 * achtung.h: Base header file with application-wide stuff
 *
 * Copyright (c) 1999, 2000, Joe Shaw, Phil Schwan, Helix Code, Inc.,
 *                           the Puffin Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@linuxcare.com>
 *
 */

#ifndef __ACHTUNG_ACHTUNG_H
#define __ACHTUNG_ACHTUNG_H

#define DEFAULT_FONT "Helvetica"
#define DEFAULT_FONT_SIZE 12.0

#define SCROLL_HEIGHT 1200
#define SCROLL_WIDTH 1600
#define MAX_ZOOM 250

#define DEBUG 6

#if DEBUG > 5
#  define ACHTUNG_ENTRY printf("==> %s (%s, line %d)\n", __FUNCTION__,	\
			       __FILE__, __LINE__);
#  define ACHTUNG_EXIT printf("<== %s (%s, line %d)\n", __FUNCTION__,	\
			      __FILE__, __LINE__);
#else
#  define ACHTUNG_ENTRY ;
#  define ACHTUNG_EXIT ;
#endif

#endif /* __ACHTUNG_ACHTUNG_H */
