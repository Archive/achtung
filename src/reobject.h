/*
 * reobject.h: Implementation of the rectangle/ellipse base class.
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_REOBJECT_H
#define __ACHTUNG_REOBJECT_H

#include <libgnome/gnome-defs.h>

#include "achobject.h"

BEGIN_GNOME_DECLS

typedef struct _REObject      REObject;
typedef struct _REObjectClass REObjectClass;

#define REOBJECT_TYPE            (reobject_get_type ())
#define REOBJECT(obj)            (GTK_CHECK_CAST((obj), REOBJECT_TYPE, \
				  REObject))
#define REOBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                  REOBJECT_TYPE, REObjectClass))
#define IS_REOBJECT(obj)         (GTK_CHECK_TYPE ((obj), REOBJECT_TYPE))
#define IS_REOBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                  REOBJECT_TYPE))

struct _REObject {
	AchObject parent_object;

	/* Color of the object */
	guint32 fill_color;
	guint32 outline_color;
};

struct _REObjectClass {
	AchObjectClass parent_class;
};

/* Standard object functions */
GtkType reobject_get_type(void);

#endif
