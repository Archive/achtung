/*
 * achobject.c: Implementation of the base AchObject class
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Helix Code, Inc., 
 *                          the Puffin Group, Inc, and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@off.net>
 *
 */

#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "achobject.h"
#include "slide.h"
#include "slideshow.h"

static void achobject_class_init(AchObjectClass *klass);
static void achobject_init(GtkObject *object);
static void achobject_set_arg(GtkObject *object, GtkArg *arg, guint arg_id);
static void achobject_get_arg(GtkObject *object, GtkArg *arg, guint arg_id);
static void achobject_lost_focus(AchObject *obj);
static void achobject_got_focus(AchObject *obj);
static void achobject_destroy(GtkObject *object);

/* Object argument IDs */
enum {
	ARG_0,
	ARG_X1,
	ARG_Y1,
	ARG_X2,
	ARG_Y2
};

static GtkObjectClass *parent_class;

GtkType
achobject_get_type(void)
{
	static GtkType achobject_type = 0;

	if (!achobject_type) {
		static const GtkTypeInfo achobject_info = {
			"AchObject",
			sizeof(AchObject),
			sizeof(AchObjectClass),
			(GtkClassInitFunc) achobject_class_init,
			(GtkObjectInitFunc) achobject_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		achobject_type = gtk_type_unique(gtk_object_get_type(),
						 &achobject_info);
	}

	return achobject_type;
} /* achobject_get_type */

/* Class initialization function for AchObject */
static void
achobject_class_init(AchObjectClass *klass)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) klass;
	
	parent_class = gtk_type_class(gtk_object_get_type());

	gtk_object_add_arg_type("AchObject::x1",
				GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X1);
	gtk_object_add_arg_type("AchObject::y1",
				GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y1);
	gtk_object_add_arg_type("AchObject::x2",
				GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X2);
	gtk_object_add_arg_type("AchObject::y2",
				GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y2);

	object_class->destroy = achobject_destroy;
	object_class->set_arg = achobject_set_arg;
	object_class->get_arg = achobject_get_arg;
	klass->got_focus = achobject_got_focus;
	klass->lost_focus = achobject_lost_focus;
} /* achobject_class_init */

/* AchObject intialization function */
static void
achobject_init(GtkObject *object)
{
	AchObject *obj;

	g_return_if_fail(object);

	obj = ACHOBJECT(object);

	obj->x1 = obj->y1 = obj->x2 = obj->y2 = 0.0;
	obj->slide = NULL;

	obj->clone = NULL;
} /* achobject_init */

void
achobject_changed(AchObject *obj)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(obj);
	g_return_if_fail(IS_ACHOBJECT(obj));

	slide_achobject_changed(obj);

	ACHTUNG_EXIT;
} /* achobject_changed */

AchObject *
achobject_new(Slide *slide)
{
	AchObject *obj;

	g_return_val_if_fail(slide, NULL);
	g_return_val_if_fail(IS_SLIDE(slide), NULL);

	obj = gtk_type_new(achobject_get_type());

	obj->slide = slide;
	if (slide->object_added)
		(* slide->object_added) (slide, obj);
	achobject_changed(obj);

	return obj;
} /* achobject_new */

static void
achobject_destroy(GtkObject *object)
{
	AchObject *obj;

	ACHTUNG_ENTRY;

	g_return_if_fail(object);
	g_return_if_fail(IS_ACHOBJECT(object));

	obj = ACHOBJECT(object);

	if (obj->slide && obj->slide->object_removed)
		(* obj->slide->object_removed) (obj->slide, ACHOBJECT(obj));

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy) (object);

	ACHTUNG_EXIT;
} /* achobject_destroy */

static void
achobject_set_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
	AchObject *obj;

	g_return_if_fail(object);
	g_return_if_fail(IS_ACHOBJECT(object));

	obj = ACHOBJECT(object);

	switch (arg_id) {
	case ARG_X1:
		obj->x1 = GTK_VALUE_DOUBLE(*arg);
		break;

	case ARG_Y1:
		obj->y1 = GTK_VALUE_DOUBLE(*arg);
		break;

	case ARG_X2:
		obj->x2 = GTK_VALUE_DOUBLE(*arg);
		break;

	case ARG_Y2:
		obj->y2 = GTK_VALUE_DOUBLE(*arg);
		break;

	default:
		return;
	}

	achobject_changed(obj);
} /* achobject_set_arg */

static void
achobject_get_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
	AchObject *obj;

	g_return_if_fail(object);
	g_return_if_fail(IS_ACHOBJECT(object));

	obj = ACHOBJECT(object);

	switch (arg_id) {
	case ARG_X1:
		GTK_VALUE_DOUBLE(*arg) = obj->x1;
		break;

	case ARG_Y1:
		GTK_VALUE_DOUBLE(*arg) = obj->y1;
		break;

	case ARG_X2:
		GTK_VALUE_DOUBLE(*arg) = obj->x2;
		break;

	case ARG_Y2:
		GTK_VALUE_DOUBLE(*arg) = obj->y2;
		break;

	default:
		arg->type = GTK_TYPE_INVALID;
		break;
	}
} /* achobject_get_arg */

/* object-side routines for losing focus */
static void
achobject_lost_focus(AchObject *obj)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(obj);
	g_return_if_fail(obj->slide);

	achobject_changed(obj);

	ACHTUNG_EXIT;
} /* achobject_lost_focus */

/* object-side routines for receiving focus */
static void
achobject_got_focus(AchObject *obj)
{
 	ACHTUNG_ENTRY;

	g_return_if_fail(obj);
	g_return_if_fail(obj->slide);

	achobject_changed(obj);

	ACHTUNG_EXIT;
} /* achobject_got_focus */

void 
achobject_focus(AchObject *obj)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(obj);
	g_return_if_fail(obj->slide);

	if (obj->slide->current_item)
		slide_unfocus(obj->slide);

	slide_set_current_item(obj->slide, obj);
	if (ACHOBJECT_CLASS(GTK_OBJECT(obj)->klass)->got_focus)
		(* ACHOBJECT_CLASS(GTK_OBJECT(obj)->klass)->got_focus) (obj);

	ACHTUNG_EXIT;
} /* achobject_focus */

void
achobject_set_dimensions(AchObject *obj, double x1, double y1, double x2,
			 double y2)
{
	g_return_if_fail(obj);

	obj->x1 = x1;
	obj->y1 = y1;
	obj->x2 = x2;
	obj->y2 = y2;

	achobject_changed(obj);
} /* achobject_set_dimensions */

void
achobject_move(AchObject *obj, gdouble dx, gdouble dy)
{
	g_return_if_fail(obj);

	obj->x1 += dx;
	obj->y1 += dy;
	obj->x2 += dx;
	obj->y2 += dy;

	achobject_changed(obj);
} /* achobject_set_dimensions */
