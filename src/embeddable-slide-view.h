/*
 * embeddable-slide-view.h: Implementation of the individual slide view
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, the Puffin Group, Inc., and
 *                          Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@off.net>
 *          Phil Schwan <pschwan@off.net>
 *          Mike Kestner <mkestner@ameritech.net>
 */

#ifndef __ACHTUNG_EMBEDDABLE_SLIDE_VIEW_H
#define __ACHTUNG_EMBEDDABLE_SLIDE_VIEW_H

#include <libgnome/gnome-defs.h>
#include <bonobo.h>
#include <gdraw/shapes-layer.h>
#include "achtung-embeddable.h"

BEGIN_GNOME_DECLS

typedef struct _EmbeddableSlideView      EmbeddableSlideView;
typedef struct _EmbeddableSlideViewClass EmbeddableSlideViewClass;
typedef struct _CanvasObject             CanvasObject;
typedef enum   _EmbeddableSlideViewMode      	 EmbeddableSlideViewMode;

#include "achobject.h"

#define EMBEDDABLE_SLIDE_VIEW_TYPE            (embeddable_slide_view_get_type ())
#define EMBEDDABLE_SLIDE_VIEW(obj)            (GTK_CHECK_CAST((obj), EMBEDDABLE_SLIDE_VIEW_TYPE, EmbeddableSlideView))
#define EMBEDDABLE_SLIDE_VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), EMBEDDABLE_SLIDE_VIEW_TYPE, EmbeddableSlideViewClass))
#define IS_EMBEDDABLE_SLIDE_VIEW(obj)         (GTK_CHECK_TYPE ((obj), EMBEDDABLE_SLIDE_VIEW_TYPE))
#define IS_EMBEDDABLE_SLIDE_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), EMBEDDABLE_SLIDE_VIEW_TYPE))

struct _EmbeddableSlideView {
	BonoboXObject	base;

	Slide 		*slide;

	gboolean activated;

	GtkWidget *table;
	GtkWidget *hscroll;
	GtkWidget *vscroll;
	GtkWidget *hruler;
	GtkWidget *vruler;

	GnomeCanvas *canvas;
	GnomeCanvasItem *background;

	GDrawShapesLayer *shapes_layer;
	GDrawShapesLayer *master_layer;

	/* Zoom factor for the current view (in percent) */
	int zoom_factor;
	gboolean auto_zoom;

	/* Presentation Mode indicator */
	gboolean present_mode;

	/* Bonobo stuff */
	BonoboView 		*bview;
	BonoboItemContainer 	*container;
	BonoboUIComponent 	*ui_component;
	Bonobo_UIContainer 	corba_uic;
};
	
struct _EmbeddableSlideViewClass {
	BonoboXObjectClass parent_class;

	POA_GNOME_Achtung_Slide__epv epv;
};

/* Standard object functions */
GtkType embeddable_slide_view_get_type(void);

BonoboView *embeddable_slide_view_factory(BonoboEmbeddable *, 
					  const Bonobo_ViewFrame, 
					  SlideShow *);

EmbeddableSlideView *embeddable_slide_view_new(Slide *slide);

void embeddable_slide_view_set_slide(EmbeddableSlideView *view, Slide *slide);

void embeddable_slide_view_zoom(EmbeddableSlideView *view, int percent);
void embeddable_slide_view_auto_zoom(EmbeddableSlideView *view);

/* Background event handler (called from slide-view-popup.c) */
gboolean background_handle_event(GnomeCanvasItem *background, GdkEvent *event,
				 EmbeddableSlideView *view);
#endif
