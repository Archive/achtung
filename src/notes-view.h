/*
 * notes-view.h: Implementation of the notes view
 *
 * Copyright (c) 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_NOTES_VIEW_H
#define __ACHTUNG_NOTES_VIEW_H

#include <libgnome/gnome-defs.h>
#include "idl/Achtung.h"

BEGIN_GNOME_DECLS

typedef struct _NotesView         NotesView;
typedef struct _NotesViewClass    NotesViewClass;

#include "view.h"

#define NOTES_VIEW_TYPE            (notes_view_get_type ())
#define NOTES_VIEW(obj)            (GTK_CHECK_CAST((obj), NOTES_VIEW_TYPE, \
			            NotesView))
#define NOTES_VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                    NOTES_VIEW_TYPE, NotesViewClass))
#define IS_NOTES_VIEW(obj)         (GTK_CHECK_TYPE ((obj), NOTES_VIEW_TYPE))
#define IS_NOTES_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                    NOTES_VIEW_TYPE))

struct _NotesView {
	View parent_view;

	BonoboClientSite 		*client_site;
	GNOME_Achtung_Slideshow 	server;
	GNOME_Achtung_Slide 		corba_view;

	GtkWidget *vbox;
	GtkWidget *slide_wrapper;
	GtkWidget *text_box;
};

struct _NotesViewClass {
	ViewClass parent_class;
};

/* Standard object functions */
GtkType notes_view_get_type(void);

/* Create a new NotesView */
NotesView *notes_view_new(Window *win, Slide *slide);
#endif
