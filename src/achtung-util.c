/*
 * achtung-util.c: Various miscellaneous utility functions.
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "achobject.h"
#include "achtung-util.h"

AchObject *cut_obj = NULL;

guint32
achtung_color_to_rgba(GnomeCanvas *canvas, char *name)
{
	GdkColor color;
	guint32 rgba;

	ACHTUNG_ENTRY;

	g_return_val_if_fail(canvas, 0);
	g_return_val_if_fail(name, 0);

	gnome_canvas_get_color(canvas, name, &color);
	rgba = ((color.red & 0xff00) << 16 |
		(color.green & 0xff00) << 8 |
		(color.blue & 0xff00) |
		0xff);

	ACHTUNG_EXIT;

	return rgba;
} /* achtung_color_to_rgba */

guint32
achtung_gdkcolor_to_rgba(GdkColor *color)
{
	guint32 rgba;

	ACHTUNG_ENTRY;

	rgba = ((color->red & 0xff00) << 16 |
		(color->green & 0xff00) << 8 |
		(color->blue & 0xff00) |
		0xff);

	ACHTUNG_EXIT;

	return rgba;
} /* achtung_gdkcolor_to_rgba */

GdkColor *
achtung_rgba_to_gdkcolor(guint32 rgba)
{
	GdkColor color;

	color.red = (rgba >> 16) | 0xff00;
	color.green = (rgba >> 8) | 0xff00;
	color.blue = rgba | 0xff00;

	return gdk_color_copy(&color);
} /* achtung_rgba_to_gdkcolor */

gchar *
achtung_rgba_to_string(guint32 rgba)
{
	return g_strdup_printf("rgb:%04x/%04x/%04x",
			       (rgba >> 16) | 0xff00,
			       (rgba >> 8) | 0xff00,
			       rgba | 0xff00);
} /* achtung_rgba_to_string */

void
achtung_set_clipboard_object(AchObject *obj)
{
	ACHTUNG_ENTRY;

	if (cut_obj)
		gtk_object_unref(GTK_OBJECT(cut_obj));
	cut_obj = obj;
	if (obj)
		gtk_object_ref(GTK_OBJECT(cut_obj));

	ACHTUNG_EXIT;
}  /* achtung_set_clipboard_object */

AchObject *
achtung_get_clipboard_object(void)
{
	return cut_obj;
} /* achtung_get_clipboard_object */
