/*
 * menu.c: Menu and main toolbar handling routines
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Helix Code, Inc., the Puffin
 *                          Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@off.net>
 *	    Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <bonobo.h>
#include <gnome.h>
#include <glade/glade.h>
#include "achtung.h"
#include "dialogs.h"
#include "embeddable-slide-view.h"
#include "file.h"
#include "global-font.h"
#include "menu.h"
#include "present-view.h"
#include "slide.h"
#include "slideshow.h"
#include "slide-view.h"
#include "sort-view.h"
#include "notes-view.h"
#include "view.h"
#include "window.h"

static void
new_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	SlideShow *ss;
	SlideView *view;
	Slide *slide;
	Window *new_win;
	Window *win = WINDOW(user_data);

	if(window_get_active_view(win)) {
		new_win = window_new();
	} else {
		new_win = win;
	}

	ss = slideshow_new(NULL);
	slide = SLIDE(ss->slides->data);
	view = slide_view_new(new_win, slide);
	window_set_view(new_win, VIEW(view));
} /* new_cmd */

static void
open_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	SlideShow *ss = slideshow_open();
	Window *win = WINDOW(user_data);

	if (ss) {
		Window *new_win;
		SlideView *view;

		if(window_get_active_view(win)) {
			new_win = window_new();
		} else {
			new_win = win;
		}
		if ((view = slide_view_new(new_win, SLIDE(ss->slides->data)))) {
			window_set_view(new_win, VIEW(view));
		}
	}
} /* open_cmd */

static void
save_as_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	char *filename;
	Window *win = WINDOW(user_data);
	SlideShow *ss = window_get_active_slideshow(win);

	filename = file_selector(ss, "Save Slideshow As", FILE_SAVE);
	if (filename)
		slideshow_save_to_file(ss, filename);
} /* save_as_cmd */

static void
save_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	SlideShow *ss = window_get_active_slideshow(win);

	g_return_if_fail(ss);

	if (ss->filename)
		slideshow_save_to_file(ss, ss->filename);
	else
		save_as_cmd(uic, user_data, cname);
} /* save_cmd */

static void 
page_setup_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);

	dialog_page_setup(win);
} /* page_setup_cmd */

static void
close_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	View *view;
	Window *win = WINDOW(user_data);

	g_return_if_fail(win);
	view = window_get_active_view(win);
	g_return_if_fail(view);

	(* view->deactivate)(view);
	gtk_object_unref(GTK_OBJECT(view->slideshow));
	window_destroy(win);

} /* close_cmd */

static void
exit_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	/* window_destroy() (which gtk_object_destroy indirectly calls),
	 * mangles the open_windows list; do not attempt to traverse it */
	while (open_windows) {
		View *view;
		Window *win = WINDOW(open_windows->data);
		g_return_if_fail(win);

		view = window_get_active_view(win);

		if(view) {
			(* view->deactivate)(view);
			gtk_object_unref(GTK_OBJECT(view->slideshow));
		}

		window_destroy(win);
	}

	if(open_slideshows)
		return;

	gtk_main_quit();
} /* exit_cmd */

static void
prev_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	int index;
	Window *win = WINDOW(user_data);
	View *view = window_get_active_view(win);
	SlideShow *ss;

	g_return_if_fail(view);
	ss = view->slideshow;
	g_return_if_fail(ss);

	index = slideshow_get_index(ss, view->current_slide);
	view_set_slide_idx(view, index - 1);
} /* prev_cmd */

static void
next_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	int index;
	Window *win = WINDOW(user_data);
	View *view = window_get_active_view(win);
	SlideShow *ss;

	g_return_if_fail(view);
	ss = view->slideshow;
	g_return_if_fail(ss);

	index = slideshow_get_index(ss, view->current_slide);
	view_set_slide_idx(view, index + 1);
} /* next_cmd */

static void
first_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	View *view = window_get_active_view(win);
	SlideShow *ss;

	g_return_if_fail(view);
	ss = view->slideshow;
	g_return_if_fail(ss);

	view_set_slide_idx(view, 0);
} /* first_cmd */

static void
last_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	int index;
	Window *win = WINDOW(user_data);
	View *view = window_get_active_view(win);
	SlideShow *ss;

	g_return_if_fail(view);
	ss = view->slideshow;
	g_return_if_fail(ss);

	index = g_list_length(ss->slides) - 1;
	view_set_slide_idx(view, index);
} /* last_cmd */

static void
insert_slide_before_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	View *view = window_get_active_view(win);
	SlideShow *ss;
	Slide *slide;

	g_return_if_fail(view);
	ss = view->slideshow;
	g_return_if_fail(ss);

	slide = slide_new();
	slideshow_insert(
		ss, slide, slideshow_get_index(ss, view->current_slide));
	view_set_slide(view, slide);
} /* insert_slide_before_cmd */

static void
insert_slide_after_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	View *view = window_get_active_view(win);
	SlideShow *ss;
	Slide *slide;

	g_return_if_fail(view);
	ss = view->slideshow;
	g_return_if_fail(ss);

	slide = slide_new();
	slideshow_insert(
		ss, slide, slideshow_get_index(ss, view->current_slide) + 1);
	view_set_slide(view, slide);
} /* insert_slide_after_cmd */

static void
append_slide_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	View *view = window_get_active_view(win);
	SlideShow *ss;
	Slide *slide;

	g_return_if_fail(view);
	ss = view->slideshow;
	g_return_if_fail(ss);

	slide = slide_new();
	slideshow_append(ss, slide);
	view_set_slide(view, slide);
} /* append_slide_cmd */

static void
remove_slide_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	View *view = window_get_active_view(win);
	SlideShow *ss;

	g_return_if_fail(view);
	ss = view->slideshow;
	g_return_if_fail(ss);

	slideshow_remove(ss, view->current_slide);
	window_sensitize_navigation_items(view->window);
} /* remove_slide_cmd */

static void
slideview_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	View *oldview = window_get_active_view(win);
	SlideView *view;

	g_return_if_fail(oldview);

	ACHTUNG_ENTRY;

	view = slide_view_new(win, oldview->current_slide);

	window_remove_view(win, oldview);

	window_set_view(win, VIEW(view));

	ACHTUNG_EXIT;
} /* slideview_cmd */

static void
slidesort_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	View *oldview = window_get_active_view(win);
	SortView *view;

	g_return_if_fail(oldview);

	ACHTUNG_ENTRY;

	view = sort_view_new(win, oldview->current_slide);

	window_remove_view(win, oldview);

	window_set_view(win, VIEW(view));

	ACHTUNG_EXIT;
} /* slidesort_cmd */

static void
notes_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	View *oldview = window_get_active_view(win);
	NotesView *view;

	ACHTUNG_ENTRY;

	g_return_if_fail(oldview);

	view = notes_view_new(win, oldview->current_slide);

	window_remove_view(win, oldview);

	window_set_view(win, VIEW(view));

	ACHTUNG_EXIT;
} /* notes_cmd */

static void
present_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	View *old_view = window_get_active_view(win);
	PresentView *view;

	ACHTUNG_ENTRY;

	view = present_view_new(win, old_view->current_slide);

	window_remove_view(win, old_view);

	window_set_view(win, VIEW(view));

	ACHTUNG_EXIT;
} /* present_cmd */

static void
master_slide_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);
	View *view = window_get_active_view(win);
	static int previous_slide = -1;

	/* The previous_slide variable stores the last slide displayed
	   before the user clicks on the Master Slide menu option. As always,
	   -1 means the master slide. So, if you are on slide 6, when you
	   click on Master Slide, 6 (well, technically 5, but I digress) is
	   stored in the variable. When it is clicked again, the previous
	   slide is set and -1 is stored again */

	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	g_return_if_fail(view->slideshow);

	if (previous_slide == -1) {
		previous_slide = slideshow_get_index(
			view->slideshow, view->current_slide);
		view_set_slide_idx(view, -1);
	} else {
		view_set_slide_idx(view, previous_slide);
		previous_slide = -1;
	}
} /* master_slide_cmd */

static void
change_font_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
} /* change_font_cmd */

static void
options_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	Window *win = WINDOW(user_data);

	dialog_options(win);
}

static void
about_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	GtkWidget *aboutbox;

	const gchar *authors[] = {
		"Joe Shaw <joe@ximian.com>",
		"Phil Schwan <pschwan@linuxcare.com>",
		"Mike Kestner <mkestner@ameritech.net>",
		NULL
	};

	aboutbox = gnome_about_new(
		_("Achtung"),
		VERSION,
		_("Copyright (C) 1999-2001"),
		(const char **) authors,
		_("The GNOME Office Presentation Application"),
		NULL);
	gtk_widget_show(aboutbox);
} /* about_cmd */

static void
bold_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
} /* bold_cmd */

static void
italic_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
} /* italic_cmd */

static void
underline_cmd(BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
} /* underline_cmd */

BonoboUIVerb win_verbs [] = {
        BONOBO_UI_VERB ("FileNew", new_cmd),
        BONOBO_UI_VERB ("FileOpen", open_cmd),
        BONOBO_UI_VERB ("FileSave", save_cmd),
        BONOBO_UI_VERB ("FileSaveAs", save_as_cmd),
        BONOBO_UI_VERB ("FilePageSetup", page_setup_cmd),
        BONOBO_UI_VERB ("FileClose", close_cmd),
        BONOBO_UI_VERB ("FileExit", exit_cmd),
        BONOBO_UI_VERB ("ViewSlide", slideview_cmd),
        BONOBO_UI_VERB ("ViewNotes", notes_cmd),
        BONOBO_UI_VERB ("ViewSort", slidesort_cmd),
        BONOBO_UI_VERB ("ViewPresent", present_cmd),
        BONOBO_UI_VERB ("ViewMasterSlide", master_slide_cmd),
        BONOBO_UI_VERB ("SlidePrev", prev_cmd),
        BONOBO_UI_VERB ("SlideNext", next_cmd),
        BONOBO_UI_VERB ("SlideFirst", first_cmd),
        BONOBO_UI_VERB ("SlideLast", last_cmd),
        BONOBO_UI_VERB ("SlideInsertBefore", insert_slide_before_cmd),
        BONOBO_UI_VERB ("SlideInsertAfter", insert_slide_after_cmd),
        BONOBO_UI_VERB ("SlideAppend", append_slide_cmd),
        BONOBO_UI_VERB ("SlideRemove", remove_slide_cmd),
        BONOBO_UI_VERB ("ToolsOptions", options_cmd),
        BONOBO_UI_VERB ("HelpAbout", about_cmd),
        BONOBO_UI_VERB_END
};

gchar win_cmd_xml[] =
	"<commands>\n"
        "	<cmd name=\"FileNew\" _tip=\"Create a new presentation\" pixtype=\"stock\" pixname=\"New\"/>\n"
        "	<cmd name=\"FileOpen\" _tip=\"Open an existing presentation\" pixtype=\"stock\" pixname=\"Open\" accel=\"F3\"/>\n"
        "	<cmd name=\"FileSave\" _tip=\"Save the current presentation\" pixtype=\"stock\" pixname=\"Save\" accel=\"*Control*s\"/>\n"
        "	<cmd name=\"FileSaveAs\" _tip=\"Save the current presentation with a different name\" pixtype=\"stock\" pixname=\"Save As\"/>\n"
        "	<cmd name=\"FilePageSetup\" _tip=\"Set the Page dimensions\"/>\n"
        "	<cmd name=\"FileClose\" _label=\"Close\" _tip=\"Close the current presentation\" pixtype=\"stock\" pixname=\"Close\" accel=\"*Control*w\"/>\n"
        "	<cmd name=\"FileExit\" _tip=\"Exit the application\" pixtype=\"stock\" pixname=\"Exit\" accel=\"*Control*q\"/>\n"
        "	<cmd name=\"ViewSlide\" _label=\"Slide View\" _tip=\"Individual slide view\"/>\n"
        "	<cmd name=\"ViewNotes\" _label=\"Slide Notes\" _tip=\"Notes relating to the slide\"/>\n"
        "	<cmd name=\"ViewSort\" _label=\"Slide Sorter\" _tip=\"Slide sorting view\"/>\n"
        "	<cmd name=\"ViewPresent\" _label=\"Presentation\" _tip=\"Present slideshow\"/>\n"
        "	<cmd name=\"SlidePrev\" _tip=\"Previous Slide\" pixtype=\"stock\" pixname=\"Back\" accel=\"*Control*p\"/>\n"
        "	<cmd name=\"SlideNext\" _tip=\"Next Slide\" pixtype=\"stock\" pixname=\"Forward\" accel=\"*Control*n\"/>\n"
        "	<cmd name=\"SlideFirst\" _tip=\"First Slide\" pixtype=\"stock\" pixname=\"First\" accel=\"Home\"/>\n"
        "	<cmd name=\"SlideLast\" _tip=\"Last Slide\" pixtype=\"stock\" pixname=\"Last\" accel=\"End\"/>\n"
        "	<cmd name=\"SlideInsertBefore\" _tip=\"Insert Slide Before\"/>\n"
        "	<cmd name=\"SlideInsertAfter\" _tip=\"Insert Slide After\"/>\n"
        "	<cmd name=\"SlideAppend\" _tip=\"Append Slide\"/>\n"
        "	<cmd name=\"SlideRemove\" _tip=\"Remove Slide\"/>\n"
        "	<cmd name=\"SlideMaster\" _tip=\"Master Slide View\"/>\n"
        "	<cmd name=\"ToolsOptions\" _tip=\"Change the application options\"/>\n"
        "	<cmd name=\"HelpAbout\" _tip=\"About Achtung\" pixtype=\"stock\" pixname=\"About\"/>\n"
	"</commands>\n";

gchar win_menu_xml[] =
	"<menu>\n"
	"	<submenu name=\"File\" _label=\"_File\">\n"
	"		<menuitem name=\"FileNew\" _label=\"_New\" verb=\"\"/>\n"
	"		<menuitem name=\"FileOpen\" _label=\"_Open\" verb=\"\"/>\n"
	"		<menuitem name=\"FileSave\" _label=\"_Save\" verb=\"\"/>\n"
	"		<menuitem name=\"FileSaveAs\" _label=\"Save _As\" verb=\"\"/>\n"
	"		<separator/>"
	"		<menuitem name=\"FilePageSetup\" _label=\"Page Setup\" verb=\"\"/>\n"
	"		<separator/>"
	"		<menuitem name=\"FileClose\" _label=\"_Close\" verb=\"\"/>\n"
	"		<menuitem name=\"FileExit\" _label=\"E_xit\" verb=\"\"/>\n"
	"	</submenu>\n"
	"	<submenu name=\"Edit\" _label=\"_Edit\">\n"
	"	</submenu>\n"
	"	<submenu name=\"View\" _label=\"_View\">\n"
	"		<menuitem name=\"ViewSlide\" _label=\"_Slide View\" verb=\"\"/>\n"
	"		<menuitem name=\"ViewNotes\" _label=\"_Notes View\" verb=\"\"/>\n"
	"		<menuitem name=\"ViewSort\" _label=\"So_rt View\" verb=\"\"/>\n"
	"		<menuitem name=\"ViewPresent\" _label=\"_Presentation\" verb=\"\"/>\n"
	"	</submenu>\n"
	"	<submenu name=\"Slide\" _label=\"_Slide\">\n"
	"		<menuitem name=\"SlidePrev\" _label=\"_Previous\" verb=\"\"/>\n"
	"		<menuitem name=\"SlideNext\" _label=\"_Next\" verb=\"\"/>\n"
	"		<menuitem name=\"SlideFirst\" _label=\"_First\" verb=\"\"/>\n"
	"		<menuitem name=\"SlideLast\" _label=\"_Last\" verb=\"\"/>\n"
	"		<separator/>"
	"		<menuitem name=\"SlideInsertBefore\" _label=\"Insert _Before\" verb=\"\"/>\n"
	"		<menuitem name=\"SlideInsertAfter\" _label=\"_Insert After\" verb=\"\"/>\n"
	"		<menuitem name=\"SlideAppend\" _label=\"_Append\" verb=\"\"/>\n"
	"		<menuitem name=\"SlideRemove\" _label=\"_Remove\" verb=\"\"/>\n"
	"		<separator/>"
	"		<menuitem name=\"SlideMaster\" _label=\"_Master Slide\" verb=\"\"/>\n"
	"	</submenu>\n"
	"	<submenu name=\"Tools\" _label=\"_Tools\">\n"
	"		<menuitem name=\"ToolOptions\" _label=\"_Options\" verb=\"\"/>\n"
	"	</submenu>\n"
	"	<submenu name=\"Help\" _label=\"_Help\">\n"
	"		<menuitem name=\"HelpAbout\" _label=\"_About\" verb=\"\"/>\n"
	"	</submenu>\n"
	"</menu>\n";

gchar win_toolbar_xml[] =
	"<dockitem name=\"Toolbar\" _tip=\"Main toolbar\">"
	"	<toolitem name=\"FileNew\" _label=\"New\" verb=\"\"/>"
	"	<toolitem name=\"FileOpen\" _label=\"Open\" verb=\"\"/>"
	"	<toolitem name=\"FileSave\" _label=\"Save\" verb=\"\"/>"
	"	<separator/>"
	"	<toolitem name=\"SlidePrev\" _label=\"Prev\" verb=\"\"/>"
	"	<toolitem name=\"SlideNext\" _label=\"Next\" verb=\"\"/>"
	"	<separator/>"
	"	<toolitem name=\"SlideInsertAfter\" _label=\"Insert\" verb=\"\"/>"
	"</dockitem>\n";

BonoboUIComponent *
window_ui_component_new(Window *win)
{
	Bonobo_UIContainer corba_container;
	BonoboUIComponent *comp;
	gchar *comp_name;
	static gint count = 0;

	ACHTUNG_ENTRY;

	comp_name = g_strdup_printf("window%d", count++);
	comp = bonobo_ui_component_new(comp_name);
	g_free(comp_name);

	corba_container = bonobo_object_corba_objref(
					BONOBO_OBJECT(win->ui_container));
	bonobo_ui_component_set_container(comp, corba_container);

	printf("cmds\n");
	bonobo_ui_component_set_translate(comp, "/", win_cmd_xml, NULL);
	printf("menu\n");
	bonobo_ui_component_set_translate(comp, "/", win_menu_xml, NULL);
	printf("toolbar\n");
	bonobo_ui_component_set_translate(comp, "/", win_toolbar_xml, NULL);

	bonobo_ui_component_add_verb_list_with_data(comp, win_verbs, win);

	ACHTUNG_EXIT;

	return comp;
}

void
window_ui_component_set_cmd_sensitivity(Window *win, gboolean sens)
{
	BonoboUIComponent *uic = win->ui_component;
	gchar *val = (sens ? "1" : "0");

	ACHTUNG_ENTRY;

	/* File menu */
	bonobo_ui_component_set_prop(uic, "/commands/FileSave", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/FileSaveAs", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/FilePageSetup", "sensitive", val, NULL);
	
	/* View menu */
	bonobo_ui_component_set_prop(uic, "/commands/ViewSlide", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/ViewNotes", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/ViewSort", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/ViewPresent", "sensitive", val, NULL);

	/* Slide menu */
	bonobo_ui_component_set_prop(uic, "/commands/SlidePrev", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/SlideNext", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/SlideFirst", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/SlideLast", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/SlideInsertBefore", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/SlideInsertAfter", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/SlideAppend", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/SlideRemove", "sensitive", val, NULL);
	bonobo_ui_component_set_prop(uic, "/commands/SlideMaster", "sensitive", val, NULL);

	ACHTUNG_EXIT;
}

#if 0
static GnomeUIInfo format_toolbar[] = {
	/* Font selector */
	/* Size label */
	/* Size entry */
	{ GNOME_APP_UI_TOGGLEITEM, N_("Bold"), N_("Makes the font bold"),
	  bold_cmd, NULL, NULL, GNOME_APP_PIXMAP_STOCK, 
	  GNOME_STOCK_PIXMAP_TEXT_BOLD },
	{ GNOME_APP_UI_TOGGLEITEM, N_("Italic"), N_("Sets the font italic"),
	  italic_cmd, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
	  GNOME_STOCK_PIXMAP_TEXT_ITALIC },
	{ GNOME_APP_UI_TOGGLEITEM, N_("Underline"), N_("Underlines the font"),
	  underline_cmd, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
	  GNOME_STOCK_PIXMAP_TEXT_UNDERLINE },
	/* Zoom label */
	/* Zoom entry */
	/* Color picker */
	GNOMEUIINFO_END
};

void
toolbar_init(Window *win)
{
	BonoboUIHandlerToolbarItem *toolbar_list;

	bonobo_ui_handler_create_toolbar(win->uih, "MainToolbar");
	toolbar_list = bonobo_ui_handler_toolbar_parse_uiinfo_list_with_data(
		main_toolbar, win);
	bonobo_ui_handler_toolbar_add_list(win->uih, "/MainToolbar",
					   toolbar_list);
	bonobo_ui_handler_toolbar_free_list(toolbar_list);

	/* Create Format Toolbar */
	tb = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_ICONS);
	uibdata.connect_func = _do_ui_signal_connect;
	uibdata.data = win;
	uibdata.is_interp = FALSE;
	uibdata.relay_func = NULL;
	uibdata.destroy_func = NULL;
	gnome_app_fill_toolbar_custom(
		GTK_TOOLBAR(tb), format_toolbar, &uibdata, 
		GNOME_APP(win->priv.toplevel)->accel_group);
	gnome_app_add_toolbar(GNOME_APP(win->priv.toplevel), GTK_TOOLBAR(tb),
			      "FormatToolbar", GNOME_DOCK_ITEM_BEH_NORMAL,
			      GNOME_DOCK_TOP, 2, 0, 0);

	/* Add font selector to toolbar */
	menu = gtk_menu_new();
	w = gtk_menu_item_new_with_label("");
	gtk_widget_show(w);
	gtk_menu_append(GTK_MENU(menu), w);

	f = global_font_list;
	i = index = 1;
	while (f) {
		w = gtk_menu_item_new_with_label(f->data);
		gtk_widget_show(w);
		gtk_menu_append(GTK_MENU(menu), w);
		gtk_signal_connect(GTK_OBJECT(w), "activate", 
				   GTK_SIGNAL_FUNC(font_changed), win);
		gtk_object_set_user_data(GTK_OBJECT(w), f->data);
		if (g_strcasecmp(DEFAULT_FONT, f->data) == 0)
			index = i;

		f = f->next;
		i++;
	}
	gtk_menu_set_active(GTK_MENU(menu), index);
	win->priv.font_menu = gtk_option_menu_new();
	gtk_container_set_border_width(GTK_CONTAINER(win->priv.font_menu), 0);
	gtk_option_menu_set_menu(GTK_OPTION_MENU(win->priv.font_menu), menu);
	gtk_widget_show(win->priv.font_menu);
	gtk_toolbar_insert_widget(GTK_TOOLBAR(tb), win->priv.font_menu, 
				  _("Font selector"), NULL, 0);

	/* Add size label to toolbar */
	w = gtk_label_new("Size:");
	gtk_widget_set_usize(w, gdk_string_measure(w->style->font, "Size:")+6,
			     0);
	gtk_widget_show(w);
	gtk_toolbar_insert_widget(GTK_TOOLBAR(tb), w, NULL, NULL, 1);

	/* Add size entry to toolbar */
	win->size_entry = gtk_entry_new_with_max_length(4);
	gtk_widget_set_usize(
		win->priv.size_entry, 
		gdk_string_measure(
			win->priv.size_entry->style->font, "0000")+4, 0);
	gtk_entry_set_text(GTK_ENTRY(win->priv.size_entry),
			   g_strdup_printf("%d", (int) DEFAULT_FONT_SIZE));
	gtk_widget_show(win->priv.size_entry);
	gtk_signal_connect(GTK_OBJECT(win->priv.size_entry), "activate",
			   GTK_SIGNAL_FUNC(font_size_changed), win);
	/* To steal away the focus from the canvas, so as not to interfere
	   with typing in a text box */
	gtk_signal_connect(GTK_OBJECT(win->priv.size_entry), 
			   "button-press-event",
			   GTK_SIGNAL_FUNC(_steal_focus), win);

	gtk_toolbar_insert_widget(GTK_TOOLBAR(tb), win->priv.size_entry,
				  _("Size"), NULL, 2);

	/* Add zoom label to toolbar */
	w = gtk_label_new("Zoom:");
	gtk_widget_set_usize(w, gdk_string_measure(w->style->font, "Zoom:")+6,
			     0);
	gtk_widget_show(w);
	gtk_toolbar_insert_widget(GTK_TOOLBAR(tb), w, NULL, NULL, 6);
	
	/* Add zoom entry to toolbar */
	adj = GTK_ADJUSTMENT(gtk_adjustment_new(100.0, 0.0, 250.0, 10.0, 50.0, 
						250.0));
	win->priv.zoom_entry = gtk_spin_button_new(adj, 25.0, 0);
	gtk_spin_button_set_update_policy(
		GTK_SPIN_BUTTON(win->priv.zoom_entry),GTK_UPDATE_IF_VALID);
	gtk_spin_button_set_numeric(
		GTK_SPIN_BUTTON(win->priv.zoom_entry), TRUE);
	gtk_widget_show(win->priv.zoom_entry);
	gtk_signal_connect(GTK_OBJECT(win->priv.zoom_entry), "activate",
			   GTK_SIGNAL_FUNC(zoom_changed), win);

	/* To steal away the focus from the canvas, so as not to interfere
	   with typing in a text box */
	gtk_signal_connect(GTK_OBJECT(win->priv.zoom_entry),
			   "button-press-event",
			   GTK_SIGNAL_FUNC(_steal_focus), win);

	gtk_toolbar_insert_widget(GTK_TOOLBAR(tb), win->priv.zoom_entry,
				  _("Zoom factor"), NULL, 7);

	/* Add color selector to toolbar */
	win->priv.color_selector =color_combo_new((const char **) bucket_xpm);
	color_combo_select_color(COLOR_COMBO(win->priv.color_selector), 1);
	gtk_widget_show(win->priv.color_selector);
	gtk_signal_connect(GTK_OBJECT(win->priv.color_selector), "changed",
			   GTK_SIGNAL_FUNC(color_changed), win);
	gtk_toolbar_insert_widget(GTK_TOOLBAR(tb), win->priv.color_selector,
				  _("Color"), _("Color"), 8);
} /* toolbar_init */

void
toolbar_set_sensitive(Window *win, int toolbar, int item, gboolean sensitive)
{
	GnomeUIInfo uii;

	switch (toolbar) {
	case MAIN_TOOLBAR:
		uii = main_toolbar[item];
		break;
	case FORMAT_TOOLBAR:
		uii = format_toolbar[item];
		break;
	default:
		g_assert_not_reached();
		break;
	}

	gtk_widget_set_sensitive(uii.widget, sensitive);
} /* toolbar_set_sensitive */
#endif
