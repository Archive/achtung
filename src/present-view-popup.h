/*
 * present-view-popup.h: Popup menu routines for the presentation
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_PRESENT_VIEW_POPUP_H
#define __ACHTUNG_PRESENT_VIEW_POPUP_H

gboolean popup_present_menu(GdkEvent *event, PresentView *view, int index, 
			    int length);

#endif /* __ACHTUNG_PRESENT_VIEW_POPUP_H */
