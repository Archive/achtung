/*
 * popup.h: General routines for popup menus
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_POPUP_H
#define __ACHTUNG_POPUP_H

void popup_add_menu_item(GnomeUIInfo *menu, GnomeUIInfo menu_data, 
			 gboolean reset);
GnomeUIInfo popup_item_stock(char *label, char *tooltip, gpointer callback,
			     gpointer stock_item);
GnomeUIInfo popup_item(char *label, char *tooltop, gpointer callback,
		       gpointer xpm_data);
GnomeUIInfo popup_submenu(char *label, GnomeUIInfo *submenu);
GnomeUIInfo popup_separator(void);
GnomeUIInfo popup_end(void);

#endif /* __ACHTUNG_POPUP_H */
