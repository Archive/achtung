/*
 * achtung-main.c: Initialization routines for Achtung
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include "achtung.h"
#include "achtung-options.h"
#include "achtung-popt.h"
#include "dialogs.h"
#include "global-font.h"
#include "plugin.h"
#include "slideshow.h"
#include "slide-view.h"
#include "view.h"
#include "achtung-embeddable.h"

#include <liboaf/liboaf.h>


poptContext ctx;
extern gboolean server_only;

struct poptOption oaf_options[] = {
        { NULL, '\0', POPT_ARG_INCLUDE_TABLE, &options, 
	  0, NULL, NULL },
        { NULL, '\0', POPT_ARG_INCLUDE_TABLE, &oaf_popt_options, 
	  0, NULL, NULL },
        { NULL, '\0', 0, NULL, 0, NULL, NULL }
};

static void 
achtung_corba_args(int argc, char *argv[])
{
	CORBA_Environment ev;
	CORBA_ORB orb;

	ACHTUNG_ENTRY;

	CORBA_exception_init(&ev);

	gnome_init_with_popt_table("Achtung", VERSION, argc, argv,
				   oaf_options, 0, NULL);
	orb = oaf_init(argc, argv);

	if (!bonobo_init(orb, NULL, NULL))
		g_error("Cannot initialize bonobo\n");

	ACHTUNG_EXIT;
} /* achtung_corba_args */

int 
main(int argc, char *argv[])
{
        bindtextdomain (PACKAGE, GNOMELOCALEDIR);                                                   
        textdomain (PACKAGE);

	achtung_corba_args(argc, argv);
	glade_gnome_init();
	gdk_rgb_init();
	if (!plugin_supported())
		g_warning("Plugins not supported\n");
	else
		plugins_init();

	achtung_load_options();
	global_font_prepare_list();
	
	bonobo_activate();

	achtung_embeddable_factory_init();

	if (!server_only) {
		Window *win;

		win = window_new();
		dialog_startup(win);
	}

	gtk_main();

	return 0;
} /* main */
