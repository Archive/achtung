/*
 * slide-view-popup.c: Popup menu routines 
 *
 * Copyright (c) 1999, 2000 Phil Schwan, Joe Shaw, the Puffin Group, Inc.,
 *                          Linuxcare, Inc., and Helix Code, Inc.
 *
 * Authors: Phil Schwan <pschwan@off.net>
 *          Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_SLIDE_VIEW_POPUP_H
#define __ACHTUNG_SLIDE_VIEW_POPUP_H

#include <glib.h>
#include <gdk/gdk.h>

#include "embeddable-slide-view.h"

gboolean popup_achobject_menu(GdkEvent *event, CanvasObject *obj);
gboolean popup_canvasroot_menu(GdkEvent *event, EmbeddableSlideView *view);

#endif /* __ACHTUNG_SLIDE_VIEW_POPUP_H */
