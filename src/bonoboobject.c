/*
 * bonoboobject.c: AchObject implementation for Bonobo objects
 *
 * Copyright (c) 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "achobject.h"
#include "bonoboobject.h"
#include "slide.h"

static void bonobject_class_init(BonObjectClass *klass);
static void bonobject_init(GtkObject *object);

static AchObjectClass *parent_class;

GtkType
bonobject_get_type(void)
{
	static GtkType bonobject_type = 0;

	if (!bonobject_type) {
		static const GtkTypeInfo bonobject_info = {
			"BonObject",
			sizeof(BonObject),
			sizeof(BonObjectClass),
			(GtkClassInitFunc) bonobject_class_init,
			(GtkObjectInitFunc) bonobject_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		bonobject_type = gtk_type_unique(
			achobject_get_type(), &bonobject_info);
	}

	return bonobject_type;
} /* bonobject_get_type */

/* Class initialization function for BonObject */
static void
bonobject_class_init(BonObjectClass *klass)
{
	GtkObjectClass *object_class;
	AchObjectClass *achobject_class;

	object_class = (GtkObjectClass *) klass;
	achobject_class = (AchObjectClass *) klass;
	
	parent_class = gtk_type_class(achobject_get_type());
} /* bonobject_class_init */

/* BonObject intialization function */
static void
bonobject_init(GtkObject *object)
{
	AchObject *aobj;
	BonObject *obj;

	g_return_if_fail(object);

	aobj = ACHOBJECT(object);
	obj = BONOBJECT(object);

	aobj->x1 = 0.0;
	aobj->y1 = 0.0;
	aobj->x2 = 0.0;
	aobj->y2 = 0.0;
	aobj->slide = NULL;

	obj->client_site = NULL;
} /* bonobject_init */

AchObject *
bonobject_new(Slide *slide, char *id, BonoboItemContainer *container)
{
	BonObject *obj;
	static gint index = 0;
	gchar *name;

	g_return_val_if_fail(slide, NULL);
	g_return_val_if_fail(IS_SLIDE(slide), NULL);

	obj = gtk_type_new(bonobject_get_type());

	obj->id = g_strdup(id);
	obj->client_site = bonobo_client_site_new(container);
	obj->server = bonobo_object_activate(obj->id, 0);

	if (!obj->server)
		return NULL;

	if (!bonobo_client_site_bind_embeddable(
		obj->client_site, obj->server)) {
		bonobo_object_unref(BONOBO_OBJECT(obj->server));
		return NULL;
	}

	name = g_strdup_printf("bonobject%d", index++);
	bonobo_item_container_add(container, 
				  name,
				  BONOBO_OBJECT(obj->client_site));
	g_free(name);

	ACHOBJECT(obj)->slide = slide;
	if (slide->object_added)
		(* slide->object_added) (slide, ACHOBJECT(obj));
	achobject_changed(ACHOBJECT(obj));

	return ACHOBJECT(obj);
} /* bonobject_new */
