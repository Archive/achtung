/*
 * notes-view.c: Implementation of the NotesView class
 *
 * Copyright (c) 2000 Joe Shaw, Mike Kestner, and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *	   Mike Kestner <mkestner@ameritech.net>
 *
 */
#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include "achtung.h"
#include "achtung-embeddable.h"
#include "notes-view.h"
#include "slideshow.h"

static ViewClass *parent_class;

static void
notes_view_destroy(GtkObject *object)
{
	NotesView *view;
	CORBA_Environment ev;
	BonoboViewFrame *vf;

	g_return_if_fail(object);
	g_return_if_fail(IS_NOTES_VIEW(object));

	ACHTUNG_ENTRY;

	view = NOTES_VIEW(object);

	CORBA_exception_init(&ev);

	vf = BONOBO_VIEW_FRAME(view->client_site->view_frames->data);
	Bonobo_Unknown_unref(bonobo_view_frame_get_view(vf), &ev);

	gtk_object_destroy(GTK_OBJECT(view->vbox));

	bonobo_object_unref(BONOBO_OBJECT(view->client_site));

	Bonobo_Unknown_unref(view->server, &ev);

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy) (object);

	ACHTUNG_EXIT;
} 

static void
notes_view_class_init(NotesViewClass *klass)
{
	GtkObjectClass *object_class;
	ViewClass *view_class;

	object_class = (GtkObjectClass *) klass;
	view_class = (ViewClass *) klass;
	
	parent_class = gtk_type_class(view_get_type());

	object_class->destroy = notes_view_destroy;
} 

static GtkWidget *
notes_view_activate(View *view)
{
	ACHTUNG_ENTRY;

	g_return_val_if_fail(view, NULL);

	ACHTUNG_EXIT;
	return view->view_widget;
} 

static void
notes_view_deactivate(View *view)
{
	NotesView *nview;
	Slide *slide;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	slide = view->current_slide;
	g_return_if_fail(slide);

	nview = NOTES_VIEW(view);

	g_free(slide->notes);

	slide->notes = gtk_editable_get_chars(
		GTK_EDITABLE(nview->text_box), 0, -1);

	notes_view_destroy(GTK_OBJECT(view));

	ACHTUNG_EXIT;
} 

static void
notes_view_got_focus(View *view)
{
	NotesView *nview;
	Slide *slide;
	int pos = 0;
	int idx;
	CORBA_Environment ev;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	g_return_if_fail(IS_NOTES_VIEW(view));

	nview = NOTES_VIEW(view);
	slide = view->current_slide;
	g_return_if_fail(slide);
	g_return_if_fail(nview->text_box);

	if (slide->notes) {
		gtk_text_freeze(GTK_TEXT(nview->text_box));
		gtk_editable_delete_text(GTK_EDITABLE(nview->text_box), 0, -1);
		gtk_editable_insert_text(GTK_EDITABLE(nview->text_box), 
					 slide->notes, strlen(slide->notes), 
					 &pos);
		gtk_text_thaw(GTK_TEXT(nview->text_box));
	}

	idx = slideshow_get_index(slide->slideshow, slide);

	CORBA_exception_init(&ev);
	GNOME_Achtung_Slide_setSlide(NOTES_VIEW(view)->corba_view, 
				     idx, &ev);

	if(ev._major != CORBA_NO_EXCEPTION) {
		printf("Exception while setting slide in notes view.\n");
		if(ev._repo_id) {
			printf("repo_id=%s\n", ev._repo_id);
		} else {
			printf("Null repo_id.");
		}
	}

	gtk_signal_connect(GTK_OBJECT(slide), "slide_removed",
		GTK_SIGNAL_FUNC(view_remove_slide), view);

	ACHTUNG_EXIT;
} 

static void
notes_view_lost_focus(View *view)
{
	NotesView *nview;
	Slide *slide;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	g_return_if_fail(IS_NOTES_VIEW(view));

	slide = view->current_slide;
	g_return_if_fail(slide);

	nview = NOTES_VIEW(view);

	g_free(slide->notes);

	slide->notes = g_strdup(gtk_editable_get_chars(
				GTK_EDITABLE(nview->text_box), 0, -1));

	gtk_signal_disconnect_by_func(GTK_OBJECT(view->current_slide),
		GTK_SIGNAL_FUNC(view_remove_slide), view);

	ACHTUNG_EXIT;
} 

static void
notes_view_init(GtkObject *object)
{
	NotesView *notes_view;
	View *view;

	g_return_if_fail(object);

	notes_view = NOTES_VIEW(object);
	view = VIEW(object);

	notes_view->text_box = NULL;
	notes_view->slide_wrapper = NULL;
	notes_view->vbox = NULL;
	notes_view->client_site = NULL;
	notes_view->server = NULL;

	view->update = NULL;
	view->activate = notes_view_activate;
	view->deactivate = notes_view_deactivate;
	view->got_focus = notes_view_got_focus;
	view->lost_focus = notes_view_lost_focus;
} 

GtkType
notes_view_get_type(void)
{
	static GtkType notes_view_type = 0;

	if (!notes_view_type) {
		static const GtkTypeInfo notes_view_info = {
			"NotesView",
			sizeof(NotesView),
			sizeof(NotesViewClass),
			(GtkClassInitFunc) notes_view_class_init,
			(GtkObjectInitFunc) notes_view_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		notes_view_type = gtk_type_unique(view_get_type(),
						  &notes_view_info);
	}

	return notes_view_type;
} 

static void
client_site_sys_exception_cb(BonoboObject *client, CORBA_Object cobject,
			     CORBA_Environment *ev, NotesView *view)
{
	gtk_object_unref(GTK_OBJECT(view));
}

static gboolean
notes_view_embed_slide(NotesView *view, Window *win)
{
	BonoboObjectClient *client;
	gchar *cs_name;
	static gint count = 0;

	g_return_val_if_fail(view, FALSE);
	g_return_val_if_fail(win, FALSE);

	ACHTUNG_ENTRY;

	view->client_site = bonobo_client_site_new(win->container);

	view->server = bonobo_object_corba_objref(BONOBO_OBJECT(
			achtung_embeddable_new(VIEW(view)->slideshow)));

	client = bonobo_object_client_from_corba(view->server);

	if(client == NULL) {
		gtk_object_unref(GTK_OBJECT(view));
		return FALSE;
	}

	if(!bonobo_client_site_bind_embeddable(view->client_site, client)) {
		gtk_object_unref(GTK_OBJECT(view));
		return FALSE;
	}

	cs_name = g_strdup_printf("Notes%d", count++);
	bonobo_item_container_add(win->container, cs_name,
				  BONOBO_OBJECT(view->client_site));
	g_free(cs_name);

	gtk_signal_connect(GTK_OBJECT(view->client_site), "system_exception",
			   GTK_SIGNAL_FUNC(client_site_sys_exception_cb), view);

	ACHTUNG_EXIT;

	return TRUE;
}

static void
view_frame_sys_exception_cb(BonoboViewFrame *view_frame, CORBA_Object cobject,
 			    CORBA_Environment *ev, NotesView *view)
{
	gtk_object_unref(GTK_OBJECT(view));
}

static void
user_activate_request_cb(BonoboViewFrame *view_frame, NotesView *view)
{
	bonobo_view_frame_view_activate(view_frame);
}

static void
view_frame_activated_cb (BonoboViewFrame *view_frame, gboolean activated, 
			 NotesView *view)
{
	bonobo_view_frame_set_covered(view_frame, !activated);
}

static gboolean
notes_view_create_view_frame(NotesView *view, BonoboUIContainer *uic)
{
	BonoboViewFrame *view_frame;
	GtkWidget *wrapper;
	View *v;

	g_return_val_if_fail(view, FALSE);

	ACHTUNG_ENTRY;

	v = VIEW(view);

	view_frame = bonobo_client_site_new_view(view->client_site,
			bonobo_object_corba_objref(BONOBO_OBJECT(uic)));

	g_return_val_if_fail(view_frame, FALSE);

	gtk_signal_connect(GTK_OBJECT(view_frame), "system_exception",
			   GTK_SIGNAL_FUNC(view_frame_sys_exception_cb), view);
	gtk_signal_connect(GTK_OBJECT(view_frame), "user_activate",
			   GTK_SIGNAL_FUNC(user_activate_request_cb), view);
	gtk_signal_connect(GTK_OBJECT(view_frame), "activated",
			   GTK_SIGNAL_FUNC(view_frame_activated_cb), view);

	view->corba_view = bonobo_view_frame_get_view(view_frame);

	wrapper = bonobo_view_frame_get_wrapper(view_frame);

	gtk_widget_show_all(wrapper);

	gtk_box_pack_start(GTK_BOX(view->vbox), wrapper, FALSE, FALSE, 0);

	view->slide_wrapper = wrapper;

	ACHTUNG_EXIT;

	return TRUE;
}

#if 0
static void
attach_canvas(NotesView *view)
{
	GtkWidget *swin;
	View *v;

	g_return_if_fail(view);

	v = VIEW(view);

	swin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);

	gtk_widget_push_visual(gdk_rgb_get_visual());
	gtk_widget_push_colormap(gdk_rgb_get_cmap());
	view->canvas = GNOME_CANVAS(gnome_canvas_new());
	gtk_widget_pop_colormap();
	gtk_widget_pop_visual();

	gnome_canvas_set_scroll_region(view->canvas, 0, 0, 600, 800);

	gtk_container_add(GTK_CONTAINER(swin), GTK_WIDGET(view->canvas));

	v->view_widget = swin;

	gtk_widget_show_all(swin);
}	
#endif

static void
vbox_size_alloc(GtkWidget *box, GtkAllocation *alloc, NotesView *view)
{
	GtkAllocation wrapper_alloc;

	wrapper_alloc.x = alloc->x;
	wrapper_alloc.y = alloc->y;
	wrapper_alloc.width = alloc->width;
	wrapper_alloc.height = (gint16) (alloc->height / 2);

	gtk_widget_size_allocate(GTK_WIDGET(view->slide_wrapper), 
				 &wrapper_alloc);
}

NotesView *
notes_view_new(Window *win, Slide *slide)
{
	NotesView *nview;
	View *view;

	g_return_val_if_fail(win, NULL);
	g_return_val_if_fail(slide, NULL);

	ACHTUNG_ENTRY;

	nview = gtk_type_new(notes_view_get_type());
	view = VIEW(nview);

	view->slideshow = slide->slideshow;
	view->current_slide = slide;

	/* vbox is the collection container for the slide and notes */
	nview->vbox = gtk_vbox_new(TRUE, 0);

	gtk_widget_show(nview->vbox);

	/* Embed the slide and create its embedded view */
	if (!notes_view_embed_slide(nview, win))
		return NULL;

	if (!notes_view_create_view_frame(nview, win->ui_container))
		return NULL;

	/* Create the text box */
	nview->text_box = gtk_text_new(NULL, NULL);
	gtk_text_set_editable(GTK_TEXT(nview->text_box), TRUE);
	gtk_text_set_word_wrap(GTK_TEXT(nview->text_box), TRUE);
	gtk_text_set_line_wrap(GTK_TEXT(nview->text_box), TRUE);

	gtk_box_pack_start(GTK_BOX(nview->vbox), nview->text_box, 
			   TRUE, TRUE, 0);

	gtk_signal_connect(GTK_OBJECT(nview), "destroy",
			   GTK_SIGNAL_FUNC(notes_view_destroy), NULL);

	gtk_signal_connect(GTK_OBJECT(nview->vbox), "size_allocate",
			   GTK_SIGNAL_FUNC(vbox_size_alloc), view);

	view->view_widget = nview->vbox;

	ACHTUNG_EXIT;

	return nview;
} 

