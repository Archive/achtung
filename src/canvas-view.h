/*
 * canvas-view.h: Implementation of the base CanvasView Class
 *
 * Copyright (c) 2000 Joe Shaw, Phil Schwan, Helix Code, Inc., the Puffin
 *                    Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@linuxcare.com>
 *
 */

#ifndef __ACHTUNG_CANVAS_VIEW_H
#define __ACHTUNG_CANVAS_VIEW_H

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

typedef struct _CanvasView      CanvasView;
typedef struct _CanvasViewClass CanvasViewClass;

#include "view.h"

#define CANVAS_VIEW_TYPE            (canvas_view_get_type ())
#define CANVAS_VIEW(obj)            (GTK_CHECK_CAST((obj), \
			             CANVAS_VIEW_TYPE, CanvasView))
#define CANVAS_VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                     CANVAS_VIEW_TYPE, CanvasViewClass))
#define IS_CANVAS_VIEW(obj)         (GTK_CHECK_TYPE ((obj), \
			             CANVAS_VIEW_TYPE))
#define IS_CANVAS_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                     CANVAS_VIEW_TYPE)

struct _CanvasView {
	View parent_object;

	GnomeCanvas *canvas;
	Window *window;
};

struct _CanvasViewClass {
	ViewClass parent_class;
};

/* Standard object functions */
GtkType canvas_view_get_type(void);

/* Create a new CanvasView */
CanvasView *canvas_view_new(void);

/* Set the scroll region for the canvas */
void canvas_view_set_size(CanvasView *view, gdouble width, gdouble height);

#endif
