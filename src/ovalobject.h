/*
 * ovalobject.h: Implementation of the oval object
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_OVALOBJECT_H
#define __ACHTUNG_OVALOBJECT_H

#include <libgnome/gnome-defs.h>

#include "reobject.h"

BEGIN_GNOME_DECLS

typedef struct _OvalObject      OvalObject;
typedef struct _OvalObjectClass OvalObjectClass;

#define OVALOBJECT_TYPE            (ovalobject_get_type ())
#define OVALOBJECT(obj)            (GTK_CHECK_CAST((obj), OVALOBJECT_TYPE, \
				    OvalObject))
#define OVALOBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                    OVALOBJECT_TYPE, OvalObjectClass))
#define IS_OVALOBJECT(obj)         (GTK_CHECK_TYPE ((obj), OVALOBJECT_TYPE))
#define IS_OVALOBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                    OVALOBJECT_TYPE))

struct _OvalObject {
	REObject parent_object;
};
	
struct _OvalObjectClass {
	REObjectClass parent_class;
};

/* Standard object functions */
GtkType ovalobject_get_type(void);

/* Create a new OvalObject */
AchObject *ovalobject_new(Slide *slide);

/* Destroy a OvalObject */
void ovalobject_destroy(GtkObject *object);

#endif
