/*
 * embeddable-slide-view-toolbar.c: Toolbars for the EmbeddableSlideView class
 *
 * Coypright (c) 1999, 2000 Joe Shaw, Phil Schwan, Mike Kestner,
 *		Helix Code, Inc., the Puffin Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@off.net>
 *	    Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include <gnome.h>

#include "achtung.h"
#include "achtung-util.h"
#include "embeddable-slide-view.h"
#include "embeddable-slide-view-toolbar.h"
#include "file.h"
#include "global-font.h"
#include "gtk-combo-text.h"
#include "pixmaps.h"
#include "widgets/widget-color-combo.h"


static void
fill_color_changed(ColorCombo *cc, GdkColor *color, int colod_index,
		   EmbeddableSlideView *view)
{
	guint32 rgba;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	rgba = achtung_gdkcolor_to_rgba(color);
	view->slide->slideshow->defaults.fill_color = rgba;

	ACHTUNG_EXIT;
} /* fill_color_changed */

static void
line_color_changed(ColorCombo *cc, GdkColor *color, int colod_index,
		   EmbeddableSlideView *view)
{
	guint32 rgba;
	
	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	rgba = achtung_gdkcolor_to_rgba(color);
	view->slide->slideshow->defaults.line_color = rgba;

	ACHTUNG_EXIT;
} /* line_color_changed */

static void
text_color_changed(ColorCombo *cc, GdkColor *color, int colod_index,
		   EmbeddableSlideView *view)
{
	guint32 rgba;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	rgba = achtung_gdkcolor_to_rgba(color);
	view->slide->slideshow->defaults.text_color = rgba;

	ACHTUNG_EXIT;
} /* text_color_changed */

static void
change_zoom_cb(GtkWidget *entry, EmbeddableSlideView *view)
{
	gchar *zoom_str;

	g_return_if_fail(view);

	zoom_str = gtk_editable_get_chars(GTK_EDITABLE(entry), 0, -1);

	if (g_strcasecmp(zoom_str, "Auto")) {
		int factor = atoi(zoom_str);
		if (factor > 0) {
			view->auto_zoom = FALSE;
			embeddable_slide_view_zoom(view, factor);
			zoom_str = g_strdup_printf("%d%%", factor);
			gtk_entry_set_text(GTK_ENTRY(entry), zoom_str);
		} else {
			if (view->auto_zoom) {
				zoom_str = g_strdup("Auto");
				gtk_entry_set_text(GTK_ENTRY(entry), zoom_str);
			} else {
				zoom_str = g_strdup_printf("%d%%", 
							   view->zoom_factor);
				gtk_entry_set_text(GTK_ENTRY(entry), zoom_str);
			}
		}				
	} else {
		view->auto_zoom = TRUE;
		embeddable_slide_view_auto_zoom(view);
	}
	g_free(zoom_str);
}

static GtkWidget*
create_zoom_combo(EmbeddableSlideView *view)
{
	char const * const preset_zoom[] =
	{
	    "Auto",
	    "200%",
	    "100%",
	    "75%",
	    "50%",
	    "25%",
	    NULL
	};
	int i, len;	
	GtkWidget *zoom, *entry;
	
	zoom = gtk_combo_text_new(FALSE);
	if (!gnome_preferences_get_toolbar_relief_btn())
		gtk_combo_box_set_arrow_relief(GTK_COMBO_BOX (zoom), 
					       GTK_RELIEF_NONE);
	entry = GTK_COMBO_TEXT(zoom)->entry;
	gtk_signal_connect(GTK_OBJECT(entry), "activate",
			   GTK_SIGNAL_FUNC(change_zoom_cb), view);

	/* Set a reasonable default width */
	len = gdk_string_measure(entry->style->font, "ABCDEF");
	gtk_widget_set_usize (entry, len, 0);

	/* Preset values */
	for (i = 0; preset_zoom[i] != NULL ; ++i)
		gtk_combo_text_add_item(GTK_COMBO_TEXT(zoom),
					preset_zoom[i], preset_zoom[i]);

	gtk_combo_text_select_item(GTK_COMBO_TEXT(zoom), 0);

	return zoom;
}

char draw_toolbar [] =
	"<dockitem name=\"draw_toolbar\" look=\"icon\">\n"
	"       <control name=\"zoom\" _tip=\"Zoom Factor\"/>\n"
	"       <control name=\"fill_color\" _tip=\"Fill Color\"/>\n"
	"       <control name=\"line_color\" _tip=\"Line Color\"/>\n"
	"</dockitem>";
 
static void
create_toolbars(EmbeddableSlideView *view)
{
	CORBA_Environment ev;
	GtkWidget *w;
	BonoboControl *control;
	Bonobo_Control corba_control;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	CORBA_exception_init(&ev);

	bonobo_ui_component_set_translate(view->ui_component, "/", 
					  draw_toolbar, &ev);

	/* Zoom Control */
	w = create_zoom_combo(view);
	gtk_widget_show(w);
	control = bonobo_control_new(w);
	corba_control = (Bonobo_Control) bonobo_object_corba_objref(
		BONOBO_OBJECT(control));
	bonobo_ui_component_object_set(view->ui_component, "/draw_toolbar/zoom", 
				       corba_control, NULL);

	/* Widget fill color */
	w = color_combo_new((const char **) bucket_xpm);
	color_combo_select_color(COLOR_COMBO(w), 1);
	gtk_widget_show(w);
	gtk_signal_connect(GTK_OBJECT(w), "changed",
			   GTK_SIGNAL_FUNC(fill_color_changed), view);
	control = bonobo_control_new(w);
	corba_control = (Bonobo_Control) bonobo_object_corba_objref(
		BONOBO_OBJECT(control));
	bonobo_ui_component_object_set(view->ui_component, "/draw_toolbar/fill_color", 
				       corba_control, NULL);

	/* Outline/Line color */
	w = color_combo_new((const char **) line_xpm);
	color_combo_select_color(COLOR_COMBO(w), 0);
	gtk_widget_show(w);
	gtk_signal_connect(GTK_OBJECT(w), "changed",
			   GTK_SIGNAL_FUNC(line_color_changed), view);
	control = bonobo_control_new(w);
	corba_control = (Bonobo_Control) bonobo_object_corba_objref(
		BONOBO_OBJECT(control));
	bonobo_ui_component_object_set(view->ui_component, "/draw_toolbar/line_color", 
				       corba_control, NULL);

	ACHTUNG_EXIT;
} /* embeddable_slide_view_create_toolbars */

static inline gint
do_color_combo_add(ColorCombo *cc, guint32 rgba)
{
	gchar *str;
	gint retval;

	str = achtung_rgba_to_string(rgba);
	retval = color_combo_add(cc, str, TRUE);
	g_free(str);

	return retval;
}

void
embeddable_slide_view_create_ui_component(EmbeddableSlideView *view)
{
	BonoboUIComponent *comp;

	comp = bonobo_ui_component_new("ESV");
	bonobo_ui_component_set_container(comp, view->corba_uic);
	view->ui_component = comp;

	create_toolbars(view);

	embeddable_slide_view_update_verb_sensitivity(view);

}

void
embeddable_slide_view_update_verb_sensitivity(EmbeddableSlideView *view)
{
}

