/*
 * slideshow.c: Implementation of the SlideShow class
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Helix Code, Inc.,
 *                          the Puffin Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@off.net>
 *
 */

#include <gnome.h>
#include "achtung.h"
#include "achtung-options.h"
#include "achobject.h"
#include "config.h"
#include "file.h"
#include "menu.h"
#include "slide.h"
#include "slideshow.h"
#include "view.h"

GList *open_slideshows = NULL;

static GtkObjectClass *parent_class;

enum SIGNALS {
	SLIDE_ADDED,
	LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0 };

static void slideshow_class_init(SlideShowClass *klass);
static void slideshow_init(GtkObject *object);
static void _slideshow_destroy(GtkObject *object);

GtkType
slideshow_get_type(void)
{
	static GtkType slideshow_type = 0;

	if (!slideshow_type) {
		static const GtkTypeInfo slideshow_info = {
			"SlideShow",
			sizeof(SlideShow),
			sizeof(SlideShowClass),
			(GtkClassInitFunc) slideshow_class_init,
			(GtkObjectInitFunc) slideshow_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		slideshow_type = gtk_type_unique(gtk_object_get_type(),
						 &slideshow_info);
	}

	return slideshow_type;
} /* slideshow_get_type */

/* Class initialization function for SlideShow */
static void
slideshow_class_init(SlideShowClass *klass)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class(gtk_object_get_type());

	object_class->destroy = _slideshow_destroy;

	signals[SLIDE_ADDED] = gtk_signal_new(
		"slide_added", GTK_RUN_LAST, object_class->type,
		GTK_SIGNAL_OFFSET(SlideShowClass, slide_added),
		gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
		GTK_TYPE_POINTER);

	gtk_object_class_add_signals(object_class, signals, LAST_SIGNAL);
} /* slideshow_class_init */

/* SlideShow intialization function */
static void
slideshow_init(GtkObject *object)
{
	SlideShow *ss;

	g_return_if_fail(object);
	g_return_if_fail(IS_SLIDESHOW(object));

	ss = SLIDESHOW(object);

	ss->name = NULL;
	ss->filename = NULL;
	ss->slides = NULL;
	ss->update_list = NULL;
} /* slideshow_init */

static void
_slideshow_destroy(GtkObject *object)
{
	g_return_if_fail(object);
	g_return_if_fail(IS_SLIDESHOW(object));

	slideshow_destroy(SLIDESHOW(object));

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy) (object);
} /* _slideshow_destroy */

/* Create a new slideshow */
SlideShow *
slideshow_new(const char *name)
{
	SlideShow *ss;
	Slide *slide;
	AchtungOptions *opt;
	SlideShowPageInfo pi;

	ACHTUNG_ENTRY;

	ss = gtk_type_new(slideshow_get_type());

	if (name)
		ss->name = g_strdup(name);

	/* Set up defaults */
	ss->defaults.fill_color = ~0;
	ss->defaults.line_color = 0;
	ss->defaults.background_color = 0xff0000ff;
	ss->defaults.text_color = 0;

	/* Set up default page information */
	pi.paper = g_strdup(gnome_paper_name_default());
	pi.slide_portrait = FALSE;
	pi.notes_portrait = TRUE;
	pi.start_page = 1;
	opt = achtung_get_options();
	pi.inch_units = opt->inch_units;
	slideshow_set_page_info(ss, &pi);

	/* Create the master slide */
	ss->master_slide = slide_new();
	slide_set_slideshow(ss->master_slide, ss);

	/* Create the first (blank) slide */
	slide = slide_new();
	slideshow_append(ss, slide);

	open_slideshows = g_list_append(open_slideshows, ss);

	ACHTUNG_EXIT;

	return ss;
} /* slideshow_new */

/* Set the name of the slideshow */
void
slideshow_set_name(SlideShow *ss, const char *name)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(ss);

	if (ss->name)
		g_free(ss->name);
	ss->name = g_strdup(name);

	ACHTUNG_EXIT;
} /* slideshow_set_name */

void
slideshow_set_filename(SlideShow *ss, const char *filename)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(ss);

	if (ss->filename)
		g_free(ss->filename);
	ss->filename = g_strdup(filename);

	ACHTUNG_EXIT;
} /* slideshow_set_filename */

SlideShow *
slideshow_open()
{
	gchar *filename;
	SlideShow *ss;
	
	filename = file_selector(NULL, "Open Slideshow", FILE_LOAD);

	if (!filename)
		return NULL;

	ss = slideshow_new(NULL);
	if (!slideshow_load_from_file(ss, filename)) {
		GtkWidget *box = 
			gnome_error_dialog("Unable to open slideshow");
		gtk_window_set_modal(GTK_WINDOW(box), TRUE);
		gtk_widget_show(box);
		gtk_object_destroy(GTK_OBJECT(ss));
		return NULL;
	}

	return ss;
} /* slideshow_open */

void
slideshow_append(SlideShow *ss, Slide *slide)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(ss);
	g_return_if_fail(IS_SLIDESHOW(ss));
	g_return_if_fail(slide);

	if (g_list_find(ss->slides, slide)) {
		g_warning("Slide is already in slideshow!\n");
		return;
	}

	ss->slides = g_list_append(ss->slides, slide);
	slide_set_slideshow(slide, ss);

	gtk_signal_emit(GTK_OBJECT(ss), signals[SLIDE_ADDED], slide);

	ACHTUNG_EXIT;
} /* slideshow_append */

void
slideshow_insert(SlideShow *ss, Slide *slide, gint position)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(ss);
	g_return_if_fail(slide);

	if (g_list_find(ss->slides, slide)) {
		g_warning("Slide is already in slideshow!\n");
		return;
	}

	ss->slides = g_list_insert(ss->slides, slide, position);
	slide_set_slideshow(slide, ss);
	
	gtk_signal_emit(GTK_OBJECT(ss), signals[SLIDE_ADDED], slide);

	ACHTUNG_EXIT;
} /* slideshow_insert */

void
slideshow_move(SlideShow *ss, gint old_idx, gint new_idx)
{
	Slide *slide;

	g_return_if_fail(ss);

	ACHTUNG_ENTRY;

	slide = slideshow_get_slide(ss, old_idx);
	if (!slide) {
		g_warning("Trying to move an invalidly indexed slide!\n");
		return;
	}

	ss->slides = g_list_remove(ss->slides, slide);
	ss->slides = g_list_insert(ss->slides, slide, new_idx);

	gtk_signal_emit_by_name(GTK_OBJECT(slide), "slide_reordered");

	ACHTUNG_EXIT;	
}

void
slideshow_remove(SlideShow *ss, Slide *slide)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(ss);
	g_return_if_fail(ss->slides);
	g_return_if_fail(slide);

	if (!g_list_find(ss->slides, slide)) {
		g_warning("Trying to remove invalid slide!\n");
		return;
	}

	slide_set_slideshow(slide, NULL);
	ss->slides = g_list_remove(ss->slides, slide);
	gtk_object_destroy(GTK_OBJECT(slide));

	ACHTUNG_EXIT;
} /* slideshow_remove */

void
slideshow_remove_nth(SlideShow *ss, gint idx)
{
	Slide *slide;

	ACHTUNG_ENTRY;

	g_return_if_fail(ss);
	g_return_if_fail(ss->slides);

	slide = slideshow_get_slide(ss, idx);
	if (!slide) {
		g_warning("Trying to remove an invalidly indexed slide!\n");
		return;
	}

	slide_set_slideshow(slide, NULL);
	ss->slides = g_list_remove(ss->slides, slide);
	gtk_object_destroy(GTK_OBJECT(slide));
	
	ACHTUNG_EXIT;
} /* slideshow_remove_nth */

gint
slideshow_length(SlideShow *ss)
{
	ACHTUNG_ENTRY;

	g_return_val_if_fail(ss, -1);

	ACHTUNG_EXIT;

	return g_list_length(ss->slides);
} /* slideshow_length */

void
slideshow_destroy(SlideShow *ss)
{
	GList *s;

	ACHTUNG_ENTRY;

	open_slideshows = g_list_remove(open_slideshows, ss);

	for (s = ss->slides; s; s = s->next) {
		g_return_if_fail(s->data);
		g_return_if_fail(IS_SLIDE(s->data));

		gtk_object_destroy(GTK_OBJECT(s->data));
	}

	g_list_free(ss->slides);
	ss->slides = NULL;
	
	gtk_object_destroy(GTK_OBJECT(ss->master_slide));

	/* Free various bits of data */
	if (ss->name)
		g_free(ss->name);
	if (ss->filename)
		g_free(ss->filename);
	if (ss->page_info.paper)
		g_free(ss->page_info.paper);

	/* FIXME: Free up the bonobo resources here */

	if (!g_list_length(open_slideshows)) {
		/* FIXME: replace with a better shutdown function call */
		gtk_main_quit();
	}

	ACHTUNG_EXIT;
} /* slideshow_destroy */

static void
set_point_dimensions(SlideShowPageInfo *pi)
{
	ACHTUNG_ENTRY;
	
	g_return_if_fail(pi);

	if (!g_strcasecmp(pi->paper, "Custom")) {
		if (pi->inch_units) {
			/* FIXME conversion rates */
			pi->pswidth = pi->width * 0.0;
			pi->psheight = pi->height * 0.0;
		} else {
			/* FIXME conversion rates */
			pi->pswidth = pi->width * 0.0;
			pi->psheight = pi->height * 0.0;
		}
	} else {
		if (pi->slide_portrait) {
			pi->pswidth = gnome_paper_pswidth(
					gnome_paper_with_name(pi->paper)); 
			pi->psheight = gnome_paper_psheight(
					gnome_paper_with_name(pi->paper));
		} else {
			pi->pswidth = gnome_paper_psheight(
					gnome_paper_with_name(pi->paper));
			pi->psheight= gnome_paper_pswidth(
					gnome_paper_with_name(pi->paper)); 
		}
	}

	ACHTUNG_EXIT;
}

Slide *
slideshow_get_slide(SlideShow *ss, int index)
{
	Slide *slide;

	g_return_val_if_fail(ss, NULL);
	g_return_val_if_fail(index >= -1, NULL);

	if (index == -1)
		return ss->master_slide;
	
	slide = SLIDE(g_list_nth_data(ss->slides, index));

	return slide;
} /* slideshow_get_slide */

int
slideshow_get_index(SlideShow *ss, Slide *slide)
{
	int index;

	g_return_val_if_fail(ss, -2);
	g_return_val_if_fail(slide, -2);
	g_return_val_if_fail(IS_SLIDE(slide), -2);

	if (slide == ss->master_slide)
		return -1;

	index = g_list_index(ss->slides, slide);

	if (index == -1)
		return -2;

	return index;
} /* slideshow_get_index */

void
slideshow_set_page_info(SlideShow *ss, SlideShowPageInfo *pi)
{
	g_return_if_fail(ss);
	g_return_if_fail(pi);

	if (ss->page_info.paper)
		g_free(ss->page_info.paper);
	ss->page_info.paper = g_strdup(pi->paper);
	ss->page_info.width = pi->width;
	ss->page_info.height = pi->height;
	ss->page_info.inch_units = pi->inch_units;
	ss->page_info.slide_portrait = pi->slide_portrait;
	ss->page_info.notes_portrait = pi->notes_portrait;
	ss->page_info.start_page = pi->start_page;
	set_point_dimensions(&ss->page_info);
} /* slideshow_set_page_info */
