/* file.c: File loading and saving routines
 *
 * This file is heavily based upon the file routines in Gnumeric. In fact,
 * it was originally ripped out and thrown in virtually unchanged, but it has
 * been adapted a bit for Achtung as time has progressed.
 *
 * Copyright (c) 1999, 2000 Miguel de Icaza, Joe Shaw and Helix Code, Inc.
 *
 * Authors: Miguel de Icaza <miguel@helixcode.com>
 *          Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "slideshow.h"
#include "file.h"

static GList *achtung_file_formats = NULL;
static FileFormat *default_format = NULL;
static FileFormat *current_format = NULL;

static gint
file_priority_sort(gconstpointer a, gconstpointer b)
{
	const FileFormat *fa = (const FileFormat *)a;
	const FileFormat *fb = (const FileFormat *)b;

	return (fb->open_priority - fa->open_priority);
} /* file_priority_sort */

void
file_format_register(int priority, const char *desc, const char *extension,
		     FileFormatProbe probe_fn, FileFormatOpen open_fn,
		     FileFormatSave save_fn, gboolean is_default)
{
	FileFormat *ff = g_new(FileFormat, 1);

	ff->open_priority = priority;
	ff->format_description = desc ? g_strdup(desc) : NULL;
	ff->extension = g_strdup(extension);
	ff->probe = probe_fn;
	ff->open = open_fn;
	ff->save = save_fn;
	if (is_default)
		default_format = ff;

	achtung_file_formats = g_list_insert_sorted(achtung_file_formats, ff,
						    file_priority_sort);
} /* file_format_register */

void
file_format_unregister(FileFormatProbe probe, FileFormatOpen open, 
		       FileFormatSave save)
{
	GList *l;

	for (l = achtung_file_formats; l; l = l->next) {
		FileFormat *ff = l->data;

		if (ff->probe == probe && ff->open == open && 
		    ff->save == save) {
			achtung_file_formats = 
				g_list_remove_link(achtung_file_formats, l);
			g_list_free_1(l);
			if (ff->format_description)
				g_free(ff->format_description);
			g_free(ff->extension);
			return;
		}

	}
} /* file_format_unregister */

gboolean
slideshow_save_to_file(SlideShow *ss, const char *filename)
{
	g_return_val_if_fail(ss, FALSE);
	g_return_val_if_fail(filename, FALSE);
	g_return_val_if_fail(default_format, FALSE);
	g_return_val_if_fail(default_format->save, FALSE);

	if (!current_format)
		current_format = default_format;

	if (!(*current_format->save)(ss, filename)) {
		g_warning("Unable to save file %s\n", filename);
		return FALSE;
	}
	slideshow_set_filename(ss, filename);
	return TRUE;
} /* slideshow_save_to */

gboolean
slideshow_load_from_file(SlideShow *ss, const char *filename)
{
	GList *l;

	ACHTUNG_ENTRY;

	g_return_val_if_fail(ss, FALSE);
	g_return_val_if_fail(filename, FALSE);

	for (l = achtung_file_formats; l; l = l->next) {
		const FileFormat *ff = l->data;

		if (ff->probe == NULL || ff->open == NULL)
			continue;

		if ((*ff->probe)(filename)) {
			if ((*ff->open)(ss, filename)) {
				slideshow_set_filename(ss, filename);
				return TRUE;
			} else
				return FALSE;
		}
	}
	return FALSE;
} /* slideshow_load_from */

static void
ok_selector(GtkWidget *fs, gboolean *accepted)
{
	*accepted = TRUE;
	gtk_main_quit();
} /* ok_selector */

static gboolean
delete_selector(GtkWidget *fs)
{
	gtk_main_quit();
	gtk_widget_destroy(fs);
	return TRUE;
} /* delete_selector */

static void
format_activate(GtkMenuItem *item, FileFormat *format)
{
	GList *l = achtung_file_formats;

	while (l) {
		FileFormat *ff = l->data;

		if (ff == format)
			current_format = format;

		l = l->next;
	}
} /* format_activate */

static GtkWidget *
make_format_chooser(void)
{
	GtkWidget *box, *label;
	GtkWidget *omenu, *menu;
	GList *l;
	int selected = -1;
	int i = 0;

	box = gtk_hbox_new(0, GNOME_PAD);
	label = gtk_label_new(_("File format:"));
	omenu = gtk_option_menu_new();
	menu = gtk_menu_new();

	l = achtung_file_formats;
	while (l) {
		GtkWidget *item;
		FileFormat *ff = l->data;

		if (ff->save) {
			item = gtk_menu_item_new_with_label
				(ff->format_description);
			gtk_widget_show(item);
			gtk_menu_append(GTK_MENU(menu), item);
			gtk_signal_connect(GTK_OBJECT(item), "activate", 
					   GTK_SIGNAL_FUNC(format_activate), 
					   ff);
			if (ff == current_format)
				selected = i;
			i++;
		}
		l = l->next;
	}

	gtk_option_menu_set_menu(GTK_OPTION_MENU(omenu), GTK_WIDGET(menu));
	if (selected > 0)
		gtk_option_menu_set_history(GTK_OPTION_MENU(omenu), selected);

	gtk_box_pack_start(GTK_BOX(box), label, FALSE, FALSE, GNOME_PAD);
	gtk_box_pack_start(GTK_BOX(box), omenu, FALSE, TRUE, GNOME_PAD);
	gtk_widget_show_all(box);

	return box;
} /* make_format_chooser */

char *
file_selector(SlideShow *ss, const char *title, gboolean type)
{
	GtkWidget *fs;
	GtkWidget *format_selector;
	gboolean accepted = FALSE;
	char *result;

	ACHTUNG_ENTRY;

	fs = gtk_file_selection_new(title);
	gtk_file_selection_hide_fileop_buttons(GTK_FILE_SELECTION(fs));
	gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(fs)->ok_button),
			   "clicked", GTK_SIGNAL_FUNC(ok_selector), &accepted);
	gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(fs)->cancel_button),
			   "clicked", GTK_SIGNAL_FUNC(gtk_main_quit), NULL);
	gtk_signal_connect(GTK_OBJECT(fs), "delete_event",
			   GTK_SIGNAL_FUNC(delete_selector), NULL);
	gtk_window_set_modal(GTK_WINDOW(fs), TRUE);
#if 0
	if (ss && ss->priv->toplevel)
		gtk_window_set_transient_for(GTK_WINDOW(fs), 
					     GTK_WINDOW(ss->priv->toplevel));
#endif
	if (type == FILE_SAVE) {
		format_selector = make_format_chooser();
		gtk_box_pack_start
			(GTK_BOX(GTK_FILE_SELECTION(fs)->action_area), 
			 format_selector,
			 FALSE, TRUE, 0);
	}


	gtk_widget_show(fs);
/*	gtk_grab_add(fs); */
	gtk_main();
	
	if (!accepted)
		result = NULL;
	else {
		char *name = 
		       gtk_file_selection_get_filename(GTK_FILE_SELECTION(fs));

		if (type == FILE_LOAD) {
			if (name[strlen(name)-1] == '/')
				result = NULL;
			else
				result = g_strdup(name);
		} else {
			if (name[strlen(name)-1] != '/') {
				if (!current_format)
					current_format = default_format;
				slideshow_save_to_file(ss, name);
			}
			/* Doesn't apply for saving */
			result = NULL;
		}
	}

	gtk_widget_destroy(fs);

	ACHTUNG_EXIT;

	return result;
} /* file_selector */
