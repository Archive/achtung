/*
 * slide.c: Implementation of the Slide class
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Helix Code, Inc., 
 *                          the Puffin Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@off.net>
 *
 */

#include <config.h>
#include <gnome.h>
#include <bonobo/bonobo-object-client.h>
#include <gdraw/oafiids.h>
#include "achtung.h"
#include "slide.h"
#include "slideshow.h"
#include "slide-view.h"
#include "achobject.h"

enum SIGNALS {
	BACKGROUND_CHANGED,
	SLIDE_REMOVED,
	SLIDE_REORDERED,
	LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0 };

static GtkObjectClass *parent_class;

static void
slide_do_destroy(Slide *slide)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(slide);

	if (slide->bg_info) {
		if (slide->bg_info->pixbuf)
			g_free(slide->bg_info->pixbuf);
		g_free(slide->bg_info);
	}

	bonobo_object_client_unref(slide->shapes, NULL);

	g_free(slide->notes);

	ACHTUNG_EXIT;
} /* slide_do_destroy */

static void
slide_destroy(GtkObject *object)
{
	Slide *slide;

	g_return_if_fail(object);
	g_return_if_fail(IS_SLIDE(object));

	slide = SLIDE(object);

	slide_do_destroy(slide);
	
	if (parent_class->destroy)
		(* parent_class->destroy) (object);
} /* slide_destroy */

static void
slide_class_init(GtkObjectClass *klass)
{
	parent_class = klass;

	klass->destroy = slide_destroy;

	signals[BACKGROUND_CHANGED] = gtk_signal_new(
		"background_changed", GTK_RUN_LAST, klass->type,
		GTK_SIGNAL_OFFSET(SlideClass, background_changed),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	signals[SLIDE_REMOVED] = gtk_signal_new(
		"slide_removed", GTK_RUN_LAST, klass->type,
		GTK_SIGNAL_OFFSET(SlideClass, slide_removed),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	signals[SLIDE_REORDERED] = gtk_signal_new(
		"slide_reordered", GTK_RUN_LAST, klass->type,
		GTK_SIGNAL_OFFSET(SlideClass, slide_reordered),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	gtk_object_class_add_signals(klass, signals, LAST_SIGNAL);
} /* slide_class_init */

static void
slide_init(GtkObject *object)
{
	Slide *slide;

	g_return_if_fail(object);
	g_return_if_fail(IS_SLIDE(object));

	slide = SLIDE(object);

	slide->slideshow = NULL;
	slide->shapes = NULL;
	slide->bg_info = NULL;
	slide->notes = NULL;
} /* slide_init */

GtkType
slide_get_type(void)
{
	static GtkType slide_type = 0;

	if (!slide_type) {
		static const GtkTypeInfo slide_info = {
			"Slide",
			sizeof(Slide),
			sizeof(SlideClass),
			(GtkClassInitFunc) slide_class_init,
			(GtkObjectInitFunc) slide_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		slide_type = gtk_type_unique(gtk_object_get_type(),
					     &slide_info);
	}

	return slide_type;
} /* slide_get_type */

Slide *
slide_new(void)
{
	Slide *slide;
	BackgroundInfo *bg_info;

	ACHTUNG_ENTRY;

	slide = gtk_type_new(slide_get_type());

	slide->shapes = bonobo_object_activate(GDRAW_SHAPES_OAFIID, 0);

	bg_info = g_new0(BackgroundInfo, 1);
	bg_info->color = 0;
	bg_info->image_type = BACKGROUND_IMAGE_NONE;
	bg_info->pixbuf = NULL;
	bg_info->page_info = NULL;
	slide->bg_info = bg_info;

	slide_background_changed(slide);

	slide->notes = g_strdup("");

	ACHTUNG_EXIT;

	return slide;
} /* slide_new */

void
slide_background_changed(Slide *slide)
{
	g_return_if_fail(slide);
	g_return_if_fail(IS_SLIDE(slide));

	gtk_signal_emit(GTK_OBJECT(slide), signals[BACKGROUND_CHANGED]);
} /* slide_background_changed */

void
slide_set_slideshow(Slide *slide, SlideShow *ss)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(slide);

	slide->slideshow = ss;

	if (!ss) {
		gtk_signal_emit(GTK_OBJECT(slide), signals[SLIDE_REMOVED]);
		return;
	}
	
	slide->bg_info->color = ss->defaults.background_color;
	slide->bg_info->page_info = &ss->page_info;

	slide_background_changed(slide);

	ACHTUNG_EXIT;
} /* slide_set_slideshow */

