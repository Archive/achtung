/*
 * imageobject.h: Implementation of the image object
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_IMAGEOBJECT_H
#define __ACHTUNG_IMAGEOBJECT_H

#include <libgnome/gnome-defs.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

BEGIN_GNOME_DECLS

typedef struct _ImageObject      ImageObject;
typedef struct _ImageObjectClass ImageObjectClass;

#define IMAGEOBJECT_TYPE            (imageobject_get_type ())
#define IMAGEOBJECT(obj)            (GTK_CHECK_CAST((obj), IMAGEOBJECT_TYPE, \
				     ImageObject))
#define IMAGEOBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                     IMAGEOBJECT_TYPE, ImageObjectClass))
#define IS_IMAGEOBJECT(obj)         (GTK_CHECK_TYPE ((obj), IMAGEOBJECT_TYPE))
#define IS_IMAGEOBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                     IMAGEOBJECT_TYPE))

struct _ImageObject {
	AchObject parent_object;

	/* Filename */
	gchar *filename;

	/* Pixbuf */
	GdkPixbuf *pixbuf;
};
	
struct _ImageObjectClass {
	AchObjectClass parent_class;
};

/* Standard object functions */
GtkType imageobject_get_type(void);

/* Create a new ImageObject */
AchObject *imageobject_new(Slide *slide);
AchObject *imageobject_new_with_pixbuf(Slide *slide, GdkPixbuf *pixbuf);

/* Destroy a ImageObject */
void imageobject_destroy(GtkObject *object);

#endif
