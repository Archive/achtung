/*
 * textobject.c: Implementation of the text object
 *
 * Copyright (c) 1999, 2000 Phil Schwan, Joe Shaw, the Puffin Group, Inc.,
 *                          Linuxcare, Inc. and Helix Code, Inc.
 *
 * Author: Phil Schwan <pschwan@off.net>
 *         Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#if 0
#	include <gtktext/gtktextcanvas.h>
#endif
#include "achtung.h"
#include "achobject.h"
#include "embeddable-slide-view.h"
#include "textobject.h"
#include "slide.h"
#include "slide-view.h"

static void textobject_class_init(TextObjectClass *klass);
static void textobject_init(GtkObject *object);
static void textobject_set_arg(GtkObject *object, GtkArg *arg, guint arg_id);
static void textobject_get_arg(GtkObject *object, GtkArg *arg, guint arg_id);
static void textobject_destroy(GtkObject *object);
static void textobject_got_focus(AchObject * achobject);
static void textobject_lost_focus(AchObject * achobject);

/* Object argument IDs */
enum {
	ARG_0,
	ARG_LAST
};

static AchObjectClass *parent_class;

GtkType
textobject_get_type(void)
{
	static GtkType textobject_type = 0;

	if (!textobject_type) {
		static const GtkTypeInfo textobject_info = {
			"TextObject",
			sizeof(TextObject),
			sizeof(TextObjectClass),
			(GtkClassInitFunc) textobject_class_init,
			(GtkObjectInitFunc) textobject_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		textobject_type = gtk_type_unique(achobject_get_type(),
						  &textobject_info);
	}

	return textobject_type;
} /* textobject_get_type */

/* Class initialization function for TextObject */
static void
textobject_class_init(TextObjectClass *klass)
{
	GtkObjectClass *object_class;
	AchObjectClass *achobject_class;

	object_class = (GtkObjectClass *) klass;
	achobject_class = (AchObjectClass *) klass;
	
	parent_class = gtk_type_class(achobject_get_type());

	object_class->destroy = textobject_destroy;
	object_class->set_arg = textobject_set_arg;
	object_class->get_arg = textobject_get_arg;
	achobject_class->got_focus = textobject_got_focus;
	achobject_class->lost_focus = textobject_lost_focus;
} /* textobject_class_init */

/* TextObject intialization function */
static void
textobject_init(GtkObject *object)
{
	AchObject *aobj;
	TextObject *obj;

	g_return_if_fail(object);

	aobj = ACHOBJECT(object);
	obj = TEXTOBJECT(object);

	aobj->x1 = 0.0;
	aobj->y1 = 0.0;
	aobj->x2 = 0.0;
	aobj->y2 = 0.0;
	aobj->slide = NULL;

	obj->bg_color = ~0;
} /* textobject_init */

AchObject *
textobject_new(Slide *slide)
{
	TextObject *obj;
	AchObject *ao;

	g_return_val_if_fail(slide, NULL);
	g_return_val_if_fail(IS_SLIDE(slide), NULL);

	obj = gtk_type_new(textobject_get_type());

#if 0
	obj->buffer = gtk_text_buffer_new(NULL);

	/* The buffer is going to be sink'd when the canvasitem is created.
	 * Grab a ref to it which we'll release when the object is destroyed.
	 */
	gtk_object_ref(GTK_OBJECT(obj->buffer));
#endif

	ao = ACHOBJECT(obj);

	ao->slide = slide;
	if (slide->object_added)
		(* slide->object_added) (slide, ACHOBJECT(obj));

	achobject_changed(ACHOBJECT(obj));

	return ACHOBJECT(obj);
} /* textobject_new */

static void
textobject_destroy(GtkObject *object)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(object);
	g_return_if_fail(IS_TEXTOBJECT(object));

#if 0
	gtk_object_unref(GTK_OBJECT(TEXTOBJECT(object)->buffer));
#endif

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy) (object);

	ACHTUNG_EXIT;
} /* textobject_destroy */

static void
textobject_set_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
	TextObject *obj;

	g_return_if_fail(object);
	g_return_if_fail(IS_TEXTOBJECT(object));

	obj = TEXTOBJECT(object);

	switch (arg_id) {
	default:
		break;
	}
} /* textobject_set_arg */

static void
textobject_get_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
	TextObject *obj;

	g_return_if_fail(object);
	g_return_if_fail(IS_TEXTOBJECT(object));

	obj = TEXTOBJECT(object);

	switch (arg_id) {
#if 0
	case ARG_COLOR:
		GTK_VALUE_INT(*arg) = obj->bg_color;
		break;
#endif
	default:
		arg->type = GTK_TYPE_INVALID;
		break;
	}
} /* textobject_get_arg */

static void
textobject_got_focus(AchObject * achobject)
{
	CanvasObject *cobj;

        g_return_if_fail(achobject);

	achobject_changed(achobject);

	cobj = gtk_object_get_data(GTK_OBJECT(achobject), "cobj");
	g_return_if_fail(cobj);

	gnome_canvas_item_set(cobj->canvas_item,
			      "editable", TRUE,
			      NULL);
	gnome_canvas_item_grab_focus(cobj->canvas_item);
} /* textobject_got_focus */

static void
textobject_lost_focus(AchObject * achobject)
{
	CanvasObject *cobj;

        g_return_if_fail(achobject);

	ACHTUNG_ENTRY;

	achobject_changed(achobject);

	cobj = gtk_object_get_data(GTK_OBJECT(achobject), "cobj");
	g_return_if_fail(cobj);

	gnome_canvas_item_set(cobj->canvas_item,
			      "editable", FALSE,
			      NULL);
#if 0
	/* If the text object is empty, destroy it.  Otherwise, the user will
	 * never be able to click on it and it will just be leaked. */
	gtk_object_get(GTK_OBJECT(cobj->canvas_item), "text_length",
		       &len, NULL);
	if (len == 0)
		gtk_object_destroy(GTK_OBJECT(achobject));
#endif

	ACHTUNG_EXIT;
} /* textobject_lost_focus */
