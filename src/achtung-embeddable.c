/*
 * achtung-embeddable.c: Implements the AchtungEmbeddable object
 *
 * Copyright (c) 2000 Mike Kestner and Helix Code, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include "idl/Achtung.h"
#include "achtung-embeddable.h"
#include "achtung.h"
#include "embeddable-slide-view.h"

#define PARENT_TYPE BONOBO_X_OBJECT_TYPE

static BonoboGenericFactory *achtung_embeddable_factory_object;

static inline AchtungEmbeddable *
achtung_embeddable_from_servant(PortableServer_Servant servant)
{
	return ACHTUNG_EMBEDDABLE(bonobo_object_from_servant(servant));
}	   

static CORBA_double
SlideShow_get_aspect_ratio(PortableServer_Servant servant, 
			   CORBA_Environment * ev)
{
	AchtungEmbeddable *ae = achtung_embeddable_from_servant(servant);
	SlideShowPageInfo *pi = &ae->slideshow->page_info;

	return (pi->pswidth / pi->psheight);
}

static void
achtung_embeddable_destroy(GtkObject *obj)
{
	AchtungEmbeddable *ae = ACHTUNG_EMBEDDABLE(obj);

	ACHTUNG_ENTRY;

	gtk_object_unref(GTK_OBJECT(ae->slideshow));

	ACHTUNG_EXIT;
}

static void
achtung_embeddable_destroyed(AchtungEmbeddable *ae)
{
	achtung_embeddable_destroy(GTK_OBJECT(ae));
}

static void
achtung_embeddable_class_init(GtkObjectClass *klass)
{
	AchtungEmbeddableClass *ae_class = ACHTUNG_EMBEDDABLE_CLASS (klass);
	POA_GNOME_Achtung_Slideshow__epv *epv = &ae_class->epv;

	ACHTUNG_ENTRY;

	epv->getAspectRatio = SlideShow_get_aspect_ratio;

	klass->destroy = achtung_embeddable_destroy;

	ACHTUNG_EXIT;
}

static void
achtung_embeddable_init(GtkObject *object)
{
} /* achtung_embeddable_init */

BONOBO_X_TYPE_FUNC_FULL (AchtungEmbeddable, GNOME_Achtung_Slideshow, 
			 PARENT_TYPE, achtung_embeddable);

AchtungEmbeddable *
achtung_embeddable_new(SlideShow *ss)
{
	AchtungEmbeddable *ae;
	BonoboEmbeddable *emb;

	g_return_val_if_fail(ss, NULL);

	ACHTUNG_ENTRY;

	ae = gtk_type_new(ACHTUNG_EMBEDDABLE_TYPE);

	ae->slideshow = ss;

	emb = bonobo_embeddable_new(
			(BonoboViewFactory)embeddable_slide_view_factory, ss);

	bonobo_object_add_interface(BONOBO_OBJECT(ae), BONOBO_OBJECT(emb));

	ae->embeddable = emb;

	gtk_object_ref(GTK_OBJECT(ss));

	ACHTUNG_EXIT;

	return ae;
} /* achtung_embeddable_new */

static BonoboObject *
achtung_embeddable_factory(BonoboGenericFactory *this)
{
	AchtungEmbeddable *ae;
	SlideShow *ss;

	ACHTUNG_ENTRY;

	ss = slideshow_new(NULL);

	ae = achtung_embeddable_new(ss);

	if (!ae) {
		printf("returning NULL...\n");
		return NULL;
	}

	gtk_signal_connect(
		GTK_OBJECT(ae), "destroy",
		GTK_SIGNAL_FUNC(achtung_embeddable_destroyed), NULL);

	/* The embeddable is also carrying a ref on the slideshow.  Unref it so
	 * that the slideshow is destroyed when the embeddable is destroyed. */
	gtk_object_unref(GTK_OBJECT(ss));

	ACHTUNG_EXIT;

	return BONOBO_OBJECT(ae);
} /* achtung_embeddable_factory */

void
achtung_embeddable_factory_init()
{
	ACHTUNG_ENTRY;

	if (achtung_embeddable_factory_object != NULL) {
		printf("returning...\n");
		return;
	}

	achtung_embeddable_factory_object = bonobo_generic_factory_new(
		"OAFIID:GNOME_Achtung_SlideshowFactory:9d825ad6-cf3d-4ed5-ae42-00a10723b925", 
		(BonoboGenericFactoryFn)achtung_embeddable_factory, NULL);

	if (!achtung_embeddable_factory_object)
		g_error("Unable to register Achtung object factory");

	ACHTUNG_EXIT;
} /* achtung_embeddable_factory_init */
