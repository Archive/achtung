/*
 * background.c: Manages the slide's background, be it image or solid color.
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <glib.h>
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>
#include "achtung.h"
#include "background.h"
#include "slide.h"

void 
background_set_color(Slide *slide, guint32 color)
{		
	ACHTUNG_ENTRY;

	g_return_if_fail(slide);

	if (slide->bg_info->image_type != BACKGROUND_IMAGE_NONE)
		gdk_pixbuf_unref(slide->bg_info->pixbuf);

	slide->bg_info->image_type = BACKGROUND_IMAGE_NONE;
	slide->bg_info->color = color;
	slide_background_changed(slide);

	ACHTUNG_EXIT;
} /* background_set_color */

void 
background_set_image_center(Slide *slide, GdkPixbuf *pixbuf)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(slide);
	g_return_if_fail(pixbuf);

	if (slide->bg_info->image_type != BACKGROUND_IMAGE_NONE)
		gdk_pixbuf_unref(slide->bg_info->pixbuf);

	slide->bg_info->image_type = BACKGROUND_IMAGE_CENTER;
	slide->bg_info->pixbuf = pixbuf;
	slide_background_changed(slide);

	ACHTUNG_EXIT;
} /* background_set_image_center */

void 
background_set_image_scale(Slide *slide, GdkPixbuf *pixbuf)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(slide);
	g_return_if_fail(pixbuf);

	if (slide->bg_info->image_type != BACKGROUND_IMAGE_NONE)
		gdk_pixbuf_unref(slide->bg_info->pixbuf);

	slide->bg_info->image_type = BACKGROUND_IMAGE_SCALED;
	slide->bg_info->pixbuf = pixbuf;
	slide_background_changed(slide);

	ACHTUNG_EXIT;
} /* background_set_image_scale */

void 
background_set_image_tile(Slide *slide, GdkPixbuf *pixbuf)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(slide);
	g_return_if_fail(pixbuf);

	if (slide->bg_info->image_type != BACKGROUND_IMAGE_NONE)
		gdk_pixbuf_unref(slide->bg_info->pixbuf);

	slide->bg_info->image_type = BACKGROUND_IMAGE_TILE;
	slide->bg_info->pixbuf = pixbuf;
	slide_background_changed(slide);

	ACHTUNG_EXIT;
} /* background_set_image_tile */

GnomeCanvasItem *
background_canvas_item(EmbeddableSlideView *view, GnomeCanvasGroup *g)
{
	GnomeCanvasGroup *group;
	double slide_width, slide_height;
	BackgroundInfo *bg_info = view->slide->bg_info;

	ACHTUNG_ENTRY;

	slide_width = bg_info->page_info->pswidth;
	slide_height = bg_info->page_info->psheight;

	group = GNOME_CANVAS_GROUP(gnome_canvas_item_new(
		g,
		gnome_canvas_group_get_type(),
		"x", - slide_width / 2.0,
		"y", - slide_height / 2.0,
		NULL));

	if(view->present_mode) {
		/* Cover canvas with black background */
		gnome_canvas_item_new(group, gnome_canvas_rect_get_type(),
				      "x1", - slide_width / 2.0, 
				      "y1", - slide_height / 2.0,
				      "x2", slide_width * 1.5,
				      "y2", slide_height * 1.5,
				      "outline_color_rgba", 0x000000ff,
				      "fill_color_rgba", 0x000000ff,
				      NULL);
	} else {
		/* Dropshadow */
		gnome_canvas_item_new(group, gnome_canvas_rect_get_type(),
				      "x1", 8.0, "y1", 8.0,
				      "x2", slide_width + 8.0,
				      "y2", slide_height + 8.0,
				      "outline_color_rgba", 0x000000ff,
				      "fill_color_rgba", 0x000000ff,
				      NULL);
	}

	switch (bg_info->image_type) {
	case BACKGROUND_IMAGE_NONE:
		gnome_canvas_item_new(
			group,
			gnome_canvas_rect_get_type(),
			"x1", 0.0,
			"y1", 0.0,
			"x2", slide_width,
			"y2", slide_height,
			"outline_color_rgba", bg_info->color,
			"fill_color_rgba", bg_info->color,
			NULL);
		break;
	case BACKGROUND_IMAGE_CENTER:
	{
		double width, height;
		double x, y;

		width = (double) gdk_pixbuf_get_width(bg_info->pixbuf);
		height = (double) gdk_pixbuf_get_height(bg_info->pixbuf);
		x = (slide_width - width) / 2.0;
		y = (slide_height - height) / 2.0;

		gnome_canvas_item_new(
			group,
			gnome_canvas_rect_get_type(),
			"x1", 0.0,
			"y1", 0.0,
			"x2", slide_width,
			"y2", slide_height,
			"fill_color", NULL,
			"outline_color", NULL,
			NULL);

		gnome_canvas_item_new(
			group,
			gnome_canvas_pixbuf_get_type(),
			"pixbuf", bg_info->pixbuf,
			"width_set", TRUE,
			"height_set", TRUE,
			"x", x,
			"y", y,
			"width", width,
			"height", height,
			NULL);
		break;
	}
	case BACKGROUND_IMAGE_SCALED:
   		gnome_canvas_item_new(
			group,
			gnome_canvas_rect_get_type(),
			"x1", 0.0,
			"y1", 0.0,
			"x2", slide_width,
			"y2", slide_height,
			"fill_color", NULL,
			"outline_color", NULL,
			NULL);

		gnome_canvas_item_new(
			group,
			gnome_canvas_pixbuf_get_type(),
			"pixbuf", bg_info->pixbuf,
			"width_set", TRUE,
			"height_set", TRUE,
			"x", 0.0,
			"y", 0.0,
			"width", slide_width,
			"height", slide_height,
			NULL);
		break;
	case BACKGROUND_IMAGE_TILE:
	{
		double x, y;
		double width, height;

		gnome_canvas_item_new(
			group,
			gnome_canvas_rect_get_type(),
			"x1", 0.0,
			"y1", 0.0,
			"x2", slide_width,
			"y2", slide_height,
			"fill_color", NULL,
			"outline_color", NULL,
			NULL);

		x = 0.0;
		y = 0.0;
		width = (double) gdk_pixbuf_get_width(bg_info->pixbuf);
		height = (double) gdk_pixbuf_get_height(bg_info->pixbuf);
		while (y <= slide_height) {
			gnome_canvas_item_new(
				group,
				gnome_canvas_pixbuf_get_type(),
				"pixbuf", bg_info->pixbuf,
				"width_set", TRUE,
				"height_set", TRUE,
				"x", x,
				"y", y,
				"width", width,
				"height", height,
				NULL);

			x += width;
			if (x > slide_width) {
				x = 0.0;
				y += height;
			}
		}
		break;
	}
	default:
		g_return_val_if_fail(NULL, NULL);
	}

	gnome_canvas_item_lower_to_bottom(GNOME_CANVAS_ITEM(group));

	ACHTUNG_EXIT;

	return GNOME_CANVAS_ITEM(group);
} /* background_canvas_item */
