/*
 * bonoboobject.h: AchObject implementation of Bonobo objects
 *
 * Copyright (c) 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_BONOBOOBJECT_H
#define __ACHTUNG_BONOBOOBJECT_H

#include <libgnome/gnome-defs.h>

#include "achobject.h"

BEGIN_GNOME_DECLS

typedef struct _BonObject      BonObject;
typedef struct _BonObjectClass BonObjectClass;

#define BONOBJECT_TYPE            (bonobject_get_type ())
#define BONOBJECT(obj)            (GTK_CHECK_CAST((obj),                   \
				   BONOBJECT_TYPE, BonObject))
#define BONOBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass),          \
                                   BONOBJECT_TYPE, BonObjectClass))
#define IS_BONOBJECT(obj)         (GTK_CHECK_TYPE ((obj),                  \
                                   BONOBJECT_TYPE))
#define IS_BONOBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass),          \
                                   BONOBJECT_TYPE))

struct _BonObject {
	AchObject parent_object;

	BonoboClientSite *client_site;
	BonoboObjectClient *server;
	char *id;
};
	
struct _BonObjectClass {
	AchObjectClass parent_class;
};

/* Standard object functions */
GtkType bonobject_get_type(void);

/* Create a new BonObject */
AchObject *bonobject_new(Slide *slide, char *id, BonoboItemContainer *container);

/* Destroy a BonObject */
void bonobject_destroy(GtkObject *object);

#endif
