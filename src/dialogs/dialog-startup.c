/*
 * achtung-main.c: Initialization routines for Achtung
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <gnome.h>
#include <glade/glade.h>
#include "config.h"
#include "achtung.h"
#include "slide.h"
#include "slide-view.h"
#include "slideshow.h"
#include "view.h"
#include "dialogs.h"

void
dialog_startup(Window *win)
{
	GladeXML *gui;
	GtkWidget *dialog, *new_rb, *open_rb, *druid_rb;
	gint     b;

	g_return_if_fail(win);

	gui = glade_xml_new(ACHTUNG_GLADEDIR "/startup.glade", NULL);
	if (!gui) {
		g_error("Missing startup.glade file. Reinstall Achtung");
		return;
	}

	dialog = glade_xml_get_widget(gui, "dialog");
	new_rb = glade_xml_get_widget(gui, "b1");
	open_rb = glade_xml_get_widget(gui, "b2");
	druid_rb = glade_xml_get_widget(gui, "b3");

	if (!dialog || !new_rb || !open_rb || !druid_rb) {
		g_warning("Corruption detected in startup.glade\n");
		gtk_object_destroy(GTK_OBJECT(gui));
		return;
	}

	b = window_dialog_run(win, GNOME_DIALOG(dialog));

	if (b == 0) {
		if (GTK_TOGGLE_BUTTON(new_rb)->active) {
			SlideShow *ss;
			SlideView *view;

			ss = slideshow_new("Untitled");
			view = slide_view_new(win, SLIDE(ss->slides->data));
			window_set_view(win, VIEW(view));
		} else if (GTK_TOGGLE_BUTTON(open_rb)->active) {
			SlideShow *ss;
			SlideView *view;

			ss = slideshow_open();
			if (ss) {
				view = slide_view_new(win, SLIDE(ss->slides->data));
				window_set_view(win, VIEW(view));
			} /* else Cancel / no file */
		} else if (GTK_TOGGLE_BUTTON(druid_rb)->active) {
			printf("DRUIDS!\n");
		} else
			g_assert_not_reached();
	}

	if (b != -1)
		gtk_widget_destroy(dialog);

	gtk_object_destroy(GTK_OBJECT(gui));

} /* dialog_startup */
