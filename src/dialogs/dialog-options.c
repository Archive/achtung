/**
 * dialog-options.c:  Implements the application options dialog.
 *
 * Author:
 *  Mike Kestner <mkestner@ameritech.net>
 *
 **/

#include <config.h>
#include <gnome.h>
#include <glade/glade.h>
#include "achtung-options.h"
#include "dialogs.h"
#include "window.h"

static void
dlg_options_destroy(dlg_options_t *dopt)
{
	g_return_if_fail(dopt);

	gtk_object_unref(GTK_OBJECT(dopt->gui));
	dopt->opt->dlg_info = NULL;
	g_free(dopt);
}

static void
save_general_page(dlg_options_t *dopt)
{
}

static void
save_view_page(dlg_options_t *dopt)
{
	GtkWidget *tmp;
	gfloat gran;

	g_return_if_fail(dopt);

	tmp = glade_xml_get_widget(dopt->gui, "inch_rb");
	dopt->opt->inch_units = GTK_TOGGLE_BUTTON(tmp)->active;

	tmp = glade_xml_get_widget(dopt->gui, "horiz_ruler_chk");
	dopt->opt->horz_ruler = GTK_TOGGLE_BUTTON(tmp)->active;

	tmp = glade_xml_get_widget(dopt->gui, "vert_ruler_chk");
	dopt->opt->vert_ruler = GTK_TOGGLE_BUTTON(tmp)->active;

	tmp = glade_xml_get_widget(dopt->gui, "gran_sb");
	gran = gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(tmp));
	if(dopt->opt->inch_units) {
		dopt->opt->grid_gran = (gint) (gran * 72);
	} else {
		dopt->opt->grid_gran = (gint) (gran * 72 / 2.54);
	}

	if(dopt->opt->grid_gran < 1)
		dopt->opt->grid_gran = 1;

}

static void
save_state(dlg_options_t *dopt)
{
	save_general_page(dopt);
	save_view_page(dopt);
	achtung_store_options();
}

static gint
delete_cmd(GtkWidget *w, GdkEvent *event, dlg_options_t *dopt)
{
	g_return_val_if_fail(dopt, FALSE);

	dlg_options_destroy(dopt);
	return FALSE;
}

static void
cancel_cmd(GtkWidget *b, dlg_options_t *dopt)
{
	g_return_if_fail(dopt);

	gnome_dialog_close(GNOME_DIALOG(dopt->dialog));
	dlg_options_destroy(dopt);
}
	
static void
update_cmd(GtkWidget *b, dlg_options_t *dopt)
{
	g_return_if_fail(dopt);

	if (dopt->is_dirty) {
		save_state(dopt);
		gtk_widget_set_sensitive(GTK_WIDGET(b), FALSE);
		dopt->is_dirty = FALSE;
	}
}

static void
upd_n_close_cmd(GtkWidget *b, dlg_options_t *dopt)
{
	g_return_if_fail(dopt);

	if (dopt->is_dirty)
		save_state(dopt);
	gnome_dialog_close(GNOME_DIALOG(dopt->dialog));
	dlg_options_destroy(dopt);
}

static void
set_dirty_cmd(GtkWidget *rb, dlg_options_t *dopt)
{
	GtkWidget *apply_btn;
	g_return_if_fail(dopt);

	apply_btn = glade_xml_get_widget(dopt->gui, "apply_button");

	gtk_widget_set_sensitive(GTK_WIDGET(apply_btn), TRUE);
	dopt->is_dirty = TRUE;
}

static void
units_changed_cmd(GtkWidget *rb, dlg_options_t *dopt)
{
	GtkWidget *unit_label;

	unit_label = glade_xml_get_widget(dopt->gui, "unit_lbl");

	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb))) {
		gtk_label_set_text(GTK_LABEL(unit_label), "in");
	} else {
		gtk_label_set_text(GTK_LABEL(unit_label), "cm");
	}

	set_dirty_cmd(rb, dopt);
}

static gboolean
prepare_general_page(dlg_options_t *dopt)
{
	g_return_val_if_fail(dopt, FALSE);

	return TRUE;
}

static gboolean
prepare_view_page(dlg_options_t *dopt)
{
	GtkWidget *in_rb, *cm_rb, *vert_chk, *horz_chk, *gran_sb, *unit_lbl; 
	gfloat gran;

	g_return_val_if_fail(dopt, FALSE);
	g_return_val_if_fail(dopt->gui, FALSE);
	g_return_val_if_fail(dopt->opt, FALSE);

	/* Get handles for the necessary widgets */
	vert_chk = glade_xml_get_widget (dopt->gui, "vert_ruler_chk");
	horz_chk = glade_xml_get_widget (dopt->gui, "horiz_ruler_chk");
	gran_sb = glade_xml_get_widget (dopt->gui, "gran_sb");
	in_rb 	= glade_xml_get_widget (dopt->gui, "inch_rb");
	cm_rb 	= glade_xml_get_widget (dopt->gui, "cm_rb");
	unit_lbl = glade_xml_get_widget (dopt->gui, "unit_lbl");
	
	if (!vert_chk || !horz_chk || !gran_sb || !in_rb || 
	    !cm_rb || !unit_lbl) {
		g_warning ("Corruption detected in page-setup.glade\n");
		return FALSE;
	}

	/* Set the initial dialog state */
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(gran_sb), 
				  dopt->opt->grid_gran); 

	if (dopt->opt->inch_units) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(in_rb), TRUE);
		gran = dopt->opt->grid_gran / 72.0;
		gtk_label_set_text(GTK_LABEL(unit_lbl), "in");
	} else {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cm_rb), TRUE); 
		gran = dopt->opt->grid_gran / 72.0 * 2.54;
		gtk_label_set_text(GTK_LABEL(unit_lbl), "cm");
	}

	gtk_spin_button_set_value(GTK_SPIN_BUTTON(gran_sb), gran); 

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(vert_chk), 
				     dopt->opt->vert_ruler); 
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(horz_chk), 
				     dopt->opt->horz_ruler); 

	/* Connect the necessary signal handlers */
	gtk_signal_connect (GTK_OBJECT(GTK_SPIN_BUTTON(gran_sb)->adjustment), 
			   "value_changed", GTK_SIGNAL_FUNC(set_dirty_cmd), 
			   dopt);
	gtk_signal_connect (GTK_OBJECT(vert_chk), "toggled",
			    GTK_SIGNAL_FUNC(set_dirty_cmd), dopt);
	gtk_signal_connect (GTK_OBJECT(horz_chk), "toggled",
			    GTK_SIGNAL_FUNC(set_dirty_cmd), dopt);
	gtk_signal_connect (GTK_OBJECT(in_rb), "toggled",
			    GTK_SIGNAL_FUNC(units_changed_cmd), dopt);

	return TRUE;
}

static gboolean
prepare_dlg(dlg_options_t *dopt)
{
	GtkWidget *ok_btn, *apply_btn, *cancel_btn;

	g_return_val_if_fail(dopt, FALSE);
	g_return_val_if_fail(dopt->gui, FALSE);

	ok_btn = glade_xml_get_widget(dopt->gui, "ok_button");
	apply_btn = glade_xml_get_widget(dopt->gui, "apply_button");
	cancel_btn = glade_xml_get_widget(dopt->gui, "cancel_button");

	if (!ok_btn || !apply_btn || !cancel_btn) {
		g_warning("Corruption detected in options.glade\n");
		return FALSE;
	}

	if (!prepare_general_page(dopt))
		return FALSE;

	if (!prepare_view_page(dopt))
		return FALSE;

	gtk_signal_connect(GTK_OBJECT(ok_btn), "clicked",
			   GTK_SIGNAL_FUNC(upd_n_close_cmd), dopt);
	gtk_signal_connect(GTK_OBJECT(apply_btn), "clicked",
			   GTK_SIGNAL_FUNC(update_cmd), dopt);
	gtk_signal_connect(GTK_OBJECT(cancel_btn), "clicked",
			   GTK_SIGNAL_FUNC(cancel_cmd), dopt);

	return TRUE;
}

static dlg_options_t*
dlg_options_new(AchtungOptions *opt)
{
	dlg_options_t *dopt;

	dopt = g_new0(dlg_options_t, 1);

	if (!dopt)
		return NULL;

	dopt->opt = opt;

	dopt->gui = glade_xml_new(ACHTUNG_GLADEDIR "/options.glade", NULL);
	if (!dopt->gui)
		return NULL;

	dopt->dialog = GNOME_DIALOG(glade_xml_get_widget(dopt->gui, "dialog"));

	if (!dopt->dialog || !prepare_dlg(dopt)) {
		gtk_object_destroy(GTK_OBJECT(dopt->gui));
		return NULL;
	}

	gtk_signal_connect(GTK_OBJECT(dopt->dialog), "delete_event",
			   GTK_SIGNAL_FUNC(delete_cmd), dopt);
		
	dopt->is_dirty = FALSE;

	return dopt;
}

void
dialog_options (Window *win)
{
	AchtungOptions *options;

	g_return_if_fail(win);

	options = achtung_get_options();

	if (options->dlg_info) {
		GnomeDialog *dlg = options->dlg_info->dialog;
		if (GNOME_IS_DIALOG(dlg)) {
			window_dialog_reparent(win, dlg);
			return;
		} else {
			g_assert_not_reached();
		}			
	}

	options->dlg_info = dlg_options_new(options);

	if (options->dlg_info) {
		window_dialog_reparent(win, options->dlg_info->dialog);
		gtk_widget_show_all(GTK_WIDGET(options->dlg_info->dialog));
	}
}
