/*
 * dialogs.h: Interface for Achtung dialog functions
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __ACHTUNG_DIALOGS_H
#define __ACHTUNG_DIALOGS_H

#include <gnome.h>
#include <glade/glade.h>
#include "window.h"

typedef struct _dlg_options_t dlg_options_t;

#include "achtung-options.h"

struct _dlg_options_t {
	GladeXML *gui;
	GnomeDialog *dialog;	
	AchtungOptions *opt;
	gboolean is_dirty;
};

void dialog_options(Window *win);
void dialog_page_setup(Window *win);
void dialog_startup(Window *win);

#endif
