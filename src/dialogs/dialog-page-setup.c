/**
 * dialog-page-setup.c:  Implements the page size and orientation dialog.
 *
 * Author:
 *  Mike Kestner <mkestner@ameritech.net>
 *
 **/

#include <config.h>
#include <gnome.h>
#include <glade/glade.h>
#include "dialogs.h"
#include "window.h"

typedef struct {
	GladeXML *gui;
	GnomeDialog *dialog;	
	SlideShowPageInfo *pi;
	GtkWidget *sl_pix;
	GtkWidget *sp_pix;
	GtkWidget *nl_pix;
	GtkWidget *np_pix;
	gint auto_change;
} dlg_page_info_t;

static GtkWidget*
achtung_load_image(char const * const name)
{
	GtkWidget *image;
	char *path;

	path = g_strconcat (ACHTUNG_PIXMAPDIR "/", name, NULL);
	image = gnome_pixmap_new_from_file (path);
	g_free (path);

	if (image)
		gtk_widget_show (image);
	
	return image;
}

static double
get_paper_height(gchar const *name, gboolean in_inches, gboolean is_portrait)
{
	double pts, val;
	gchar *unit_str;

	g_return_val_if_fail(name, 0.0);

	if (!gnome_paper_with_name(name)) {
		g_warning("Invalid Paper name detected\n");
		return 0.0;
	}

	if (is_portrait) {
		pts = gnome_paper_psheight(gnome_paper_with_name(name));
	} else {
		pts = gnome_paper_pswidth(gnome_paper_with_name(name));
	}

	if (in_inches) {
		unit_str = g_strdup("Inch");
	} else {
		unit_str = g_strdup("Centimeter");
	}

	val = gnome_paper_convert(pts, gnome_unit_with_name(unit_str));
	g_free(unit_str);
	return val;
}

static double
get_paper_width(gchar *name, gboolean in_inches, gboolean is_portrait)
{
	double pts, val;
	gchar *unit_str;

	g_return_val_if_fail(name, 0.0);

	if (!gnome_paper_with_name(name)) {
		g_warning("Invalid Paper name detected\n");
		return 0.0;
	}

	if (is_portrait) {
		pts = gnome_paper_pswidth(gnome_paper_with_name(name));
	} else {
		pts = gnome_paper_psheight(gnome_paper_with_name(name));
	}

	if (in_inches) {
		unit_str = g_strdup("Inch");
	} else {
		unit_str = g_strdup("Centimeter");
	}

	val = gnome_paper_convert(pts, gnome_unit_with_name(unit_str));
	g_free(unit_str);
	return val;
}

static void
paper_entry_cb(GtkWidget *entry, dlg_page_info_t *dpi)
{
	GtkWidget *sl_rb, *sp_rb;
	gchar *ptype;

	g_return_if_fail(entry);
	g_return_if_fail(dpi);

	sl_rb = glade_xml_get_widget(dpi->gui, "slide_land_rb");
	sp_rb = glade_xml_get_widget(dpi->gui, "slide_port_rb");

	ptype = gtk_entry_get_text(GTK_ENTRY(entry));
	if (!g_strcasecmp(ptype, "Custom")) {
		gtk_widget_set_sensitive(GTK_WIDGET(sl_rb), FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(sp_rb), FALSE);
	} else {
		GtkWidget *h_sb, *w_sb, *in_rb, *or_rb;
		double w, h;

		gtk_widget_set_sensitive(GTK_WIDGET(sl_rb), TRUE);
		gtk_widget_set_sensitive(GTK_WIDGET(sp_rb), TRUE);

		h_sb = glade_xml_get_widget(dpi->gui, "height_spin");
		w_sb = glade_xml_get_widget(dpi->gui, "width_spin");
		in_rb = glade_xml_get_widget(dpi->gui, "inch_rb");
		or_rb = glade_xml_get_widget(dpi->gui, "slide_port_rb");

		w = get_paper_width(ptype, GTK_TOGGLE_BUTTON(in_rb)->active,
				    GTK_TOGGLE_BUTTON(or_rb)->active);
		h = get_paper_height(ptype, GTK_TOGGLE_BUTTON(in_rb)->active,
				     GTK_TOGGLE_BUTTON(or_rb)->active);
		dpi->auto_change += 2;
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(w_sb), w);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(h_sb), h);
	}
	
}

static void
change_dim_cb(GtkWidget *rb, dlg_page_info_t *dpi)
{
	GtkWidget *combo;
	gchar *ptype;

	g_return_if_fail(rb);
	g_return_if_fail(dpi);

	/* Check if this is an automatically generated change. */
	if (dpi->auto_change > 0) {
		dpi->auto_change--;
		return;
	}

	combo = glade_xml_get_widget(dpi->gui, "paper_combo");

	ptype = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(combo)->entry));

	/* Only update the combo if it isn't already set to custom */
	if (g_strcasecmp(ptype, "Custom"))
		gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(combo)->entry),
				   "Custom"); 
}

static void
orient_slide_cb(GtkWidget *rb, dlg_page_info_t *dpi)
{
	GtkWidget *w_sb, *h_sb;
	double w, h;

	g_return_if_fail(rb);
	g_return_if_fail(dpi);

	w_sb = glade_xml_get_widget(dpi->gui, "width_spin");
	h_sb = glade_xml_get_widget(dpi->gui, "height_spin");

	w = gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(w_sb));
	h = gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(h_sb));

	dpi->auto_change += 2;
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(w_sb), h);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(h_sb), w);

	if (GTK_TOGGLE_BUTTON(rb)->active) {
		gtk_widget_hide(GTK_WIDGET(dpi->sl_pix));
		gtk_widget_show(GTK_WIDGET(dpi->sp_pix));
	} else {
		gtk_widget_hide(GTK_WIDGET(dpi->sp_pix));
		gtk_widget_show(GTK_WIDGET(dpi->sl_pix));
	}
}

static void
orient_notes_cb(GtkWidget *rb, dlg_page_info_t *dpi)
{
	g_return_if_fail(rb);
	g_return_if_fail(dpi);

	if (GTK_TOGGLE_BUTTON(rb)->active) {
		gtk_widget_hide(GTK_WIDGET(dpi->nl_pix));
		gtk_widget_show(GTK_WIDGET(dpi->np_pix));
	} else {
		gtk_widget_hide(GTK_WIDGET(dpi->np_pix));
		gtk_widget_show(GTK_WIDGET(dpi->nl_pix));
	}
}

static void
change_unit_cb(GtkWidget *rb, dlg_page_info_t *dpi)
{
	GtkWidget *combo;
	gchar *ptype;

	g_return_if_fail(rb);
	g_return_if_fail(dpi);

	combo = glade_xml_get_widget(dpi->gui, "paper_combo");

	ptype = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(combo)->entry));

	/* Only update the dimensions if this is not a custom paper */
	if (g_strcasecmp(ptype, "Custom")) {
		GtkWidget *w_sb, *h_sb, *in_rb, *or_rb;
		double w, h;

		w_sb = glade_xml_get_widget(dpi->gui, "width_spin");
		h_sb = glade_xml_get_widget(dpi->gui, "height_spin");
		in_rb = glade_xml_get_widget(dpi->gui, "inch_rb");
		or_rb = glade_xml_get_widget(dpi->gui, "slide_port_rb");
		if (!h_sb ||!w_sb || !in_rb || !or_rb) {
			g_warning("Corruption detected in page-setup.glade\n");
			return;
		}

		w = get_paper_width(ptype, GTK_TOGGLE_BUTTON(in_rb)->active,
				    GTK_TOGGLE_BUTTON(or_rb)->active);
		h = get_paper_height(ptype, GTK_TOGGLE_BUTTON(in_rb)->active,
				     GTK_TOGGLE_BUTTON(or_rb)->active);
		dpi->auto_change += 2;
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(w_sb), w);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(h_sb), h);
	}
}

static gboolean
prepare_dlg(dlg_page_info_t *dpi)
{
	GtkWidget *combo, *num_sb, *np_rb, *sl_rb, *sp_rb, *in_rb, *ht_sb, 
		  *wid_sb, *sl_box, *nt_box, *nl_rb, *cm_rb;
	GList *pl;

	g_return_val_if_fail(dpi, FALSE);

	/* Get handles for the necessary widgets */
	wid_sb 	= glade_xml_get_widget (dpi->gui, "width_spin");
	ht_sb 	= glade_xml_get_widget (dpi->gui, "height_spin");
	sp_rb 	= glade_xml_get_widget (dpi->gui, "slide_port_rb");
	sl_rb 	= glade_xml_get_widget (dpi->gui, "slide_land_rb");
	in_rb 	= glade_xml_get_widget (dpi->gui, "inch_rb");
	cm_rb 	= glade_xml_get_widget (dpi->gui, "cm_rb");
	combo 	= glade_xml_get_widget (dpi->gui, "paper_combo");
	num_sb 	= glade_xml_get_widget (dpi->gui, "slide_spin");
	np_rb = glade_xml_get_widget (dpi->gui, "note_port_rb");
	nl_rb = glade_xml_get_widget (dpi->gui, "note_land_rb");
	nt_box = glade_xml_get_widget (dpi->gui, "note_orient_hbox");
	sl_box = glade_xml_get_widget (dpi->gui, "slide_orient_hbox");
	
	if (!combo || !num_sb || !np_rb || !nl_rb || !wid_sb || !ht_sb || 
	    !sl_rb || !sp_rb || !in_rb || !cm_rb || !sl_box || !nt_box) {
		g_warning ("Corruption detected in page-setup.glade\n");
		return FALSE;
	}

	/* Set the initial dialog state */
	pl = g_list_copy(gnome_paper_name_list());
	pl = g_list_append(pl, "Custom");
	gtk_combo_set_popdown_strings(GTK_COMBO(combo), pl);

	if (!g_list_find_custom(pl, dpi->pi->paper,
				(GCompareFunc)g_strcasecmp)) {
		g_warning("Invalid paper name detected\n");
		g_free(dpi->pi->paper);
		dpi->pi->paper = g_strdup((gchar *)pl->data);
	}
	g_list_free(pl);

	gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(combo)->entry),dpi->pi->paper);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(num_sb), 
				  dpi->pi->start_page);

	if (dpi->pi->inch_units) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(in_rb), TRUE);
	} else {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cm_rb), TRUE); 
	}

	if (!g_strcasecmp(dpi->pi->paper, "Custom")) {
		gtk_widget_set_sensitive(GTK_WIDGET(sl_rb), FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(sp_rb), FALSE);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(wid_sb), 
					  dpi->pi->width);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(ht_sb), 
					  dpi->pi->height);
	} else {
		double h, w;

		h = get_paper_height(dpi->pi->paper, dpi->pi->inch_units,
				     dpi->pi->slide_portrait);
		w = get_paper_width(dpi->pi->paper, dpi->pi->inch_units,
				    dpi->pi->slide_portrait);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(wid_sb), w);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(ht_sb), h);
	}

	dpi->sl_pix = achtung_load_image("orient-horizontal.png");
	dpi->sp_pix = achtung_load_image("orient-vertical.png");
	dpi->nl_pix = achtung_load_image("orient-horizontal.png");
	dpi->np_pix = achtung_load_image("orient-vertical.png");

	gtk_box_pack_start(GTK_BOX(sl_box), dpi->sl_pix, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(sl_box), dpi->sp_pix, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(nt_box), dpi->nl_pix, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(nt_box), dpi->np_pix, 0, 0, 0);

	if (dpi->pi->slide_portrait) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(sp_rb), TRUE); 
		gtk_widget_hide(GTK_WIDGET(dpi->sl_pix));
	} else {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(sl_rb), TRUE); 
		gtk_widget_hide(GTK_WIDGET(dpi->sp_pix));
	}

	if (dpi->pi->notes_portrait) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(np_rb), TRUE); 
		gtk_widget_hide(GTK_WIDGET(dpi->nl_pix));
	} else {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(nl_rb), TRUE); 
		gtk_widget_hide(GTK_WIDGET(dpi->np_pix));
	}

	gtk_signal_connect (GTK_OBJECT(GTK_COMBO(combo)->entry), "changed",
			    GTK_SIGNAL_FUNC(paper_entry_cb), dpi);
	gtk_signal_connect (GTK_OBJECT(GTK_SPIN_BUTTON(wid_sb)->adjustment), 
			   "value_changed", GTK_SIGNAL_FUNC(change_dim_cb), 
			   dpi);
	gtk_signal_connect (GTK_OBJECT(GTK_SPIN_BUTTON(ht_sb)->adjustment), 
			    "value_changed", GTK_SIGNAL_FUNC(change_dim_cb), 
			    dpi);
	gtk_signal_connect (GTK_OBJECT(sp_rb), "toggled",
			    GTK_SIGNAL_FUNC(orient_slide_cb), dpi);
	gtk_signal_connect (GTK_OBJECT(np_rb), "toggled",
			    GTK_SIGNAL_FUNC(orient_notes_cb), dpi);
	gtk_signal_connect (GTK_OBJECT(in_rb), "toggled",
			    GTK_SIGNAL_FUNC(change_unit_cb), dpi);

	return TRUE;
}
static void
save_state(dlg_page_info_t *dpi)
{
	GtkWidget *tmp;

	g_return_if_fail(dpi);

	tmp = glade_xml_get_widget(dpi->gui, "inch_rb");
	dpi->pi->inch_units = GTK_TOGGLE_BUTTON(tmp)->active;

	tmp = glade_xml_get_widget(dpi->gui, "note_port_rb");
	dpi->pi->notes_portrait = GTK_TOGGLE_BUTTON(tmp)->active;

	tmp = glade_xml_get_widget(dpi->gui, "slide_port_rb");
	dpi->pi->slide_portrait = GTK_TOGGLE_BUTTON(tmp)->active;

	tmp = glade_xml_get_widget(dpi->gui, "slide_spin");
	dpi->pi->start_page = gtk_spin_button_get_value_as_int(
						GTK_SPIN_BUTTON(tmp));

	tmp = glade_xml_get_widget(dpi->gui, "paper_combo");
	g_free(dpi->pi->paper);
	dpi->pi->paper = g_strdup(gtk_entry_get_text(
					GTK_ENTRY(GTK_COMBO(tmp)->entry)));

	if (!g_strcasecmp(dpi->pi->paper, "Custom")) {
		tmp = glade_xml_get_widget(dpi->gui, "width_spin");
		dpi->pi->width = gtk_spin_button_get_value_as_float(
						GTK_SPIN_BUTTON(tmp));

		tmp = glade_xml_get_widget(dpi->gui, "height_spin");
		dpi->pi->height = gtk_spin_button_get_value_as_float(
						GTK_SPIN_BUTTON(tmp));
	}
}

static dlg_page_info_t*
dlg_page_info_new(SlideShow *ss)
{
	dlg_page_info_t *dpi;

	g_return_val_if_fail(ss, NULL);

	dpi = g_new0(dlg_page_info_t, 1);

	dpi->pi = &ss->page_info;

	dpi->gui = glade_xml_new (ACHTUNG_GLADEDIR "/page-setup.glade", NULL);
	if (!dpi->gui)
		return NULL;

	dpi->dialog = GNOME_DIALOG(glade_xml_get_widget (dpi->gui, "dialog"));

	if (!dpi->dialog || !prepare_dlg(dpi)) {
		gtk_object_unref(GTK_OBJECT(dpi->gui));	
		return NULL;
	}

	return dpi;
}

static void
dlg_page_info_destroy(dlg_page_info_t *dpi)
{
	gtk_object_unref(GTK_OBJECT(dpi->gui));
	g_free(dpi);
}

void
dialog_page_setup (Window *win)
{
	SlideShow *ss;
	dlg_page_info_t *dpi;
	gint retval;

	g_return_if_fail(win);
	ss = window_get_active_slideshow(win);
	g_return_if_fail(ss);

	dpi = dlg_page_info_new(ss);

	retval = window_dialog_run(win, dpi->dialog);

	if (retval == 0)
		save_state(dpi);

	if (retval != -1)
		gnome_dialog_close(dpi->dialog);

	dlg_page_info_destroy(dpi);
}
