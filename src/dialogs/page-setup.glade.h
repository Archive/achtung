/*
 * Translatable strings file generated by Glade.
 * Add this file to your project's POTFILES.in.
 * DO NOT compile it as part of your application.
 */

gchar *s = N_("Page Setup");
gchar *s = N_("_Slides sized for:");
gchar *s = N_("\n"
              "");
gchar *s = N_("Dimensions");
gchar *s = N_("_Width:");
gchar *s = N_("_Height:");
gchar *s = N_("Units");
gchar *s = N_("_inches");
gchar *s = N_("_cm");
gchar *s = N_("_Number slides from:");
gchar *s = N_("Orientation");
gchar *s = N_("Slides");
gchar *s = N_("_Portrait");
gchar *s = N_("_Landscape");
gchar *s = N_("Notes, handouts, & outline");
gchar *s = N_("P_ortrait");
gchar *s = N_("L_andscape");
