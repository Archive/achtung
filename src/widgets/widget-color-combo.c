/*
 * WidgetColorCombo: A color selector combo box
 *
 * Copyright (c) 1999, 2000 Miguel de Icaza, Joe Shaw, and Phil Schwan
 *
 * Authors: Miguel de Icaza <miguel@kernel.org>
 *          Joe Shaw <joe@off.net>
 *          Phil Schwan <pschwan@off.net>
 */
#include <config.h>
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>
#include "gtk-combo-box.h"
#include "widget-color-combo.h"

#define COLOR_PREVIEW_WIDTH 15
#define COLOR_PREVIEW_HEIGHT 15

enum {
	CHANGED,
	LAST_SIGNAL
};

static gint color_combo_signals [LAST_SIGNAL] = { 0, };

static GtkObjectClass *color_combo_parent_class;

static void
color_combo_finalize (GtkObject *object)
{
	ColorCombo *cc = COLOR_COMBO (object);

	g_free (cc->items);
	(*color_combo_parent_class->finalize) (object);
}

static void
color_combo_class_init (GtkObjectClass *object_class)
{
	object_class->finalize = color_combo_finalize;

	color_combo_parent_class = gtk_type_class (gtk_combo_box_get_type ());
	
	color_combo_signals [CHANGED] =
		gtk_signal_new (
			"changed",
			GTK_RUN_LAST,
			object_class->type,
			GTK_SIGNAL_OFFSET (ColorComboClass, changed),
			gtk_marshal_NONE__POINTER_INT,
			GTK_TYPE_NONE, 2, GTK_TYPE_POINTER, GTK_TYPE_INT);

	gtk_object_class_add_signals (object_class, color_combo_signals,
				      LAST_SIGNAL);
}

GtkType
color_combo_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"ColorCombo",
			sizeof (ColorCombo),
			sizeof (ColorComboClass),
			(GtkClassInitFunc) color_combo_class_init,
			(GtkObjectInitFunc) NULL,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gtk_combo_box_get_type (), &info);
	}

	return type;
}

static void
color_clicked (GtkWidget *button, ColorCombo *combo)
{
	int index = GPOINTER_TO_INT (gtk_object_get_user_data (GTK_OBJECT (button)));
	GnomeCanvasItem *item = combo->items [index];
	GdkColor *gdk_color;

	gtk_object_get (GTK_OBJECT (item),
			"fill_color_gdk", &gdk_color,
			NULL);

	combo->last_index = index;

	gtk_signal_emit (GTK_OBJECT (combo), color_combo_signals [CHANGED],
			 gdk_color, index);

	gnome_canvas_item_set (combo->preview_color_item,
			       "fill_color_gdk", gdk_color,
			       NULL);

	gtk_combo_box_popup_hide (GTK_COMBO_BOX (combo));
	g_free (gdk_color);
}

static gboolean
dialog_ok(GtkWidget *button, GtkWidget *window)
{
	ColorCombo *cc = gtk_object_get_data(GTK_OBJECT(window), "combo");
	gdouble color[4];
	GdkColor gdkcolor;

	g_return_val_if_fail(window, FALSE);

	gtk_color_selection_get_color(GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(window)->colorsel),
				      color);
	gdkcolor.red = (gushort)(color[0] * 0xffff);
	gdkcolor.green = (gushort)(color[1] * 0xffff);
	gdkcolor.blue = (gushort)(color[2] * 0xffff);
	gdk_colormap_alloc_color(gdk_rgb_get_cmap(), &gdkcolor, 0, 1);

	{
		gchar *color_str;
		gint index;
		GtkWidget *button;

		color_str = g_strdup_printf("rgb:%04x/%04x/%04x", gdkcolor.red,
					    gdkcolor.green, gdkcolor.blue);
		index = color_combo_add(cc, color_str, TRUE);
		g_free(color_str);

		/* Simulate a click */
		button = cc->buttons[index];
		color_clicked(button, cc);
	}

	gtk_widget_destroy(window);
	gtk_main_quit();

	return TRUE;
}

static gboolean
dialog_cancel(GtkWidget *button, GtkWidget *window)
{
	gtk_widget_destroy(window);
	gtk_main_quit();

	return TRUE;
} /* dialog_cancel */

static void
more_colors_handler(GtkWidget *button, ColorCombo *cc)
{
	GtkWidget *window;

	gtk_combo_box_popup_hide(GTK_COMBO_BOX(cc));

	window = gtk_color_selection_dialog_new(N_("Colors"));

	gtk_object_set_data(GTK_OBJECT(window), "combo", cc);

	gtk_signal_connect(GTK_OBJECT(GTK_COLOR_SELECTION_DIALOG(window)->
				      ok_button), "clicked",
			   GTK_SIGNAL_FUNC(dialog_ok), window);
	gtk_signal_connect(GTK_OBJECT(GTK_COLOR_SELECTION_DIALOG(window)->
				      cancel_button), "clicked",
			   GTK_SIGNAL_FUNC(dialog_cancel), window);

	gtk_widget_show(window);

	gtk_main();
}

/* This implements the Powerpoint 97 add functionality, where it will add up to
 * one extra row, and then start replacing them.
 *
 * It returns the index of the added button (in cc->buttons), or -1 if no
 * action was performed. */
gint
color_combo_add (ColorCombo *cc, char *color_name, gboolean dup)
{
	guint32 rgba;
	gint retval, row, col;
	GdkColor color = {0, 0, 0, 0, };
	GtkWidget *canvas, *button;

	g_return_val_if_fail(cc, -1);
	g_return_val_if_fail(color_name, -1);

	gdk_color_parse(color_name, &color);
	rgba = ((color.red & 0xff00) << 16 |
		(color.green & 0xff00) << 8 |
		(color.blue & 0xff00) |
		0xff);

	if (dup == FALSE && (retval = color_combo_lookup_color(cc, rgba)) >= 0)
		/* This color is already in the table */
		return retval;

	if (cc->buttons[cc->next_add] != NULL)
		/* Replace an item in the table */
		gtk_object_destroy(GTK_OBJECT(cc->buttons[cc->next_add]));

	cc->buttons[cc->next_add] = button = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);

	gtk_widget_push_visual (gdk_rgb_get_visual ());
	gtk_widget_push_colormap (gdk_rgb_get_cmap ());
	canvas = gnome_canvas_new ();
	gtk_widget_pop_colormap ();
	gtk_widget_pop_visual ();

	gtk_widget_set_usize (canvas, COLOR_PREVIEW_WIDTH,
			      COLOR_PREVIEW_HEIGHT);
	gtk_container_add (GTK_CONTAINER (button), canvas);

	cc->items[cc->next_add] = gnome_canvas_item_new (
		GNOME_CANVAS_GROUP (gnome_canvas_root (GNOME_CANVAS (canvas))),
		gnome_canvas_rect_get_type (),
		"x1", 0.0,
		"y1", 0.0,
		"x2", (double) COLOR_PREVIEW_WIDTH,
		"y2", (double) COLOR_PREVIEW_HEIGHT,
		"fill_color", color_name,
		NULL);

	col = cc->next_add % cc->cols;
	row = cc->next_add / cc->cols;

	gtk_table_attach(GTK_TABLE (cc->table), button,
			 col, col + 1, row, row + 1,
			 GTK_FILL, GTK_FILL, 1, 1);

	gtk_signal_connect(GTK_OBJECT (button), "clicked",
			   GTK_SIGNAL_FUNC(color_clicked), cc);
	gtk_object_set_user_data(GTK_OBJECT (button),
				 GINT_TO_POINTER (cc->next_add));

	retval = cc->next_add++;
	if (cc->next_add > cc->total)
		cc->next_add -= cc->cols;

	gtk_widget_show_all (button);
	return retval;
}

static GtkWidget *
color_table_setup (ColorCombo *cc, gboolean no_color, int ncols, int nrows,
		   char **color_names)
{
	GtkWidget *widget;
	GtkWidget *table;
	int row, col;

	cc->table = table = gtk_table_new (ncols, nrows, 0);

	if (no_color) {
		widget = gtk_button_new_with_label (_("None"));
		gtk_table_attach (GTK_TABLE (table), widget, 0, ncols, 0, 1,
				  GTK_FILL | GTK_EXPAND, 0, 0, 0);
		widget = gtk_hseparator_new();
		gtk_table_attach (GTK_TABLE (table), widget, 0, ncols, 1, 2,
				  GTK_FILL | GTK_EXPAND, 0, 0, 0);
	}

	cc->next_add = 0;
	for (row = 0; row < nrows; row++) {
		for (col = 0; col < ncols; col++) {
			int pos;

			pos = row * ncols + col;

			if (color_names [pos] == NULL) {
				/* Break out of two for-loops.  */
				row = nrows;
				break;
			}

			color_combo_add(cc, color_names[pos], TRUE);
		}
	}

	widget = gtk_hseparator_new();
	gtk_table_attach(GTK_TABLE (table), widget, 0, ncols, nrows, nrows + 1,
			 GTK_FILL | GTK_EXPAND, 0, 0, 1);
	widget = gtk_button_new_with_label (_("More Colors..."));
	gtk_button_set_relief (GTK_BUTTON (widget), GTK_RELIEF_NONE);
	gtk_table_attach(GTK_TABLE (table), widget, 0, ncols, nrows+1, nrows+2,
			 GTK_FILL | GTK_EXPAND, 0, 0, 0);
	gtk_signal_connect(GTK_OBJECT (widget), "clicked",
			   GTK_SIGNAL_FUNC(more_colors_handler), cc);

	gtk_widget_show_all (table);
	return table;
}

static void
emit_change (GtkWidget *button, ColorCombo *cc)
{
	GdkColor *color = NULL;

	if (cc->last_index != -1) {
		gtk_object_get (GTK_OBJECT (cc->items [cc->last_index]),
				"fill_color_gdk", &color,
				NULL);
	}

	gtk_signal_emit (GTK_OBJECT (cc), color_combo_signals [CHANGED], color,
			 cc->last_index);

	if (color)
		g_free (color);
}

void
color_combo_construct (ColorCombo *cc, const char **icon, gboolean no_color,
		       int ncols, int nrows, char **color_names)
{
	GdkPixbuf *image;

	g_return_if_fail (cc != NULL);
	g_return_if_fail (IS_COLOR_COMBO (cc));
	g_return_if_fail (color_names != NULL);

	/*
	 * Our button with the canvas preview
	 */
	cc->preview_button = gtk_button_new ();
	gtk_widget_push_visual (gdk_rgb_get_visual ());
	gtk_widget_push_colormap (gdk_rgb_get_cmap ());
	cc->preview_canvas = GNOME_CANVAS (gnome_canvas_new ());
	gtk_widget_pop_colormap ();
	gtk_widget_pop_visual ();

	image = gdk_pixbuf_new_from_xpm_data(icon);

	cc->cols = ncols;
	cc->rows = nrows + 1; /* Powerpoint-esque expansion */
	cc->total = cc->cols * cc->rows - 1;

	/* Allocate an extra row for Powerpoint-esque expansion */
	cc->buttons = g_malloc0(sizeof(GtkWidget *) * ncols * (nrows + 1));
	cc->items = g_malloc0(sizeof(GnomeCanvasItem *) * ncols * (nrows + 1));

	gnome_canvas_set_scroll_region (cc->preview_canvas, 0, 0, 24, 24);

	gnome_canvas_item_new (
		GNOME_CANVAS_GROUP (gnome_canvas_root (cc->preview_canvas)),
		gnome_canvas_pixbuf_get_type (),
		"pixbuf",     image,
		"x",          0.0,
		"y",          0.0,
		"width_set",  TRUE,
		"width",      (double) gdk_pixbuf_get_width(image),
		"height_set", TRUE,
		"height",     (double) gdk_pixbuf_get_height(image),
		NULL);

	cc->preview_color_item = gnome_canvas_item_new (
		GNOME_CANVAS_GROUP (gnome_canvas_root (cc->preview_canvas)),
		gnome_canvas_rect_get_type (),
		"x1",         3.0,
		"y1",         19.0,
		"x2",         20.0,
		"y2",         22.0,
		"fill_color", color_names [0],
		NULL);
	gtk_container_add (GTK_CONTAINER (cc->preview_button),
			   GTK_WIDGET (cc->preview_canvas));
	gtk_widget_set_usize (GTK_WIDGET (cc->preview_canvas), 24, 24);
	gtk_signal_connect (GTK_OBJECT (cc->preview_button), "clicked",
			    GTK_SIGNAL_FUNC (emit_change), cc);

	/*
	 * Our table selector
	 */
	/* Add an extra row for the Powerpoint-esque expansion and another
	 * row for the "More Colors..." button, two for separators */
	cc->color_table = color_table_setup (cc, no_color, ncols, nrows + 4,
					     color_names);

	gtk_widget_show_all (cc->preview_button);

	gtk_combo_box_construct (GTK_COMBO_BOX (cc),
				 cc->preview_button,
				 cc->color_table);
}

GtkWidget *
color_combo_new_with_vals (const char **icon, gboolean no_color,
			   int ncols, int nrows, char **color_names)
{
	ColorCombo *cc;

	g_return_val_if_fail (icon != NULL, NULL);
	g_return_val_if_fail (color_names != NULL, NULL);

	cc = gtk_type_new (color_combo_get_type ());

	color_combo_construct (cc, icon, no_color, ncols, nrows, color_names);

	return GTK_WIDGET (cc);
}

#if 0
/*
 * this list of colors should match the Excel 2000 list of colors
 */
static char *default_colors [] = {
	"rgb:0/0/0",
	"rgb:FF/FF/FF",
	"rgb:FF/0/0",
	"rgb:0/FF/0",

	"rgb:0/0/FF",
	"rgb:FF/FF/0",
	"rgb:FF/0/FF",
	"rgb:0/FF/FF",

	"rgb:80/0/0",
	"rgb:0/80/0",
	"rgb:0/0/80",
	"rgb:80/80/0",

	"rgb:80/0/80",
	"rgb:0/80/80",
	"rgb:c0/c0/c0",
	"rgb:80/80/80",

	"rgb:99/99/FF",
	"rgb:99/33/66",
	"rgb:FF/FF/CC",
	"rgb:CC/FF/FF",

	"rgb:66/0/66",
	"rgb:FF/80/80",
	"rgb:0/66/CC",
	"rgb:CC/CC/FF",

	"rgb:0/0/80",
	"rgb:FF/0/FF",
	"rgb:FF/FF/0",
	"rgb:0/FF/FF",

	"rgb:80/0/80",
	"rgb:80/0/0",
	"rgb:0/80/80",
	"rgb:0/0/FF",

	"rgb:0/CC/FF",
	"rgb:CC/FF/FF",
	"rgb:CC/FF/CC",
	"rgb:FF/FF/99",

	"rgb:99/CC/FF",
	"rgb:FF/99/CC",
	"rgb:CC/99/FF",
	"rgb:FF/CC/99",

	"rgb:33/66/FF",
	"rgb:33/CC/CC",
	"rgb:99/CC/0",
	"rgb:FF/CC/0",

	"rgb:FF/99/0",
	"rgb:FF/66/0",
	"rgb:66/66/99",
	"rgb:96/96/96",

	"rgb:0/33/66",
	"rgb:33/99/66",
	"rgb:0/33/0",
	"rgb:33/33/0",

	"rgb:99/33/0",
	"rgb:99/33/66",
	"rgb:33/33/99",
	"rgb:33/33/33",
	NULL
};
#else
/* And _these_ colours should match the Powerpoint 97 list of colours (I'll
 * update the list if necessary as soon as I update my Powerpoint) */
static char *default_colors [] = {
	"rgb:FF/FF/FF",
	"rgb:0/0/0",
	"rgb:80/80/80",
	"rgb:0/0/0",

	"rgb:0/BE/54",
	"rgb:0/0/BE",
	"rgb:BE/80/FF", /* This one is off a bit, I think */
	"rgb:BE/BE/BE",

	NULL
};
#endif

GtkWidget *
color_combo_new (const char **icon)
{
	return color_combo_new_with_vals (icon, FALSE, 8, 1, default_colors);
}

void
color_combo_select_color (ColorCombo *cc, int idx)
{
	GdkColor *color;
	g_return_if_fail (cc != NULL);
	g_return_if_fail (IS_COLOR_COMBO (cc));
	g_return_if_fail (idx <= cc->total);

	gtk_object_get (GTK_OBJECT (cc->items [idx]),
			"fill_color_gdk", &color,
			NULL);

	gnome_canvas_item_set (cc->preview_color_item,
			       "fill_color_gdk", color,
			       NULL);
	g_free (color);
}

gint
color_combo_lookup_color (ColorCombo *cc, guint32 color)
{
	guint32 i;

	for (i = 0; i < cc->total; i++) {
		/* FIXME: need to add a readable arg for fill_color to the
		 * canvas item! */
		if (cc->items[i] == NULL)
			continue;

		if (GNOME_CANVAS_RE(cc->items[i])->fill_color == color)
			return i;
	}
	return -1;
}
