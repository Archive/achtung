/*
 * slide-view.c: Implementation of the SlideView class. This view embeds an
 * 		 EmbeddableSlideView to display and edit slides within the
 *		 Achtung application GUI.
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Mike Kestner, 
 *			    Helix Code, Inc., the Puffin Group, Inc., and 
 *			    Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@linuxcare.com>
 *	    Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <gnome.h>
#include "idl/Achtung.h"
#include "achtung.h"
#include "achtung-embeddable.h"
#include "config.h"
#include "menu.h"
#include "slide.h"
#include "slideshow.h"
#include "slide-view.h"

static ViewClass *parent_class;

static void
slide_view_destroy(GtkObject *object)
{
	SlideView *view;
	CORBA_Environment ev;

	g_return_if_fail(object);
	g_return_if_fail(IS_SLIDE_VIEW(object));

	ACHTUNG_ENTRY;

	view = SLIDE_VIEW(object);

	CORBA_exception_init(&ev);

	bonobo_object_unref(BONOBO_OBJECT(view->client_site));

	Bonobo_Unknown_unref(view->server, &ev); 

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy) (object);

	ACHTUNG_EXIT;
} 

static void
slide_view_class_init(SlideViewClass *klass)
{
	GtkObjectClass *object_class;
	ViewClass *view_class;

	ACHTUNG_ENTRY;

	object_class = (GtkObjectClass *) klass;
	view_class = (ViewClass *) klass;

	parent_class = gtk_type_class(view_get_type());

	object_class->destroy = slide_view_destroy;

	ACHTUNG_EXIT;
} 

static GtkWidget*
slide_view_activate(View *view)
{
	ACHTUNG_ENTRY;

	g_return_val_if_fail(view, NULL);

	ACHTUNG_EXIT;

	return view->view_widget;
} 

static void
slide_view_deactivate(View *view)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	slide_view_destroy(GTK_OBJECT(view));

	ACHTUNG_EXIT;
} 

static void
slide_view_got_focus(View *view)
{
	gint idx;
	Slide *slide;
	SlideShow *ss;
	CORBA_Environment ev;

	g_return_if_fail(view);
	slide = view->current_slide;
	g_return_if_fail(slide);
	ss = slide->slideshow;
	g_return_if_fail(ss);

	ACHTUNG_ENTRY;

	idx = slideshow_get_index(ss, slide);

	CORBA_exception_init(&ev);

	GNOME_Achtung_Slide_setSlide(SLIDE_VIEW(view)->corba_view, 
				     idx, &ev);

	if(ev._major != CORBA_NO_EXCEPTION) {
		printf("Exception while setting slide in slide view.\n");
		if(ev._repo_id) {
			printf("repo_id=%s\n", ev._repo_id);
		} else {
			printf("Null repo_id.");
		}
	}

	gtk_signal_connect(GTK_OBJECT(slide), "slide_removed",
		GTK_SIGNAL_FUNC(view_remove_slide), view);

	ACHTUNG_EXIT;
} 

static void
slide_view_lost_focus(View *view)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	gtk_signal_disconnect_by_func(GTK_OBJECT(view->current_slide),
		GTK_SIGNAL_FUNC(view_remove_slide), view);

	ACHTUNG_EXIT;
} 

static void
slide_view_init(GtkObject *object)
{
	SlideView *slide_view;
	View *view;

	g_return_if_fail(object);

	ACHTUNG_ENTRY;

	slide_view = SLIDE_VIEW(object);
	view = VIEW(object);

	slide_view->client_site = NULL;
	slide_view->server = CORBA_OBJECT_NIL;
	slide_view->corba_view = CORBA_OBJECT_NIL;

	view->activate = slide_view_activate;
	view->deactivate = slide_view_deactivate;
	view->got_focus = slide_view_got_focus;
	view->lost_focus = slide_view_lost_focus;

	ACHTUNG_EXIT;
} 

GtkType
slide_view_get_type(void)
{
	static GtkType slide_view_type = 0;

	ACHTUNG_ENTRY;

	if (!slide_view_type) {
		static const GtkTypeInfo slide_view_info = {
			"SlideView",
			sizeof(SlideView),
			sizeof(SlideViewClass),
			(GtkClassInitFunc) slide_view_class_init,
			(GtkObjectInitFunc) slide_view_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		slide_view_type = gtk_type_unique(view_get_type(),
						  &slide_view_info);
	}

	ACHTUNG_EXIT;

	return slide_view_type;
} 

static void
client_site_sys_exception_cb(BonoboObject *client, CORBA_Object cobject,
			     CORBA_Environment *ev, SlideView *view)
{
	gtk_object_unref(GTK_OBJECT(view));
}

static gboolean
slide_view_embed_slide(SlideView *view, Window *win)
{
	BonoboObjectClient *client;
	gchar *cs_name;
	static gint count = 0;

	g_return_val_if_fail(view, FALSE);
	g_return_val_if_fail(win, FALSE);

	ACHTUNG_ENTRY;

	view->client_site = bonobo_client_site_new(win->container);

	view->server = bonobo_object_corba_objref(BONOBO_OBJECT(
			   achtung_embeddable_new(VIEW(view)->slideshow)));

	client = bonobo_object_client_from_corba(view->server);

	if(client == NULL) {
		gtk_object_unref(GTK_OBJECT(view));
		return FALSE;
	}

	if(!bonobo_client_site_bind_embeddable(view->client_site, client)) {
		gtk_object_unref(GTK_OBJECT(view));
		return FALSE;
	}

	cs_name = g_strdup_printf("Slide%d", count++);
	bonobo_item_container_add(win->container, cs_name,
				  BONOBO_OBJECT(view->client_site));
	g_free(cs_name);

	gtk_signal_connect(GTK_OBJECT(view->client_site), "system_exception",
			   GTK_SIGNAL_FUNC(client_site_sys_exception_cb), view);

	ACHTUNG_EXIT;

	return TRUE;
}

static void
view_frame_activated_cb (BonoboViewFrame *view_frame, gboolean activated, 
			 SlideView *view)
{
	if(!activated) {
		g_warning("Unexpected deactivation of Slide View");
		gtk_object_unref(GTK_OBJECT(view));
	}
}

static void
view_frame_sys_exception_cb(BonoboViewFrame *view_frame, CORBA_Object cobject,
 			    CORBA_Environment *ev, SlideView *view)
{
	gtk_object_unref(GTK_OBJECT(view));
}

static gboolean
slide_view_create_view_frame(SlideView *view, BonoboUIContainer *uic)
{
	BonoboViewFrame *view_frame;
	View *v;
	Bonobo_View corba_view;
	CORBA_Environment ev;

	g_return_val_if_fail(view, FALSE);

	ACHTUNG_ENTRY;

	v = VIEW(view);

	view_frame = bonobo_client_site_new_view_full(view->client_site,
				bonobo_object_corba_objref(BONOBO_OBJECT(uic)),
				FALSE, TRUE);

	g_return_val_if_fail(view_frame, FALSE);

	gtk_signal_connect(GTK_OBJECT(view_frame), "system_exception",
			   view_frame_sys_exception_cb, view);

	gtk_signal_connect(GTK_OBJECT(view_frame), "activated",
			   view_frame_activated_cb, view);

	bonobo_view_frame_view_activate(view_frame);

	corba_view = bonobo_view_frame_get_view(view_frame);

	CORBA_exception_init(&ev);
	view->corba_view = Bonobo_Unknown_queryInterface(
		corba_view, "IDL:GNOME/Achtung/Slide:1.0", &ev);
	g_return_val_if_fail(view->corba_view != CORBA_OBJECT_NIL, FALSE);
	CORBA_exception_free(&ev);

	v->view_widget = bonobo_view_frame_get_wrapper(view_frame);

	ACHTUNG_EXIT;

	return TRUE;
}

SlideView *
slide_view_new(Window *win, Slide *slide)
{
	SlideView *slide_view;
	View *view;

	ACHTUNG_ENTRY;

	g_return_val_if_fail(win, NULL);
	g_return_val_if_fail(slide, NULL);

	slide_view = gtk_type_new(slide_view_get_type());
	view = VIEW(slide_view);

	view->slideshow = slide->slideshow;
	view->current_slide = slide;

	if(!slide_view_embed_slide(slide_view, win)) {
		gtk_object_unref (GTK_OBJECT (slide_view));
		return NULL;
	}

	if(!slide_view_create_view_frame(slide_view, win->ui_container)) {
		gtk_object_unref (GTK_OBJECT (slide_view));
		return NULL;
	}
 
	gtk_signal_connect(GTK_OBJECT(slide_view), "destroy",
			   GTK_SIGNAL_FUNC(slide_view_destroy), NULL);

	ACHTUNG_EXIT;

	return slide_view;
}




