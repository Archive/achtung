#ifndef __ACHTUNG_POPT_H
#define __ACHTUNG_POPT_H

char *geometry = NULL;
gboolean server_only = FALSE;

struct poptOption options[] = {
	{
		"geometry",
		'\0',
		POPT_ARG_STRING,
		&geometry,
		0,
		N_("Specify the geometry of the main window"),
		N_("GEOMETRY")
	},
	{
		"server-only",
		'\0',
		POPT_ARG_NONE,
		&server_only,
		0,
		N_("Activate the object server only, do not run the GUI"),
		NULL
	},
	{
		NULL,
		'\0',
		0,
		NULL,
		0,
		NULL,
		NULL
	}
};
#endif /* __ACHTUNG_POPT_H */
