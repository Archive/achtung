/*
 * reobject.c: Implementation of the base rectangle/ellipse class. It has no
 * actual use otherwise.
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "achobject.h"
#include "reobject.h"
#include "slide.h"

static void reobject_class_init(REObjectClass *klass);
static void reobject_init(GtkObject *object);
static void reobject_set_arg(GtkObject *object, GtkArg *arg, guint arg_id);
static void reobject_get_arg(GtkObject *object, GtkArg *arg, guint arg_id);

/* Object argument IDs */
enum {
	ARG_0,
	ARG_FILL_COLOR,
	ARG_OUTLINE_COLOR
};

static AchObjectClass *parent_class;

GtkType
reobject_get_type(void)
{
	static GtkType reobject_type = 0;

	if (!reobject_type) {
		static const GtkTypeInfo reobject_info = {
			"REObject",
			sizeof(REObject),
			sizeof(REObjectClass),
			(GtkClassInitFunc) reobject_class_init,
			(GtkObjectInitFunc) reobject_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		reobject_type = gtk_type_unique(achobject_get_type(),
						  &reobject_info);
	}

	return reobject_type;
} /* reobject_get_type */

/* Class initialization function for ReObject */
static void
reobject_class_init(REObjectClass *klass)
{
	GtkObjectClass *object_class;
	AchObjectClass *achobject_class;

	object_class = (GtkObjectClass *) klass;
	achobject_class = (AchObjectClass *) klass;
	
	parent_class = gtk_type_class(achobject_get_type());

	gtk_object_add_arg_type("REObject::fill_color",
				GTK_TYPE_INT, GTK_ARG_READWRITE,
				ARG_FILL_COLOR);
	gtk_object_add_arg_type("REObject::outline_color",
				GTK_TYPE_INT, GTK_ARG_READWRITE,
				ARG_OUTLINE_COLOR);

	object_class->set_arg = reobject_set_arg;
	object_class->get_arg = reobject_get_arg;
} /* reobject_class_init */

/* REObject intialization function */
static void
reobject_init(GtkObject *object)
{
	AchObject *aobj;
	REObject *obj;

	g_return_if_fail(object);

	aobj = ACHOBJECT(object);
	obj = REOBJECT(object);

	aobj->x1 = 0.0;
	aobj->y1 = 0.0;
	aobj->x2 = 0.0;
	aobj->y2 = 0.0;
	aobj->slide = NULL;

	obj->fill_color = 0xff;
	obj->outline_color = 0xff;
} /* reobject_init */

static void
reobject_set_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
	REObject *obj;

	g_return_if_fail(object);
	g_return_if_fail(IS_REOBJECT(object));

	obj = REOBJECT(object);

	switch (arg_id) {
	case ARG_FILL_COLOR:
		obj->fill_color = GTK_VALUE_INT(*arg);
		break;

	case ARG_OUTLINE_COLOR:
		obj->outline_color = GTK_VALUE_INT(*arg);
		break;

	default:
		return;
	}

	achobject_changed(ACHOBJECT(obj));
} /* reobject_set_arg */

static void
reobject_get_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
	REObject *obj;

	g_return_if_fail(object);
	g_return_if_fail(IS_REOBJECT(object));

	obj = REOBJECT(object);

	switch (arg_id) {
	case ARG_FILL_COLOR:
		GTK_VALUE_INT(*arg) = obj->fill_color;
		break;

	case ARG_OUTLINE_COLOR:
		GTK_VALUE_INT(*arg) = obj->outline_color;
		break;

	default:
		arg->type = GTK_TYPE_INVALID;
		break;
	}
} /* reobject_get_arg */
