/*
 * slide-view-popup.c: Popup menu routines and color/font selector hacks
 * for slide views.
 *
 * Copyright (c) 1999, 2000 Phil Schwan, Joe Shaw, the Puffin Group, Inc.,
 *               Linuxcare, Inc., and Helix Code, Inc.
 *
 * Authors: Phil Schwan <pschwan@off.net>
 *          Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "achtung.h"
#include "achtung-util.h"
#include "slide.h"
#include "slideshow.h"
#include "view.h"
#include "slide-view.h"
#include "background.h"
#include "file.h"
#include "popup.h"
#include "slide-view-popup.h"

static gboolean
_achobject_menu_fill_color_ok(GtkWidget *button, GtkWidget *window)
{
	CanvasObject *cobj = gtk_object_get_data(GTK_OBJECT(window), "cobj");
	gdouble color[4];
	GdkColor gdkcolor;

	g_return_val_if_fail(window, FALSE);
	g_return_val_if_fail(cobj, FALSE);
	g_return_val_if_fail(cobj->obj, FALSE);

	gtk_color_selection_get_color
		(GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(window)->colorsel),
		 color);
	gdkcolor.red = (gushort)(color[0] * 65535.0);
	gdkcolor.green = (gushort)(color[1] * 65535.0);
	gdkcolor.blue = (gushort)(color[2] * 65535.0);
	gdk_colormap_alloc_color(gdk_rgb_get_cmap(), &gdkcolor, 0, 1);

	gtk_object_set(GTK_OBJECT(cobj->obj), "fill_color",
	       achtung_gdkcolor_to_rgba(&gdkcolor),
	       NULL);

	gtk_widget_destroy(window);
	gtk_main_quit();

	return TRUE;
} /* _achobject_menu_fill_color_ok */

static gboolean
_achobject_menu_line_color_ok(GtkWidget *button, GtkWidget *window)
{
	CanvasObject *cobj = gtk_object_get_data(GTK_OBJECT(window), "cobj");
	gdouble color[4];
	GdkColor gdkcolor;

	g_return_val_if_fail(window, FALSE);
	g_return_val_if_fail(cobj, FALSE);
	g_return_val_if_fail(cobj->obj, FALSE);

	gtk_color_selection_get_color
		(GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(window)->colorsel),
		 color);
	gdkcolor.red = (gushort)(color[0] * 65535.0);
	gdkcolor.green = (gushort)(color[1] * 65535.0);
	gdkcolor.blue = (gushort)(color[2] * 65535.0);
	gdk_colormap_alloc_color(gdk_rgb_get_cmap(), &gdkcolor, 0, 1);

	if (IS_LINEOBJECT(cobj->obj)) {
	gtk_object_set(GTK_OBJECT(cobj->obj), "color",
	       achtung_gdkcolor_to_rgba(&gdkcolor),
	       NULL);
	}

	if (IS_REOBJECT(cobj->obj)) {
	gtk_object_set(GTK_OBJECT(cobj->obj), "outline_color",
		achtung_gdkcolor_to_rgba(&gdkcolor),
		NULL);
	}	

	gtk_widget_destroy(window);
	gtk_main_quit();

	return TRUE;
} /* _achobject_menu_line_color_ok */

static gboolean
dialog_cancel(GtkWidget *button, GtkWidget *window)
{
	gtk_widget_destroy(window);
	gtk_main_quit();

	return TRUE;
} /* dialog_cancel */

static gboolean
achobject_menu_fill_color_handler(GtkWidget *menuitem, CanvasObject *cobj)
{
	GtkWidget *window;
	gdouble color[4];
	GdkColor gdkcolor;

	g_return_val_if_fail(cobj, FALSE);
	g_return_val_if_fail(cobj->obj, FALSE);

	window = gtk_color_selection_dialog_new(N_("Color Selection"));

	gtk_object_set_data(GTK_OBJECT(window), "cobj", cobj);

	gtk_signal_connect(GTK_OBJECT(GTK_COLOR_SELECTION_DIALOG(window)->
				      ok_button), "clicked",
			   GTK_SIGNAL_FUNC(_achobject_menu_fill_color_ok),
			   window);
	gtk_signal_connect(GTK_OBJECT(GTK_COLOR_SELECTION_DIALOG(window)->
				      cancel_button), "clicked",
			   GTK_SIGNAL_FUNC(dialog_cancel), window);

	/* This is a fucking debacle */
	if (IS_REOBJECT(cobj->obj))
		gdkcolor.pixel = REOBJECT(cobj->obj)->fill_color;

	gdk_color_context_query_color(cobj->view->canvas->cc,
				      &gdkcolor);

	color[0] = gdkcolor.red / 65535.0;
	color[1] = gdkcolor.green / 65535.0;
	color[2] = gdkcolor.blue / 65535.0;
	color[3] = 1.0;

	gtk_color_selection_set_color
		(GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(window)->colorsel),
		 color);

	gtk_widget_show(window);

	gtk_main();

	return TRUE;
} /* achobject_menu_fill_color_handler */

static gboolean
achobject_menu_line_color_handler(GtkWidget *menuitem, CanvasObject *cobj)
{
	GtkWidget *window;
	gdouble color[4];
	GdkColor gdkcolor;

	g_return_val_if_fail(cobj, FALSE);
	g_return_val_if_fail(cobj->obj, FALSE);

	window = gtk_color_selection_dialog_new(N_("Color Selection"));

	gtk_object_set_data(GTK_OBJECT(window), "cobj", cobj);

	gtk_signal_connect(GTK_OBJECT(GTK_COLOR_SELECTION_DIALOG(window)->
				      ok_button), "clicked",
			   GTK_SIGNAL_FUNC(_achobject_menu_line_color_ok),
			   window);
	gtk_signal_connect(GTK_OBJECT(GTK_COLOR_SELECTION_DIALOG(window)->
				      cancel_button), "clicked",
			   GTK_SIGNAL_FUNC(dialog_cancel), window);

	/* This is a fucking debacle */
	if (IS_REOBJECT(cobj->obj))
		gdkcolor.pixel = REOBJECT(cobj->obj)->outline_color;

	if (IS_LINEOBJECT(cobj->obj))
		gdkcolor.pixel = LINEOBJECT(cobj->obj)->color;

	gdk_color_context_query_color(cobj->view->canvas->cc,
				      &gdkcolor);

	color[0] = gdkcolor.red / 65535.0;
	color[1] = gdkcolor.green / 65535.0;
	color[2] = gdkcolor.blue / 65535.0;
	color[3] = 1.0;

	gtk_color_selection_set_color
		(GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(window)->colorsel),
		 color);

	gtk_widget_show(window);

	gtk_main();

	return TRUE;
} /* achobject_menu_line_color_handler */

static void
order_front(GtkWidget *menuitem, CanvasObject *cobj)
{
	slide_raise_achobject_to_top(cobj->obj);
} /* order_front */

static void
order_back(GtkWidget *menuitem, CanvasObject *cobj)
{
	slide_lower_achobject_to_bottom(cobj->obj);
} /* order_back */

static void
order_forward_one(GtkWidget *menuitem, CanvasObject *cobj)
{
	slide_raise_achobject(cobj->obj);
} /* order_forward_one */

static void
order_backward_one(GtkWidget *menuitem, CanvasObject *cobj)
{
	slide_lower_achobject(cobj->obj);
} /* order_backward_one */

static GnomeUIInfo order[] = {
	GNOMEUIINFO_ITEM_NONE(N_("Bring to Front"),
			      N_("Bring the current object to the front"),
			      order_front),
	GNOMEUIINFO_ITEM_NONE(N_("Send to Back"),
			      N_("Send the current object to the back"),
			      order_back),
	GNOMEUIINFO_ITEM_NONE(N_("Bring Forward"),
			      N_("Bring the current object forward one level"),
			      order_forward_one),
	GNOMEUIINFO_ITEM_NONE(N_("Send Backward"),
			      N_("Send the current object backward one level"),
			      order_backward_one),
	GNOMEUIINFO_END
};

/* FIXME: MOVE ME */
static void achobject_cut(GtkWidget *menuitem, CanvasObject *cobj)
{
	g_warning("Not implemented yet.");
}

gboolean
popup_achobject_menu(GdkEvent *event, CanvasObject *cobj)
{
	GnomeUIInfo *menu;
	GtkWidget *popup;

	/* Six possible menu items */
	menu = g_new0(GnomeUIInfo, 7);

	/* Items for every type of object */
	popup_add_menu_item(menu, popup_item_stock
			    (N_("Cut"),
			     N_("Cut current object"),
			     achobject_cut,
			     GNOME_STOCK_PIXMAP_CUT),
			    FALSE);
	popup_add_menu_item(menu, popup_separator(), FALSE);
	popup_add_menu_item(menu, popup_submenu("Order", order), FALSE);
	popup_add_menu_item(menu, popup_separator(), FALSE);


	if (IS_REOBJECT(cobj->obj)) {
		popup_add_menu_item(menu, popup_item
				    (N_("Fill Color"),
				     N_("Change the current item's fill color"),
				     achobject_menu_fill_color_handler, NULL),
				    FALSE);
		popup_add_menu_item(menu, popup_item
				   (N_("Outline Color"),
				    N_("Change the current item's outline color"),
				     achobject_menu_line_color_handler, NULL),
				    FALSE);
	}

	if (IS_LINEOBJECT(cobj->obj)) {
		popup_add_menu_item(menu, popup_item
				   (N_("Line Color"),
				    N_("Change the current item's outline color"),
				     achobject_menu_line_color_handler, NULL),
				    FALSE);
	}	

	popup_add_menu_item(menu, popup_end(), TRUE);

	popup = gnome_popup_menu_new(menu);
	gnome_popup_menu_do_popup(popup, NULL, NULL, (GdkEventButton *)event,
				  cobj);

	g_free(menu);

	return TRUE;
} /* popup_achobject_menu */


/***************************************************************************
 * CANVAS ROOT POPUP MENU
 ***************************************************************************/
static gboolean
_canvasroot_menu_color_ok(GtkWidget *button, GtkWidget *window)
{
	EmbeddableSlideView *view = gtk_object_get_data(GTK_OBJECT(window), 
							"slideview");
	gdouble color[4];
	GdkColor gdkcolor;

	g_return_val_if_fail(window, FALSE);
	g_return_val_if_fail(view, FALSE);

	gtk_color_selection_get_color
		(GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(window)->colorsel),
		 color);
	gdkcolor.red = (gushort)(color[0] * 65535.0);
	gdkcolor.green = (gushort)(color[1] * 65535.0);
	gdkcolor.blue = (gushort)(color[2] * 65535.0);
	gdk_colormap_alloc_color(gdk_rgb_get_cmap(), &gdkcolor, 0, 1);
	background_set_color(view->slide,
			     achtung_gdkcolor_to_rgba(&gdkcolor));
	gtk_widget_destroy(window);
	gtk_main_quit();

	return TRUE;
} /* _canvasroot_menu_color_ok */

static gboolean
canvasroot_menu_color_handler(GtkWidget *menuitem, EmbeddableSlideView *view)
{
	GtkWidget *window;
	gdouble color[4];
	GdkColor gdkcolor;

	g_return_val_if_fail(view, FALSE);

	window = gtk_color_selection_dialog_new(N_("Color Selection"));

	gtk_object_set_data(GTK_OBJECT(window), "slideview", view);

	gtk_signal_connect(GTK_OBJECT(GTK_COLOR_SELECTION_DIALOG(window)->
				      ok_button), "clicked",
			   GTK_SIGNAL_FUNC(_canvasroot_menu_color_ok), window);
	gtk_signal_connect(GTK_OBJECT(GTK_COLOR_SELECTION_DIALOG(window)->
				      cancel_button), "clicked",
			   GTK_SIGNAL_FUNC(dialog_cancel), window);

	/* This is a fucking debacle */
	gdkcolor.pixel = view->slide->bg_info->color;
	gdk_color_context_query_color(view->canvas->cc,
				      &gdkcolor);

	color[0] = gdkcolor.red / 65535.0;
	color[1] = gdkcolor.green / 65535.0;
	color[2] = gdkcolor.blue / 65535.0;
	color[3] = 1.0;

	gtk_color_selection_set_color(GTK_COLOR_SELECTION(
		GTK_COLOR_SELECTION_DIALOG(window)->colorsel), color);

	gtk_widget_show(window);

	gtk_main();

	return TRUE;
} /* canvasroot_menu_color_handler */

static void
canvasroot_set_color(GtkWidget *menuitem, EmbeddableSlideView *view)
{
	canvasroot_menu_color_handler(menuitem, view);
} /* canvasroot_set_color */

static GdkPixbuf *
canvasroot_set_image(EmbeddableSlideView *view)
{
	GdkPixbuf *image;

	char *filename = file_selector(view->slide->slideshow, "Import Image",
				       FILE_LOAD);
	if (!filename) {
		GtkWidget *box = 
			gnome_message_box_new("Unable to load image file.",
					      GNOME_MESSAGE_BOX_ERROR,
					      GNOME_STOCK_BUTTON_OK, NULL);
		gtk_widget_show(box);
		return NULL;
	}

	image = gdk_pixbuf_new_from_file(filename);
	if (!image) {
		GtkWidget *box =
			gnome_message_box_new("Unable to load image file.",
					      GNOME_MESSAGE_BOX_ERROR,
					      GNOME_STOCK_BUTTON_OK, NULL);
		gtk_widget_show(box);
		return NULL;
	}

	return image;
} /* canvasroot_set_image_center */

static void
canvasroot_set_image_center(GtkWidget *menuitem, EmbeddableSlideView *view)
{
	GdkPixbuf *image;

	g_return_if_fail(view);

	image = canvasroot_set_image(view);
	
	if (!image)
		return;
	
	background_set_image_center(view->slide, image);

} /* canvasroot_set_image_center */

static void
canvasroot_set_image_scale(GtkWidget *menuitem, EmbeddableSlideView *view)
{
	GdkPixbuf *image;

	g_return_if_fail(view);

	image = canvasroot_set_image(view);
	
	if (!image)
		return;
	
	background_set_image_scale(view->slide, image);
} /* canvasroot_set_image_center */

static void
canvasroot_set_image_tile(GtkWidget *menuitem, EmbeddableSlideView *view)
{
	GdkPixbuf *image;

	g_return_if_fail(view);

	image = canvasroot_set_image(view);
	
	if (!image)
		return;
	
	background_set_image_tile(view->slide, image);
} /* canvasroot_set_image_tile */

gboolean
popup_canvasroot_menu(GdkEvent *event, EmbeddableSlideView *view)
{
	GnomeUIInfo *menu;
	GtkWidget *popup;
	GnomeUIInfo image[] = {
		GNOMEUIINFO_ITEM_NONE(N_("Centered"),
				      N_("Set the background to a centered image"),
				      canvasroot_set_image_center),
		GNOMEUIINFO_ITEM_NONE(N_("Scaled"),
				      N_("Set the background to a scaled image"),
				      canvasroot_set_image_scale),
		GNOMEUIINFO_ITEM_NONE(N_("Tiled"),
				      N_("Set the background to a tiled image"),
				      canvasroot_set_image_tile),
		GNOMEUIINFO_END
	};
	GnomeUIInfo background[] = {
		GNOMEUIINFO_ITEM_NONE(N_("Color"),
				      N_("Set the background color"),
				      canvasroot_set_color),
		GNOMEUIINFO_SUBTREE(N_("Image"), image),
		GNOMEUIINFO_END
	};

	/* Two menu elements */
	menu = g_new0(GnomeUIInfo, 2);

	popup_add_menu_item(menu, popup_submenu(N_("Background"), background),
			    FALSE);

	popup_add_menu_item(menu, popup_end(), TRUE);

	popup = gnome_popup_menu_new(menu);
	gnome_popup_menu_do_popup(popup, NULL, NULL, (GdkEventButton *)event, 
				  view);

	g_free(menu);

	return TRUE;
} /* popup_canvasroot_menu */
