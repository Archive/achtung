/*
 * lineobject.h: Implementation of the line and arrow objects
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_LINEOBJECT_H
#define __ACHTUNG_LINEOBJECT_H

#include <libgnome/gnome-defs.h>

#include "achobject.h"

BEGIN_GNOME_DECLS

typedef struct _LineObject      LineObject;
typedef struct _LineObjectClass LineObjectClass;
typedef struct LineObject       ArrowObject;
typedef struct LineObjectClass  ArrowObjectClass;

#define LINEOBJECT_TYPE            (lineobject_get_type ())
#define LINEOBJECT(obj)            (GTK_CHECK_CAST((obj), LINEOBJECT_TYPE, \
				    LineObject))
#define LINEOBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                    LINEOBJECT_TYPE, LineObjectClass))
#define IS_LINEOBJECT(obj)         (GTK_CHECK_TYPE ((obj), LINEOBJECT_TYPE))
#define IS_LINEOBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                    LINEOBJECT_TYPE))

#define ARROWOBJECT_TYPE            (arrowobject_get_type ())
#define ARROWOBJECT(obj)            (GTK_CHECK_CAST((obj), ARROWOBJECT_TYPE, \
				     ArrowObject))
#define ARROWOBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                     ARROWOBJECT_TYPE, ArrowObjectClass))
#define IS_ARROWOBJECT(obj)         (GTK_CHECK_TYPE ((obj), ARROWOBJECT_TYPE))
#define IS_ARROWOBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                     ARROWOBJECT_TYPE))
struct _LineObject {
	AchObject parent_object;

	/* Color of the object */
	guint32 color;

	/* Width of the line */
	double width;

	/* Arrow? */
	gboolean arrow;
};
	
struct _LineObjectClass {
	AchObjectClass parent_class;
};

/* Standard object functions */
GtkType lineobject_get_type(void);

/* Create a new LineObject */
AchObject *lineobject_new(Slide *slide, gboolean is_arrow);

/* Destroy a LineObject */
void lineobject_destroy(GtkObject *object);

#endif
