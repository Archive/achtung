#ifndef __ACHTUNG_PLUGIN_H
#define __ACHTUNG_PLUGIN_H

#include <glib.h>
#include <gmodule.h>

typedef struct _PluginData PluginData;

struct _PluginData {
	GModule *handle;
	int (*init_plugin)(PluginData *pd);
        int (*can_unload)(PluginData *pd);
        void (*cleanup_plugin)(PluginData *pd);
	char *title;
	void *private_data;
};

extern GList *plugin_list;

/* Each plugin must have this function */
extern int init_plugin(PluginData *pd);

void plugins_init(void);
PluginData *plugin_load(const gchar *filename);
void plugin_unload(PluginData *pd);
gboolean plugin_supported(void);


#endif /* ACHTUNG_PLUGIN_H */
