#ifndef __ACHTUNG_PIXMAPS_H
#define __ACHTUNG_PIXMAPS_H

#include "../pixmaps/line.xpm"
#include "../pixmaps/arrow.xpm"
#include "../pixmaps/rect.xpm"
#include "../pixmaps/oval.xpm"
#include "../pixmaps/text.xpm"
#include "../pixmaps/image.xpm"
#include "../pixmaps/bucket.xpm"
#include "../pixmaps/bonobo.xpm"

#endif /* __ACHTUNG_PIXMAPS_H */
