/*
 * present-view.h: Implementation of the PresentView Class
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_PRESENT_VIEW_H
#define __ACHTUNG_PRESENT_VIEW_H

#include <libgnome/gnome-defs.h>
#include "idl/Achtung.h"

BEGIN_GNOME_DECLS

typedef struct _PresentView      PresentView;
typedef struct _PresentViewClass PresentViewClass;

#include "view.h"

#define PRESENT_VIEW_TYPE            (present_view_get_type ())
#define PRESENT_VIEW(obj)            (GTK_CHECK_CAST((obj), \
				      PRESENT_VIEW_TYPE, PresentView))
#define PRESENT_VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                      PRESENT_VIEW_TYPE, PresentViewClass))
#define IS_PRESENT_VIEW(obj)         (GTK_CHECK_TYPE ((obj), \
				      PRESENT_VIEW_TYPE))
#define IS_PRESENT_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                      PRESENT_VIEW_TYPE))

struct _PresentView {
	View parent_object;

	BonoboClientSite 		*client_site;
	GNOME_Achtung_Slideshow 	server;
	GNOME_Achtung_Slide 		corba_view;

	GtkWidget 			*big_window;
	GtkWidget			*ebox;
	GtkWidget			*slide_wrapper;
};
	
struct _PresentViewClass {
	ViewClass parent_class;
};

/* Standard object functions */
GtkType present_view_get_type(void);

/* Create a new PresentView */
PresentView *present_view_new(Window *win, Slide *slide);
#endif
