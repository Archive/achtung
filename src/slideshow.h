/*
 * slideshow.h: Implementation of the SlideShow class
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Helix Code, Inc.,
 *                          the Puffin Group, Inc. and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@linuxcare.com>
 *
 */

#ifndef __ACHTUNG_SLIDESHOW_H
#define __ACHTUNG_SLIDESHOW_H

#include <libgnome/gnome-defs.h>

#include "config.h"

#ifdef ENABLE_BONOBO
#   include <bonobo.h>
#endif

BEGIN_GNOME_DECLS

extern GList *open_slideshows;

typedef struct _SlideShow          SlideShow;
typedef struct _SlideShowClass     SlideShowClass;

typedef struct _SlideShowPageInfo  SlideShowPageInfo;

#include "slide.h"
#include "view.h"

#define SLIDESHOW_TYPE             (slideshow_get_type ())
#define SLIDESHOW(obj)             (GTK_CHECK_CAST((obj), SLIDESHOW_TYPE, \
			            SlideShow))
#define SLIDESHOW_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), \
                                    SLIDESHOW_TYPE, SlideShowClass))
#define IS_SLIDESHOW(obj)          (GTK_CHECK_TYPE ((obj), SLIDESHOW_TYPE))
#define IS_SLIDESHOW_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), \
                                    SLIDESHOW_TYPE))

struct _SlideShowPageInfo {
	gchar 		*paper;
	gboolean	slide_portrait; /* slide orientation */
	gboolean	notes_portrait; /* notes orientation */
	gint		start_page;	/* what page number should the first
					 * page be? */
	gboolean	inch_units;
	gdouble		psheight;	/* page height in points */
	gdouble		pswidth;	/* page width in points */

	/* Remaining fields only used by custom paper format */
	gdouble		height;
	gdouble		width;
};

struct _SlideShow {
	GtkObject parent_object;

	/* Name of the slideshow, if any */
	char *name;
	/* Filename of slideshow, if any */
	char *filename;

	/* Master Slide */
	Slide *master_slide;

	/* List of all slides in the slideshow, in order */
	GList *slides;

	/* List of all AchObjects in all slides that need updating */
	GList *update_list;

	/* Slideshow defaults */
	struct {
		guint32 fill_color;
		guint32 line_color;
		guint32 background_color;
		guint32 text_color;
	} defaults;

	/* Page information */
	SlideShowPageInfo page_info;
};

struct _SlideShowClass {
	GtkObjectClass parent_class;

	void (* slide_added) (SlideShow *ss, Slide *slide);
};

/* Standard object functions */
GtkType slideshow_get_type(void);

/* Create a new SlideShow */
SlideShow * slideshow_new(const char *name);

/* Displaying a slideshow */
void slideshow_show(SlideShow *ss);
void slideshow_hide(SlideShow *ss);
SlideShow * slideshow_open(void);

/* Queueing an update for a slideshow */
void slideshow_request_update(SlideShow *ss);

/* Set the name of a SlideShow */
void slideshow_set_name(SlideShow *ss, const char *name);

void slideshow_set_filename(SlideShow *ss, const char *filename);

void slideshow_append(SlideShow *ss, Slide *slide);
void slideshow_insert(SlideShow *ss, Slide *slide, gint position);
void slideshow_move(SlideShow *ss, gint old_idx, gint new_idx);
void slideshow_remove(SlideShow *ss, Slide *slide);
void slideshow_remove_nth(SlideShow *ss, gint idx);
gint slideshow_length(SlideShow *ss);

/* If index is -1, the master slide is returned */
Slide *slideshow_get_slide(SlideShow *ss, int index);
/* Returns -1 for the master slide, -2 on error. */
int slideshow_get_index(SlideShow *ss, Slide *slide);

void slideshow_destroy(SlideShow *ss);

void slideshow_set_page_info(SlideShow *ss, SlideShowPageInfo *pi);
#endif
