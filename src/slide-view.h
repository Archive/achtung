/*
 * slide-view.h: Implementation of the individual slide view
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Mike Kestner, 
 *			    the Puffin Group, Inc., and
 *                          Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@off.net>
 *          Phil Schwan <pschwan@linuxcare.com>
 *	    Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __ACHTUNG_SLIDE_VIEW_H
#define __ACHTUNG_SLIDE_VIEW_H

#include <libgnome/gnome-defs.h>
#include <bonobo.h>
#include "idl/Achtung.h"
#include "view.h"

BEGIN_GNOME_DECLS

typedef struct _SlideView      SlideView;
typedef struct _SlideViewClass SlideViewClass;

#define SLIDE_VIEW_TYPE            (slide_view_get_type ())
#define SLIDE_VIEW(obj)            (GTK_CHECK_CAST((obj), SLIDE_VIEW_TYPE, \
			            SlideView))
#define SLIDE_VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                    SLIDE_VIEW_TYPE, SlideViewClass))
#define IS_SLIDE_VIEW(obj)         (GTK_CHECK_TYPE ((obj), SLIDE_VIEW_TYPE))
#define IS_SLIDE_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                    SLIDE_VIEW_TYPE))

struct _SlideView {
	View parent_view;

  	BonoboClientSite 		*client_site;
	GNOME_Achtung_Slideshow		server;
	GNOME_Achtung_Slide		corba_view;
};
	
struct _SlideViewClass {
	ViewClass parent_class;
};

/* Standard object functions */
GtkType slide_view_get_type(void);

SlideView *slide_view_new(Window *win, Slide *slide);

#endif
