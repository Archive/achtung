/*
 * textobject.h: Implementation of the text base class.
 *
 * Copyright (c) 1999, 2000 Phil Schwan, Joe Shaw, the Puffin Group, Inc.,
 *                          Linuxcare, Inc., and Helix Code, Inc.
 *
 * Authors: Phil Schwan <pschwan@linuxcare.com>
 *          Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_TEXTOBJECT_H
#define __ACHTUNG_TEXTOBJECT_H

#include <libgnome/gnome-defs.h>
#if 0
#include <gtktext/gtktextbuffer.h>
#endif

#include "achobject.h"
#include "slide.h"

BEGIN_GNOME_DECLS

typedef struct _TextObject      TextObject;
typedef struct _TextObjectClass TextObjectClass;

#define TEXTOBJECT_TYPE            (textobject_get_type ())
#define TEXTOBJECT(obj)            (GTK_CHECK_CAST((obj), TEXTOBJECT_TYPE, \
				    TextObject))
#define TEXTOBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                                    TEXTOBJECT_TYPE, TextObjectClass))
#define IS_TEXTOBJECT(obj)         (GTK_CHECK_TYPE ((obj), TEXTOBJECT_TYPE))
#define IS_TEXTOBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                                    TEXTOBJECT_TYPE))

struct _TextObject {
	AchObject parent_object;

#if 0
	/* Text Buffer */
	GtkTextBuffer	*buffer;
#endif

	/* Background Color of the object */
	guint32 bg_color;
};
	
struct _TextObjectClass {
	AchObjectClass parent_class;
};

AchObject * textobject_new(Slide *slide);

/* Standard object functions */
GtkType textobject_get_type(void);

#endif
