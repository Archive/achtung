/*
 * slide.h: Implementation of the Slide class
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Helix Code, Inc.,
 *                          the Puffin Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@off.net>
 *
 */

#ifndef __ACHTUNG_SLIDE_H
#define __ACHTUNG_SLIDE_H

#include <libgnome/gnome-defs.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <bonobo/bonobo-object-client.h>

BEGIN_GNOME_DECLS

typedef struct _Slide                Slide;
typedef struct _SlideClass           SlideClass;

#include "slideshow.h"

typedef   enum _BackgroundImageType  BackgroundImageType;
typedef struct _BackgroundInfo       BackgroundInfo;

#define SLIDE_TYPE            (slide_get_type ())
#define SLIDE(obj)            (GTK_CHECK_CAST((obj), SLIDE_TYPE, \
			       Slide))
#define SLIDE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                               SLIDE_TYPE, SlideClass))
#define IS_SLIDE(obj)         (GTK_CHECK_TYPE ((obj), SLIDE_TYPE))
#define IS_SLIDE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                               SLIDE_TYPE))

enum _BackgroundImageType {
	BACKGROUND_IMAGE_NONE = 0,
	BACKGROUND_IMAGE_CENTER,
	BACKGROUND_IMAGE_SCALED,
	BACKGROUND_IMAGE_TILE
};

struct _BackgroundInfo {
	guint32 color;
	BackgroundImageType image_type;
	GdkPixbuf *pixbuf;
	SlideShowPageInfo *page_info;
};

struct _Slide {
	GtkObject parent_object;

	/* Parent slideshow */
	SlideShow *slideshow;

	/* Shapes collection for this slide */
	BonoboObjectClient *shapes;

	/* Background information */
	BackgroundInfo *bg_info;

	/* Notes FIXME: Should probably become a GtkTextBuffer */
	char *notes;
};

struct _SlideClass {
	GtkObjectClass parent_class;

	void (* background_changed) (Slide *slide);
	void (* slide_removed) (Slide *slide);
	void (* slide_reordered) (Slide *slide);
};

/* Standard object functions */
GtkType slide_get_type(void);

/* Create a new Slide */
Slide *slide_new(void);

void slide_background_changed(Slide *slide);

void slide_set_slideshow(Slide *slide, SlideShow *ss);

#endif
