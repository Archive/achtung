/*
 * achtung-util.c: Various miscellaneous utility functions.
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#ifndef __ACHTUNG_UTIL_H
#define __ACHTUNG_UTIL_H

#include <glib.h>
#include <gdk/gdk.h>
#include <libgnomeui/gnome-canvas.h>

#include "achobject.h"

guint32 achtung_color_to_rgba(GnomeCanvas *canvas, char *name);
guint32 achtung_gdkcolor_to_rgba(GdkColor *color);

/* The user must free this string. */
gchar * achtung_rgba_to_string(guint32 rgba);

/* This returns a GdkColor which does not have its pixel allocated.  The user
 * is responsible for gdk_color_free()ing */
GdkColor *achtung_rgba_to_gdkcolor(guint32 rga);

void achtung_set_clipboard_object(AchObject *obj);
AchObject *achtung_get_clipboard_object(void);

#endif
