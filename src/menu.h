/*
 * menu.h: Menu and main toolbar handling routines
 *
 * Copyright (c) 1999-2001 Joe Shaw, Phil Schwan, Helix Code, Inc., the Puffin
 *                         Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@linuxcare.com>
 *
 */

#ifndef __ACHTUNG_MENU_H
#define __ACHTUNG_MENU_H

#include "window.h"

BonoboUIComponent *window_ui_component_new(Window *win);

void window_ui_component_set_cmd_sensitivity(Window *win, gboolean sense);

#endif
