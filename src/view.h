/*
 * view.h: Implementation of the base View class
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Phil Schwan, Helix Code, Inc.,
 *                          the Puffin Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@linuxcare.com>
 *
 */

#ifndef __ACHTUNG_VIEW_H
#define __ACHTUNG_VIEW_H

#include <libgnome/gnome-defs.h>

#include "config.h"

#ifdef USE_BONOBO
#   include <gnome-ui-handler.h>
#endif

BEGIN_GNOME_DECLS

typedef struct _View         View;
typedef struct _ViewClass    ViewClass;

#include "slide.h"
#include "slideshow.h"
#include "window.h"

#define VIEW_TYPE            (view_get_type ())
#define VIEW(obj)            (GTK_CHECK_CAST((obj), VIEW_TYPE, \
			      View))
#define VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                              VIEW_TYPE, ViewClass))
#define IS_VIEW(obj)         (GTK_CHECK_TYPE ((obj), VIEW_TYPE))
#define IS_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
                              VIEW_TYPE))

struct _View {
	GtkObject parent_object;

	/* Window that this view is a part of */
	Window *window;

	/* Slideshow this view displays */
	SlideShow *slideshow;

	/* Current slide */
	Slide *current_slide;

	/* The widget that is placed in the container */
	GtkWidget *view_widget;

	/* Function to call when the view is activated. Returns a widget to
	   the view */
	GtkWidget *(* activate) (View *view);
	/* Function to call when the view is deactivated. */
	void (* deactivate) (View *view);
	/* Function to call to update the view. */
	gboolean (* update) (View *view);
	/* Function to call when a new slide is given focus */
	void (* got_focus) (View *view);
	/* Function to call whena a slide loses its focus */
	void (* lost_focus) (View *view);
	/* Function to update and sensitize the menu options as necessary */
	void (* sensitize_menu_options) (View *view);
};
	
struct _ViewClass {
	GtkObjectClass parent_class;
};

/* Standard object functions */
GtkType view_get_type(void);

/* Create a new View */
View *view_new(void);
/* Destroy a view */
void view_destroy(GtkObject *object);

void view_set_slideshow(View *view, SlideShow *ss);
void view_set_slide(View *view, Slide *slide);
void view_set_slide_idx(View *view, gint idx);
void view_remove_slide(Slide *slide, View *view);
#endif
