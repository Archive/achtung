/*
 * present-view-popup.c: Popup menu routines for the presentation
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "popup.h"
#include "slideshow.h"
#include "slide.h"
#include "slide-view.h"
#include "view.h"
#include "present-view.h"
#include "present-view-popup.h"

static void
next_cmd(GtkWidget *menuitem, View *view)
{
	SlideShow *ss;
	int i;

	g_return_if_fail(view);

	ss = view->slideshow;

	i = slideshow_get_index(ss, view->current_slide);
	view_set_slide_idx(view, i+1);
} /* next_cmd */

static void
prev_cmd(GtkWidget *menuitem, View *view)
{
	SlideShow *ss;
	int i;

	g_return_if_fail(view);

	ss = view->slideshow;

	i = slideshow_get_index(ss, view->current_slide);
	view_set_slide_idx(view, i-1);
} /* prev_cmd */

static void
stop_cmd(GtkWidget *menuitem, PresentView *view)
{
	View *oldview = VIEW(view);
	SlideView *sview;
	Window *win;

	g_return_if_fail(oldview);

	ACHTUNG_ENTRY;

	win = oldview->window;

	sview = slide_view_new(win, oldview->current_slide);

	window_remove_view(win, oldview);

	window_set_view(win, VIEW(sview));

	ACHTUNG_EXIT;
} /* stop_cmd */

gboolean
popup_present_menu(GdkEvent *event, PresentView *view, int index, int length)
{
	GnomeUIInfo *menu;
	GtkWidget *popup;
	
	menu = g_new0(GnomeUIInfo, 5);

	/* gnome_popup is broken, and won't let me desensitize options.
	   Until this gets moved over to GnomeUIHandler, we just won't even
	   add them */

	if (index + 1 != length)
		popup_add_menu_item(menu, popup_item_stock(
			N_("Next"), N_("Next Slide"), next_cmd,
			GNOME_STOCK_PIXMAP_FORWARD), FALSE);

	if (index != 0)
		popup_add_menu_item(menu, popup_item_stock(
			N_("Previous"), N_("Previous Slide"), prev_cmd,
			GNOME_STOCK_PIXMAP_BACK), FALSE);

	if (index + 1 != length || index != 0)
		popup_add_menu_item(menu, popup_separator(), FALSE);

	popup_add_menu_item(menu, popup_item_stock(
		N_("Stop"), N_("Stop Slideshow"), stop_cmd,
		GNOME_STOCK_PIXMAP_STOP), FALSE);
	popup_add_menu_item(menu, popup_end(), TRUE);

	popup = gnome_popup_menu_new(menu);
	gnome_popup_menu_do_popup(popup, NULL, NULL, (GdkEventButton *) event,
				  view);
	g_free(menu);

	return TRUE;
} /* popup_present_menu */
