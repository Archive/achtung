/*
 * popup.c: General routines for popup menus
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include "popup.h"

void
popup_add_menu_item(GnomeUIInfo *menu, GnomeUIInfo menu_data, gboolean reset)
{
	static int elements = 0;

	g_return_if_fail(menu);

	elements++;
	menu[elements - 1] = menu_data;
	if (reset)
		elements = 0;
} /* popup_add_menu_item */

GnomeUIInfo
popup_item_stock(char *label, char *tooltip, gpointer callback,
		 gpointer stock_item)
{
	GnomeUIInfo item = GNOMEUIINFO_ITEM_STOCK(label, tooltip, callback,
						  stock_item);

	return item;
} /* popup_item_stock */

GnomeUIInfo
popup_item(char *label, char *tooltip, gpointer callback, gpointer xpm_data)
{
	GnomeUIInfo item = GNOMEUIINFO_ITEM(label, tooltip, callback,
					    xpm_data);

	return item;
} /* popup_item */

GnomeUIInfo
popup_submenu(char *label, GnomeUIInfo *submenu)
{
	GnomeUIInfo item = GNOMEUIINFO_SUBTREE(label, submenu);

	return item;
} /* popup_submenu */

GnomeUIInfo
popup_separator(void)
{
	GnomeUIInfo item = GNOMEUIINFO_SEPARATOR;

	return item;
} /* popup_separator */

GnomeUIInfo
popup_end(void)
{
	GnomeUIInfo item = GNOMEUIINFO_END;

	return item;
} /* popup_end */
