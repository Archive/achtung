/*
 * present-view.c: Implementation of the PresentView class
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Mike Kestner, and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *	   Mike Kestner <mkestner@ameritech.net>
 *
 */
#include <config.h>
#include <gnome.h>
#include "achobject.h"
#include "achtung.h"
#include "achtung-embeddable.h"
#include "present-view.h"
#include "present-view-popup.h"
#include "slideshow.h"
#include "slide.h"
#include "view.h"

static ViewClass *parent_class;

static void
present_view_destroy(GtkObject *object)
{
	PresentView *view;
	CORBA_Environment ev;

	g_return_if_fail(object);
	g_return_if_fail(IS_PRESENT_VIEW(object));

	ACHTUNG_ENTRY;

	view = PRESENT_VIEW(object);

	CORBA_exception_init(&ev);

	bonobo_object_unref(BONOBO_OBJECT(view->client_site));

	Bonobo_Unknown_unref(view->server, &ev);
 
	gtk_widget_destroy(view->big_window);

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy) (object);

	ACHTUNG_EXIT;
} 

static void
present_view_class_init(PresentViewClass *klass)
{
	GtkObjectClass *object_class;
	ViewClass *view_class;

	ACHTUNG_ENTRY;

	object_class = (GtkObjectClass *) klass;
	view_class = (ViewClass *) klass;
	
	parent_class = gtk_type_class(view_get_type());

	object_class->destroy = present_view_destroy;

	ACHTUNG_EXIT;
} 

static GtkWidget *
present_view_activate(View *view)
{
	PresentView *pview;
	int width, height;

	g_return_val_if_fail(view, NULL);

	ACHTUNG_ENTRY;

	pview = PRESENT_VIEW(view);

	width = gdk_screen_width();
	height = gdk_screen_height();
	
	gtk_widget_grab_focus(pview->ebox);

	view->view_widget = NULL;

	ACHTUNG_EXIT;

	return NULL;
} 

static void
present_view_deactivate(View *view)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	present_view_destroy(GTK_OBJECT(view));

	ACHTUNG_EXIT;
} 

static void
present_view_got_focus(View *view)
{
	CORBA_Environment ev;
	SlideShow *ss;
	Slide *slide;
	gint idx;

	g_return_if_fail(view);
	ss = view->slideshow;
	g_return_if_fail(ss);
	slide = view->current_slide;
	g_return_if_fail(slide);

	ACHTUNG_ENTRY;

	idx = slideshow_get_index(ss, slide);

	CORBA_exception_init(&ev);

	GNOME_Achtung_Slide_setSlide(PRESENT_VIEW(view)->corba_view, 
				     idx, &ev);

	if(ev._major != CORBA_NO_EXCEPTION) {
		printf("Exception while setting slide in present view.\n");
		if(ev._repo_id) {
			printf("repo_id=%s\n", ev._repo_id);
		} else {
			printf("Null repo_id.");
		}
	}

	gtk_signal_connect(GTK_OBJECT(slide), "slide_removed",
		GTK_SIGNAL_FUNC(view_remove_slide), view);

	ACHTUNG_EXIT;

} 

static void
present_view_lost_focus(View *view)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	gtk_signal_disconnect_by_func(GTK_OBJECT(view->current_slide),
		GTK_SIGNAL_FUNC(view_remove_slide), view);

	ACHTUNG_EXIT;
} 

static void
present_view_init(GtkObject *object)
{
	PresentView *present_view;
	View *view;

	g_return_if_fail(object);

	ACHTUNG_ENTRY;

	present_view = PRESENT_VIEW(object);
	view = VIEW(object);

	present_view->server = NULL;
	present_view->client_site = NULL;
	present_view->corba_view = NULL;

	present_view->big_window = NULL;

	view->update = NULL;
	view->activate = present_view_activate;
	view->deactivate = present_view_deactivate;
	view->got_focus = present_view_got_focus;
	view->lost_focus = present_view_lost_focus;
	view->sensitize_menu_options = NULL;

	ACHTUNG_EXIT;
} 

GtkType
present_view_get_type(void)
{
	static GtkType present_view_type = 0;

	ACHTUNG_ENTRY;
	if (!present_view_type) {
		static const GtkTypeInfo present_view_info = {
			"PresentView",
			sizeof(PresentView),
			sizeof(PresentViewClass),
			(GtkClassInitFunc) present_view_class_init,
			(GtkObjectInitFunc) present_view_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		present_view_type = gtk_type_unique(view_get_type(),
						    &present_view_info);
	}

	ACHTUNG_EXIT;

	return present_view_type;
} 

static gboolean
present_button_press(GtkWidget *canvas, GdkEvent *event, PresentView *view)
{
	int index;
	int length;
	View *v;

	ACHTUNG_ENTRY;

	v = VIEW(view);

	index = slideshow_get_index(v->slideshow, v->current_slide);
	length = slideshow_length(v->slideshow);

	if (event->button.button == 1) {
		/* Index is 0-indexed, length starts at 1, hence +2 */
		if (index + 2 > length)
			gdk_beep();
		else
			view_set_slide_idx(VIEW(view), index + 1);
		ACHTUNG_EXIT;
		return TRUE;
	} else if (event->button.button == 3) {
		popup_present_menu(event, view, index, length);
		ACHTUNG_EXIT;
		return TRUE;
	}

	ACHTUNG_EXIT;

	return FALSE;
} 

static void
client_site_sys_exception_cb(BonoboObject *client, CORBA_Object cobject,
			     CORBA_Environment *ev, PresentView *view)
{
	gtk_object_unref(GTK_OBJECT(view));
}

static gboolean
present_view_embed_slide(PresentView *view, Window *win)
{
	BonoboObjectClient *client;
	gchar *cs_name;
	static gint count = 0;

	g_return_val_if_fail(view, FALSE);
	g_return_val_if_fail(win, FALSE);

	ACHTUNG_ENTRY;

	view->client_site = bonobo_client_site_new(win->container);

	view->server = bonobo_object_corba_objref(BONOBO_OBJECT(
			   achtung_embeddable_new(VIEW(view)->slideshow)));

	client = bonobo_object_client_from_corba(view->server);

	if (client == NULL) {
		gtk_object_unref(GTK_OBJECT(view));
		return FALSE;
	}

	if (!bonobo_client_site_bind_embeddable(view->client_site, client)) {
		gtk_object_unref(GTK_OBJECT(view));
		return FALSE;
	}

	cs_name = g_strdup_printf("Present%d", count);
	bonobo_item_container_add(win->container, cs_name,
				  BONOBO_OBJECT(view->client_site));
	g_free(cs_name);

	gtk_signal_connect(GTK_OBJECT(view->client_site), "system_exception",
			   GTK_SIGNAL_FUNC(client_site_sys_exception_cb), view);

	ACHTUNG_EXIT;

	return TRUE;
}

static void
view_frame_sys_exception_cb(BonoboViewFrame *view_frame, CORBA_Object cobject,
 			    CORBA_Environment *ev, PresentView *view)
{
	gtk_object_unref(GTK_OBJECT(view));
}

static gboolean
present_view_create_view_frame(PresentView *view, BonoboUIContainer *uic)
{
	BonoboViewFrame *view_frame;
	View *v;
	CORBA_Environment ev;

	g_return_val_if_fail(view, FALSE);

	ACHTUNG_ENTRY;

	v = VIEW(view);

	view_frame = bonobo_client_site_new_view(view->client_site,
			bonobo_object_corba_objref(BONOBO_OBJECT(uic)));

	g_return_val_if_fail(view_frame, FALSE);

	gtk_signal_connect(GTK_OBJECT(view_frame), "system_exception",
			   view_frame_sys_exception_cb, view);

	view->slide_wrapper = bonobo_view_frame_get_wrapper(view_frame);
printf("pres wrapper=%p\n", view->slide_wrapper);
	gtk_widget_show_all(view->slide_wrapper);

	view->corba_view = bonobo_view_frame_get_view(view_frame);

	CORBA_exception_init(&ev);

	GNOME_Achtung_Slide_setPresentMode(view->corba_view, &ev);

	ACHTUNG_EXIT;

	return TRUE;
}

static void
ebox_size_alloc(GtkWidget *box, GtkAllocation *alloc, PresentView *view)
{
	ACHTUNG_ENTRY;

	gtk_widget_size_allocate(view->slide_wrapper, alloc);

	ACHTUNG_EXIT;
}

PresentView *
present_view_new(Window *win, Slide *slide)
{
	gint width, height;
	PresentView *pview;
	View *view;

	g_return_val_if_fail(win, NULL);
	g_return_val_if_fail(slide, NULL);

	ACHTUNG_ENTRY;

	pview = gtk_type_new(present_view_get_type());
	view = VIEW(pview);

	view->slideshow = slide->slideshow;
	view->current_slide = slide;

	if(!present_view_embed_slide(pview, win))
		return NULL;

	if(!present_view_create_view_frame(pview, win->ui_container))
		return NULL;
 
	/* Create the big window */
	pview->big_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	width = gdk_screen_width();
	height = gdk_screen_height();

	pview->ebox = gtk_event_box_new();
	gtk_container_add(GTK_CONTAINER(pview->ebox), pview->slide_wrapper); 
	gtk_widget_show_all(pview->ebox);
	gtk_container_add(GTK_CONTAINER(pview->big_window), pview->ebox);

	gtk_widget_realize(pview->big_window);
	gdk_window_set_override_redirect(pview->big_window->window, TRUE);
	gdk_window_move_resize(pview->big_window->window, 0, 0, width, height);
	gtk_widget_show(pview->big_window);

	/* Event handler */
	gtk_signal_connect(GTK_OBJECT(pview->ebox), "button_press_event",
			   GTK_SIGNAL_FUNC(present_button_press), pview);
	gtk_signal_connect(GTK_OBJECT(pview), "destroy",
			   GTK_SIGNAL_FUNC(present_view_destroy), NULL);
	gtk_signal_connect(GTK_OBJECT(pview->ebox), "size_allocate",
			   GTK_SIGNAL_FUNC(ebox_size_alloc), view);

	ACHTUNG_EXIT;

	return pview;
} 

