/*
 * rectobject.c: Implementation of the rectangle object
 *
 * Copyright (c) 1999, 2000 Joe Shaw and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include "achtung.h"
#include "achobject.h"
#include "reobject.h"
#include "rectobject.h"
#include "slide.h"
#include "slideshow.h"

static void rectobject_class_init(RectObjectClass *klass);
static void rectobject_init(GtkObject *object);

static REObjectClass *parent_class;

GtkType
rectobject_get_type(void)
{
	static GtkType rectobject_type = 0;

	if (!rectobject_type) {
		static const GtkTypeInfo rectobject_info = {
			"RectObject",
			sizeof(RectObject),
			sizeof(RectObjectClass),
			(GtkClassInitFunc) rectobject_class_init,
			(GtkObjectInitFunc) rectobject_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		rectobject_type = gtk_type_unique(reobject_get_type(),
						  &rectobject_info);
	}

	return rectobject_type;
} /* rectobject_get_type */

/* Class initialization function for RectObject */
static void
rectobject_class_init(RectObjectClass *klass)
{
	GtkObjectClass *object_class;
	AchObjectClass *achobject_class;
	REObjectClass *re_class;

	object_class = (GtkObjectClass *) klass;
	achobject_class = (AchObjectClass *) klass;
	re_class = (REObjectClass *) klass;
	
	parent_class = gtk_type_class(reobject_get_type());
} /* rectobject_class_init */

/* RectObject intialization function */
static void
rectobject_init(GtkObject *object)
{
	AchObject *aobj;
	REObject *robj;

	g_return_if_fail(object);

	aobj = ACHOBJECT(object);
	robj = REOBJECT(object);

	aobj->x1 = 0.0;
	aobj->y1 = 0.0;
	aobj->x2 = 0.0;
	aobj->y2 = 0.0;
	aobj->slide = NULL;

	robj->fill_color = 0xff;
	robj->outline_color = 0xff;
} /* rectobject_init */

AchObject *
rectobject_new(Slide *slide)
{
	RectObject *obj;

	g_return_val_if_fail(slide, NULL);
	g_return_val_if_fail(IS_SLIDE(slide), NULL);
	
	obj = gtk_type_new(rectobject_get_type());

	ACHOBJECT(obj)->slide = slide;
	if (slide->object_added)
		(* slide->object_added) (slide, ACHOBJECT(obj));
	REOBJECT(obj)->fill_color = slide->slideshow->defaults.fill_color;
	REOBJECT(obj)->outline_color = slide->slideshow->defaults.line_color;

	achobject_changed(ACHOBJECT(obj));

	return ACHOBJECT(obj);
} /* rectobject_new */
