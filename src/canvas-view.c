/*
 * canvas-view.c: Implementation of the CanvasView base class
 *
 * Copyright (c) 2000 Joe Shaw, Phil Schwan, Helix Code, Inc., the Puffin
 *                    Group, Inc., and Linuxcare, Inc.
 *                  
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@linuxcare.com>
 *
 */
#include <config.h>
#include <gnome.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>

#include "achtung.h"
#include "achobject.h"
#include "background.h"
#include "canvas-view.h"
#include "slide.h"
#include "slideshow.h"
#include "view.h"

static void canvas_view_class_init(CanvasViewClass *klass);
static void canvas_view_init(GtkObject *object);
static void canvas_view_destroy(GtkObject *object);

/* Callbacks */
#if 0
static GtkWidget *canvas_view_activate(View *view);
static void canvas_view_deactivate(View *view);
static void canvas_view_got_focus(View *view);
static void canvas_view_lost_focus(View *view);
#endif

static ViewClass *parent_class;

GtkType
canvas_view_get_type(void)
{
	static GtkType canvas_view_type = 0;

	if (!canvas_view_type) {
		static const GtkTypeInfo canvas_view_info = {
			"CanvasView",
			sizeof(CanvasView),
			sizeof(CanvasViewClass),
			(GtkClassInitFunc) canvas_view_class_init,
			(GtkObjectInitFunc) canvas_view_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		canvas_view_type = gtk_type_unique(view_get_type(),
						   &canvas_view_info);
	}
	return canvas_view_type;
} /* canvas_view_get_type */

/* Class initialization function for CanvasView */
static void
canvas_view_class_init(CanvasViewClass *klass)
{
	GtkObjectClass *object_class;
	ViewClass *view_class;

	object_class = (GtkObjectClass *) klass;
	view_class = (ViewClass *) klass;
	
	parent_class = gtk_type_class(view_get_type());

	object_class->destroy = canvas_view_destroy;
} /* canvas_view_class_init */

/* CanvasView intialization function */
static void
canvas_view_init(GtkObject *object)
{
	CanvasView *canvas_view;
	View *view;

	g_return_if_fail(object);

	canvas_view = CANVAS_VIEW(object);
	view = VIEW(object);

	canvas_view->window = NULL;

	/* Create the canvas */
	gtk_widget_push_visual(gdk_rgb_get_visual());
	gtk_widget_push_colormap(gdk_rgb_get_cmap());
	canvas_view->canvas = GNOME_CANVAS(gnome_canvas_new());
	gtk_widget_pop_colormap();
	gtk_widget_pop_visual();
	gtk_widget_show(GTK_WIDGET(canvas_view->canvas));
	gnome_canvas_set_scroll_region(canvas_view->canvas, 0.0, 0.0,
				       gdk_screen_width(),
				       gdk_screen_height());
} /* canvaS_view_init */

CanvasView *
canvas_view_new(void)
{
	CanvasView *view;

	view = gtk_type_new(canvas_view_get_type());

	return view;
} /* canvas_view_new */

static void
canvas_view_destroy(GtkObject *object)
{
	CanvasView *view;

	g_return_if_fail(object);
	g_return_if_fail(IS_VIEW(object));

	view = CANVAS_VIEW(object);

	gtk_widget_destroy(GTK_WIDGET(view->canvas));

	view->canvas = NULL;

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy) (object);
} /* canvas_view_destroy */

void
canvas_view_set_size( CanvasView *view, gdouble width, gdouble height)
{
	g_return_if_fail(view);
	g_return_if_fail(view->canvas);

	gnome_canvas_set_scroll_region(view->canvas, 
				       -width / 2.0, -height / 2.0,
				       width / 2.0, height / 2.0);
}
