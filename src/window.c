/*
 * window.c: Implementation of the Window class
 *
 * Copyright (c) 2000 Phil Schwan and Linuxcare, Inc.
 *
 * Authors: Phil Schwan <pschwan@off.net>
 *	    Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include "achtung.h"
#include "achobject.h"
#include "file.h"
#include "menu.h"
#include "slide.h"
#include "view.h"
#include "window.h"

GList *open_windows = NULL;

static GtkObjectClass *parent_class;

static void
_window_destroy(GtkObject *object)
{
	g_return_if_fail(object);
	g_return_if_fail(IS_WINDOW(object));

	window_destroy(WINDOW(object));

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy) (object);
} /* _window_destroy */

/* Class initialization function for Window */
static void
window_class_init(WindowClass *klass)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class(gtk_object_get_type());

	object_class->destroy = _window_destroy;
} /* window_class_init */

/* Window intialization function */
static void
window_init(GtkObject *object)
{
	Window *win;

	g_return_if_fail(object);

	win = WINDOW(object);

	win->views = NULL;
	win->active_view = NULL;
} /* window_init */

GtkType
window_get_type(void)
{
	static GtkType window_type = 0;

	if (!window_type) {
		static const GtkTypeInfo window_info = {
			"Window",
			sizeof(Window),
			sizeof(WindowClass),
			(GtkClassInitFunc) window_class_init,
			(GtkObjectInitFunc) window_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		window_type = gtk_type_unique(gtk_object_get_type(),
					      &window_info);
	}

	return window_type;
} /* window_get_type */

static void
window_delete(GtkWidget *widget, GdkEvent *event, Window *win)
{
	View *view;

	ACHTUNG_ENTRY;

	g_return_if_fail(win);
	view = window_get_active_view(win);
	g_return_if_fail(view);

	(* view->deactivate)(view);
	gtk_object_unref(GTK_OBJECT(view->slideshow));
	window_destroy(win);

	ACHTUNG_EXIT;
} /* window_delete */

Window *
window_new(void)
{
	Window *win;

	ACHTUNG_ENTRY;

	win = gtk_type_new(window_get_type());

	win->window = BONOBO_WINDOW(bonobo_window_new("Achtung", "Achtung"));
	win->ui_container = bonobo_ui_container_new();
	bonobo_ui_container_set_win(win->ui_container, win->window);

	win->container = bonobo_item_container_new();

	gtk_window_set_policy(GTK_WINDOW(win->window), 1, 1, 0);
	gtk_window_set_default_size(GTK_WINDOW(win->window), 600, 400);
	gtk_signal_connect(GTK_OBJECT(win->window), "delete_event",
			   GTK_SIGNAL_FUNC(window_delete), win);

	/* Add the menus and toolbars */
	win->ui_component = window_ui_component_new(win);

	/* Disable most menu/toolbar items until a view is activated */
	window_ui_component_set_cmd_sensitivity(win, FALSE);

	gtk_widget_show_all(GTK_WIDGET(win->window));

	open_windows = g_list_append(open_windows, win);

	ACHTUNG_EXIT;

	return win;
} /* window_new */

void
window_destroy(Window *win)
{
	gtk_object_destroy(GTK_OBJECT(win->window));

	open_windows = g_list_remove(open_windows, win);

} /* window_destroy */

View *
window_get_active_view(Window *win)
{
	g_return_val_if_fail(win, NULL);

	return win->active_view;
} /* window_get_active_slideshow */

SlideShow *
window_get_active_slideshow(Window *win)
{
	View *view = window_get_active_view(win);
	g_return_val_if_fail(view, NULL);

	return view->slideshow;
} /* window_get_active_slideshow */

void
window_set_view(Window *win, View *view)
{
	GtkBox *box;
	GtkWidget *w;

	g_return_if_fail(win);
	g_return_if_fail(view);

	ACHTUNG_ENTRY;

	view->window = win;

	if (!win->priv.view_area) {

		/* Create the view_area container */
		box = GTK_BOX(win->priv.view_area = gtk_vbox_new(FALSE, 0));

		bonobo_window_set_contents(win->window, win->priv.view_area);
	} else {
		box = GTK_BOX(win->priv.view_area);
	}

	win->active_view = view;
	win->views = g_list_prepend(win->views, view);

	w = (* view->activate)(view);

	if (w) {
		gtk_box_pack_start(box, w, TRUE, TRUE, 0);
		window_sensitize_navigation_items(win);
	}

	(* view->got_focus) (view);
	gtk_widget_show_all(GTK_WIDGET(win->window));

	window_ui_component_set_cmd_sensitivity(win, TRUE);

	ACHTUNG_EXIT;

}

void
window_remove_view(Window *win, View *view)
{
	g_return_if_fail(win);
	g_return_if_fail(view);

	ACHTUNG_ENTRY;

	win->views = g_list_remove(win->views, view);
	
	g_return_if_fail(win->priv.view_area);

	if (view == win->active_view)
		win->active_view = NULL;

	(* view->lost_focus)(view);
	(* view->deactivate)(view);

	ACHTUNG_EXIT;
}

#if 0
/* FIXME: This is not super-optimal.  It would be much better if it didn't
 * destroy and recreate the panes, or at least resized them to what the user
 * had before.  This will do for now. */
void
window_add_view_panes(Window *win, View *view)
{
	GtkWidget *pane;
	GtkBox *box;

	g_return_if_fail(win);
	g_return_if_fail(view);

	ACHTUNG_ENTRY;

	view->window = win;

	if (!win->priv.view_area) {
		/* Create the view_area container */
		box = GTK_BOX(win->priv.view_area = gtk_vbox_new(FALSE, 0));

		bonobo_window_set_contents(win->priv.toplevel,
					   win->priv.view_area);
		gtk_box_pack_start(box,
				   (* view->activate)(view), TRUE, TRUE, 0);
		win->active_view = view;
		(* view->got_focus) (view);
	} else if (!win->priv.panes) {
		GtkWidget *widget;
		View *oldview;

		box = GTK_BOX(win->priv.view_area);
		g_return_if_fail(box);

		/* If this is just the 2nd slideshow, create vertical pane */
		pane = gtk_vpaned_new();

		/*** This is a hack, so what else is new ***/
		widget = box->children->data;
		g_return_if_fail(widget);
		widget = ((GtkBoxChild *)widget)->widget;

		/* ref/unref to prevent gtk_container_remove() from causing
		 * automatic widget cleanup */
		gtk_widget_ref(widget);
		gtk_container_remove(GTK_CONTAINER(box), widget);
		oldview = VIEW(win->views->data);
		gtk_paned_add1(GTK_PANED(pane),(* oldview->activate)(oldview));
		gtk_widget_unref(widget);

		gtk_paned_add2(GTK_PANED(pane), (* view->activate)(view));

		win->priv.panes = g_list_prepend(NULL, pane);

		gtk_box_pack_start(box, pane, TRUE, TRUE, 0);
	} else if (g_list_length(win->priv.panes) == 1) {
		/* 3rd slideshow, destroy old pane and do xemacs-like 3 pane */
		GtkWidget *oldpane = win->priv.panes->data;
		GtkWidget *pane2, *child;

		g_return_if_fail(oldpane);

		box = GTK_BOX(win->priv.view_area);
		g_return_if_fail(box);

		pane = gtk_hpaned_new();
		pane2 = gtk_vpaned_new();

		gtk_widget_ref((child = GTK_PANED(oldpane)->child1));
		gtk_container_remove(GTK_CONTAINER(oldpane), child);
		gtk_paned_add1(GTK_PANED(pane), child);
		gtk_paned_add2(GTK_PANED(pane), (* view->activate)(view));
		gtk_widget_unref(child);

		gtk_widget_ref((child = GTK_PANED(oldpane)->child2));
		gtk_container_remove(GTK_CONTAINER(oldpane), child);
		gtk_paned_add1(GTK_PANED(pane2), pane);
		gtk_paned_add2(GTK_PANED(pane2), child);
		gtk_widget_unref(child);

		g_list_free(win->priv.panes);
		win->priv.panes = g_list_prepend(NULL, pane);
		win->priv.panes = g_list_prepend(win->priv.panes, pane2);

		gtk_container_remove(GTK_CONTAINER(box), oldpane);
		gtk_box_pack_start(box, pane2, TRUE, TRUE, 0);
	} else if (g_list_length(win->priv.panes) == 2) {
		/* 4th slideshow, a quarter of the window for each */
		GtkWidget *oldpane = win->priv.panes->data;
		GtkWidget *pane2;

		g_return_if_fail(oldpane);

		pane = gtk_hpaned_new();
		gtk_paned_add1(GTK_PANED(pane), GTK_PANED(oldpane)->child1);
		gtk_paned_add2(GTK_PANED(pane), view->view_widget);
		pane2 = gtk_vpaned_new();
		gtk_paned_add1(GTK_PANED(pane2), pane);
		gtk_paned_add2(GTK_PANED(pane2), oldpane);

		g_list_free(win->priv.panes);
		win->priv.panes = g_list_prepend(NULL, pane);
		win->priv.panes = g_list_prepend(win->priv.panes, pane2);

		gtk_widget_destroy(oldpane);
	} else {
		/* FIXME */
		g_warning("Too many panes, leaking.  Sorry.\n");
	}

	win->views = g_list_prepend(win->views, view);

	gtk_widget_show_all(GTK_WIDGET(win->window));

	ACHTUNG_EXIT;
} /* window_add_view_panes */
#endif

void
window_set_statusbar(Window *win, const char *text)
{
	g_return_if_fail(win);

	g_warning("window_set_statusbar is a noop: %s", text);
	return;

	gnome_appbar_set_default(GNOME_APPBAR(win->priv.statusbar), text);
} /* window_set_statusbar */

void
window_dialog_reparent(Window *win, GnomeDialog *dlg)
{
	g_return_if_fail(win);
	g_return_if_fail(dlg);

	gnome_dialog_set_parent(GNOME_DIALOG(dlg),
				GTK_WINDOW(win->window));
} /* window_dialog_reparent */

gint
window_dialog_run(Window *win, GnomeDialog *dlg)
{
	g_return_val_if_fail(win, -1);
	g_return_val_if_fail(dlg, -1);

	gnome_dialog_set_parent(GNOME_DIALOG(dlg),
				GTK_WINDOW(win->window));

	return gnome_dialog_run(dlg);
} /* window_dialog_run */

void
window_sensitize_navigation_items(Window *win)
{
	View *view;
	Slide *slide;
	int index, length;
	gchar *text;
	BonoboUIComponent *uic = win->ui_component;

	g_return_if_fail(win);
	view = window_get_active_view(win);
	g_return_if_fail(view);
	slide = view->current_slide;
	g_return_if_fail(slide);
	g_return_if_fail(slide->slideshow);

	index = slideshow_get_index(slide->slideshow, slide);
	length = g_list_length(slide->slideshow->slides);

	if (index == -2)
		g_return_if_fail(NULL);

	if (index == -1) {
		/* Master slide */
		bonobo_ui_component_set_prop(uic, "/commands/SlidePrev", "sensitive", "0", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideNext", "sensitive", "0", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideInsertAfter", "sensitive", "0", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideInsertBefore", "sensitive", "0", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideAppend", "sensitive", "0", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideRemove", "sensitive", "0", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideFirst", "sensitive", "0", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideLast", "sensitive", "0", NULL);

		window_set_statusbar(win, "Master Slide");
		return;
	}

	bonobo_ui_component_set_prop(uic, "/commands/SlideInsertAfter", "sensitive", "1", NULL);
	bonobo_ui_component_set_prop(uic, "/commands/SlideInsertBefore", "sensitive", "1", NULL);
	bonobo_ui_component_set_prop(uic, "/commands/SlideAppend", "sensitive", "1", NULL);

	if(length > 1)
		bonobo_ui_component_set_prop(uic, "/commands/SlideRemove", "sensitive", "1", NULL);
	else
		bonobo_ui_component_set_prop(uic, "/commands/SlideRemove", "sensitive", "0", NULL);

	if (index == 0) {
		bonobo_ui_component_set_prop(uic, "/commands/SlidePrev", "sensitive", "0", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideFirst", "sensitive", "0", NULL);
	} else {
		bonobo_ui_component_set_prop(uic, "/commands/SlidePrev", "sensitive", "1", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideFirst", "sensitive", "1", NULL);
	}

	if (index == g_list_length(slide->slideshow->slides) - 1) {
		bonobo_ui_component_set_prop(uic, "/commands/SlideLast", "sensitive", "0", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideNext", "sensitive", "0", NULL);
	} else {
		bonobo_ui_component_set_prop(uic, "/commands/SlideLast", "sensitive", "1", NULL);
		bonobo_ui_component_set_prop(uic, "/commands/SlideNext", "sensitive", "1", NULL);
	}

	text = g_strdup_printf("Slide %d", index + 1);
	window_set_statusbar(win, text);

	/* For some reason, this crashes when I change views. But insure
	   reports it as a leak if I don't free it... */
	/* g_free(text); */
} 
