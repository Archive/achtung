/*
 * achtung-options.h: Application options related functions.
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __ACHTUNG_OPTIONS_H
#define __ACHTUNG_OPTIONS_H

typedef struct _AchtungOptions AchtungOptions;

#include "dialogs.h"

struct _AchtungOptions {
	gboolean vert_ruler;	/* Show vertical ruler */
	gboolean horz_ruler;	/* Show horizontal ruler */
	gint grid_gran;		/* Point granularity of the snap-to grid */
	gboolean inch_units;	/* Inch or cm units */
	dlg_options_t *dlg_info;
};

void achtung_load_options(void);
AchtungOptions* achtung_get_options(void);
void achtung_store_options(void);

#endif
