/*
 * xml-io.c: Dynamically loadable plugin for the Achtung file format (XML)
 *
 * Copyright (c) 1999, 2000 Phil Schwan, Joe Shaw, Mike Kestner, the Puffin
 *                          Group, Inc., Linuxcare, Inc., and Helix Code, Inc.
 *
 * Authors: Phil Schwan <pschwan@off.net>
 *          Joe Shaw <joe@off.net>
 *          Mike Kestner <mkestner@ameritech.com>
 *
 */

#include <sys/stat.h>
#include <unistd.h>

#include <glib.h>
#include <gnome.h> /* for I18n */

#include "file.h"
#include "plugin.h"
#include "xml-io.h"

static int
xml_can_unload(PluginData *pd)
{
	/* For now, we always unload */
	return TRUE;
} /* xml_can_unload */

static gboolean
xml_probe(const char *filename)
{
	struct stat statbuf;

	if (!filename || stat(filename, &statbuf) < 0)
		return FALSE;

	/* FIXME: add rudimentary XML-ness check? */

	return TRUE;
} /* xml_probe */

static void
xml_cleanup_plugin(PluginData *pd)
{
	file_format_unregister(xml_probe, achtung_xml_load_file,
			       achtung_xml_save_file);
} /* xml_cleanup_plugin */

int
init_plugin(PluginData *pd)
{
	g_return_val_if_fail(pd, -1);

	file_format_register(200, _("Achtung Presentation"), "xml", xml_probe,
			     achtung_xml_load_file, achtung_xml_save_file,
			     TRUE);

	pd->can_unload = xml_can_unload;
	pd->cleanup_plugin = xml_cleanup_plugin;
	pd->title = g_strdup(_("Achtung Presentation File Format module"));
	printf("XML plugin initialized\n");

	return 0;
} /* init_plugin */
