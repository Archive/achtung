/*
 * xml-load.c: Load routines for the Achtung file format (XML)
 *
 * Copyright (c) 1999, 2000 Phil Schwan, Joe Shaw, Mike Kestner, the Puffin
 *                          Group, Inc., Linuxcare, Inc., and Helix Code, Inc.
 *
 * Authors: Phil Schwan <pschwan@off.net>
 *          Joe Shaw <joe@off.net>
 *          Mike Kestner <mkestner@ameritech.com>
 *
 */

#include <glib.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

#include "achobject.h"
#include "imageobject.h"
#include "lineobject.h"
#include "ovalobject.h"
#include "reobject.h"
#include "rectobject.h"
#include "textobject.h"

#include "xml-io.h"

/* Almost unceremoniously ripped from gnumeric */
static char *
xml_get_value(xmlNode *node, const char *name)
{
	char *ret;
	xmlNode *child;

	ret = xmlGetProp(node, name);
	if (ret)
		return ret;

	for (child = node->childs; child; child = child->next) {
		if (g_strcasecmp(child->name, name) == 0) {
			ret = xmlNodeGetContent(child);
			if (ret)
				return ret;
		}
	}
	return NULL;
} /* xml_get_value */

static gboolean
get_color_param(xmlNode *item, gchar *pname, guint32 *color)
{
	gchar *buf;

	buf = xml_get_value(item, pname);

	if(buf) {
		sscanf(buf, "%u", color);
		g_free(buf);
		return(TRUE);
	}

	buf = g_strdup_printf("%s parameter missing from XML file\n", pname);
	g_warning(buf);
	g_free(buf);
	
	return(FALSE);
}

static gboolean
get_float_param(xmlNode *item, gchar *pname, gdouble *val)
{
	gchar *buf;

	buf = xml_get_value(item, pname);

	if(buf) {
		*val = atof(buf);
		g_free(buf);
		return(TRUE);
	}

	buf = g_strdup_printf("%s parameter missing from XML file\n", pname);
	g_warning(buf);
	g_free(buf);
	
	return(FALSE);
}

static void
_xml_load_ach_object(xmlNode *item, AchObject *obj)
{
	gdouble coord;

	if(get_float_param(item, "x1", &coord))
		gtk_object_set(GTK_OBJECT(obj), "x1", coord, NULL);
	if(get_float_param(item, "x2", &coord))
		gtk_object_set(GTK_OBJECT(obj), "x2", coord, NULL);
	if(get_float_param(item, "y1", &coord))
		gtk_object_set(GTK_OBJECT(obj), "y1", coord, NULL);
	if(get_float_param(item, "y2", &coord))
		gtk_object_set(GTK_OBJECT(obj), "y2", coord, NULL);
}

static void
_xml_load_re_object(xmlNode *item, Slide *slide, gboolean is_rect)
{
	AchObject *re;
	guint32 c;
	
	if (is_rect) 
		re = rectobject_new(slide);
	else
		re = ovalobject_new(slide);

	if (get_color_param(item, "fill_color", &c))
		gtk_object_set(GTK_OBJECT(re), "fill_color", c, NULL);

	if (get_color_param(item, "outline_color", &c))
		gtk_object_set(GTK_OBJECT(re), "outline_color", c, NULL);

	_xml_load_ach_object(item, re);
}

static void
_xml_load_line_object(xmlNode *item, Slide *slide)
{
	AchObject *line;
	guint32 c;
	gdouble w;

	line = lineobject_new(slide, FALSE);
	
	if (get_color_param(item, "color", &c))
		gtk_object_set(GTK_OBJECT(line), "color", c, NULL);

	if (get_float_param(item, "width", &w))
		gtk_object_set(GTK_OBJECT(line), "width", w, NULL);

	gtk_object_set(GTK_OBJECT(line), "arrow", FALSE, NULL);

	_xml_load_ach_object(item, line);
}

static void
_xml_load_arrow_object(xmlNode *item, Slide *slide)
{
	AchObject *line;
	guint32 c;
	gdouble w;

	line = lineobject_new(slide, TRUE);
	
	if (get_color_param(item, "color", &c))
		gtk_object_set(GTK_OBJECT(line), "color", c, NULL);

	if (get_float_param(item, "width", &w))
		gtk_object_set(GTK_OBJECT(line), "width", w, NULL);

	gtk_object_set(GTK_OBJECT(line), "arrow", TRUE, NULL);

	_xml_load_ach_object(item, line);
}

static void
_xml_load_image_object(xmlNode *item, Slide *slide)
{
	AchObject *obj;
	gchar *buf;
	GdkPixbuf *img;

	buf = xml_get_value(item, "filename");

	if (!buf) {
		g_warning("Filename value missing from XML file\n");
		return;
	}

	img = gdk_pixbuf_new_from_file(buf);

	g_free(buf);

	if (!img) {
		g_warning("Error loading image \"%s\"\n", buf);
		return;
	} 

	obj = imageobject_new(slide);
	gtk_object_set(GTK_OBJECT(obj), "filename", buf, "pixbuf", img, NULL);

	_xml_load_ach_object(item, obj);
}

static void
_xml_load_text_tag(xmlNode *node, GtkTextBuffer *buffer)
{
/*	GtkTextTag *tag;
	xmlNode *child;
	gchar *buf;

	buf = xmlNodeGetContent(node);

	tag = gtk_text_buffer_create_tag(buffer, buf);

	g_free(buf);
*/
}

static void
_xml_load_textline(xmlNode *line, GtkTextBuffer *buffer)
{
	gchar *text;
	xmlNode *child;

	for(child = line->childs; child; child = child->next) {

		if(!g_strcasecmp(child->name, "text")) {
			text = xml_get_value(child, "text");
			if(text) {
				gtk_text_buffer_insert_at_char(buffer, -1, 
							text, strlen(text));
				g_free(text);
			} else
				printf("Null text in line.\n");

		} else if(!g_strcasecmp(child->name, "tag")) {

			_xml_load_text_tag(child, buffer);

		} else

			g_warning("Unknown child node in Text line.\n");
	}
}

static void
_xml_load_text_object(xmlNode *item, Slide *slide)
{
	xmlNode *layout, *line;
	AchObject *obj;

	g_return_if_fail(item);

	obj = textobject_new(slide);

	layout = item->childs;

	if(g_strcasecmp(layout->name, "layout")) {
		g_warning("Unexpected Text Object Child node\n");
		return;
	}

	for(line = layout->childs; line; line = line->next)
		if(g_strcasecmp(line->name, "line"))
			g_warning("Non-line node in text layout node.\n");
		else 
			_xml_load_textline(line, TEXTOBJECT(obj)->buffer);

	_xml_load_ach_object(item, obj); 
}

static void
_xml_load_item(xmlNode *item, Slide *slide)
{
	char *objtype;
	gboolean is_rect;

	g_return_if_fail(item);
	g_return_if_fail(slide);

	objtype = (char *)item->name;

	if ((is_rect = !g_strcasecmp(objtype, "RECT")) || 
	    !g_strcasecmp(objtype, "OVAL")) 

		_xml_load_re_object(item, slide, is_rect);

	else if (!g_strcasecmp(objtype, "LINE")) 

		_xml_load_line_object(item, slide);

	else if (!g_strcasecmp(objtype, "ARROW")) 

		_xml_load_arrow_object(item, slide);

	else if (!g_strcasecmp(objtype, "IMAGE")) 

		_xml_load_image_object(item, slide);

	else if (!g_strcasecmp(objtype, "TEXT"))

		_xml_load_text_object(item, slide);

	else if (g_strcasecmp(objtype, "COMPONENT") == 0) {
		printf("Did you really think that BonObjects would load?\n");
		return;

	} else {
		g_warning("Invalid object type in XML file\n");
		return;
	}

} /* _xml_load_item */

static void
load_slide(Slide *slide, xmlNode *node)
{
	xmlNode *child;

	g_return_if_fail(node);

	for(child = node->childs; child; child = child->next) {

		if(!g_strcasecmp(child->name, "ITEMS")) {
			xmlNode *obj;

			/* Step through the objects */
			for (obj = child->childs; obj; obj = obj->next) {
				if (!child->childs)
					continue;
				_xml_load_item(obj, slide);
			}
		} else {
			gchar *buf;
			buf = g_strdup_printf("Bad Node: %s\n", child->name);
			g_warning(buf);
			g_free(buf);
		}
	}
}

static void
_xml_load_slides(SlideShow *ss, xmlNode *node)
{
	xmlNode *child;
	Slide *slide;

	g_return_if_fail(ss);
	g_return_if_fail(ss->slides);
	g_return_if_fail(node);
	g_return_if_fail(node->childs);

	child = node->childs;

	/* There's already a blank slide in every new slideshow. */
	slide = SLIDE(ss->slides->data);

	while(child) {

		load_slide(slide, child);

		if((child = child->next) != NULL) {
			slide = slide_new();
			slideshow_append(ss, slide);
		}
	}
}

static void
_xml_load_page_info(SlideShowPageInfo *pi, xmlNode *node)
{
	gchar *param;

	g_return_if_fail(pi);
	g_return_if_fail(node);

	param = xml_get_value(node, "starting_slide");
	if (param) {
		pi->start_page = atoi(param);
		g_free(param);
	} else
		g_warning("Starting Slide value missing from XML file\n");

	param = xml_get_value(node, "notes_orientation");
	if (param) {
		if (!strcmp(param, "Landscape"))
			pi->notes_portrait = FALSE;
		else if (!strcmp(param, "Portrait"))
			pi->notes_portrait = TRUE;
		else
			g_warning("Invalid Notes Orientation value\n");
		g_free(param);
	} else
		g_warning("Notes Orientation value missing from XML file\n");

	param = xml_get_value(node, "paper");
	if (param) {
		if (pi->paper)
			g_free(pi->paper);
		else
			g_warning("pi->paper NULL in xml_load_page_info\n");

		pi->paper = param;
	} else
		g_warning("Paper value missing from file\n");

	param = xml_get_value(node, "units");
	if (param) {
		if (!strcmp(param, "cm"))
			pi->inch_units = FALSE;
		else if (!strcmp(param, "inch"))
			pi->inch_units = TRUE;
		else
			g_warning("Invalid Units value in XML file\n");
		g_free(param);
	} else
		g_warning("Units value missing from XML file\n");

	if (!strcmp(pi->paper, "Custom")) {

		param = xml_get_value(node, "page_width");
		if (param) {
			pi->width = atof(param);
			g_free(param);
		} else
			g_warning("Page Width value missing from XML file\n");

		param = xml_get_value(node, "page_height");
		if (param) {
			pi->height = atof(param);
			g_free(param);
		} else
			g_warning("Page Height value missing from XML file\n");
	} else {
		param = xml_get_value(node, "slide_orientation");
		if (param) {
			if (!strcmp(param, "Landscape"))
				pi->slide_portrait = FALSE;
			else if (!strcmp(param, "Portrait"))
				pi->slide_portrait = TRUE;
			else
				g_warning("Invalid Slide Orientation value\n");
			g_free(param);
		} else
			g_warning("Slide Orientation value missing from XML file\n");
	}
} /* _xml_load_page_info */

static void
_xml_load_slideshow_defaults(SlideShow *ss, xmlNode *node)
{
	g_return_if_fail(ss);

	(void) get_color_param(node, "default_fill_color", 
			       &ss->defaults.fill_color);
	(void) get_color_param(node, "default_line_color", 
			       &ss->defaults.line_color);
	(void) get_color_param(node, "default_background_color", 
			       &ss->defaults.background_color);
	(void) get_color_param(node, "default_text_color", 
			       &ss->defaults.text_color);
}

gboolean
achtung_xml_load_file(SlideShow *ss, const char *filename)
{
	xmlDoc *doc;
	xmlNode *node;

	g_return_val_if_fail(ss, FALSE);

	doc = xmlParseFile(filename);
	if (!doc) {
		g_warning("Unable to load XML file.\n");
		return FALSE;
	}
	if (!doc->root) {
		g_warning("No data contained in XML file!\n");
		return FALSE;
	}
	if (g_strcasecmp(doc->root->name, "SLIDESHOW")) {
		g_warning("Not a valid Achtung XML file (no slideshow)\n");
		return FALSE;
	}

	ss->name = xml_get_value(doc->root, "title");

	/* Step through the root children */
	for (node = doc->root->childs; node; node = node->next) {

		if (!g_strcasecmp(node->name, "PAGE_INFO")) 

			_xml_load_page_info(&(ss->page_info), node);

		else if (!g_strcasecmp(node->name, "DEFAULTS"))

			_xml_load_slideshow_defaults(ss, node);

		else if (!g_strcasecmp(node->name, "MASTER_SLIDE"))

			load_slide(ss->master_slide, node);

		else if (!g_strcasecmp(node->name, "SLIDES"))

			_xml_load_slides(ss, node);

		else {
			gchar *buf;
			buf = g_strdup_printf("Bad Node: %s\n", node->name);
			g_warning(buf);
			g_free(buf);
		}
	}

	return TRUE;
} /* xml_load_file */

