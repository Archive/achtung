/*
 * xml-save.c: Save routines for the Achtung file format (XML)
 *
 * Copyright (c) 1999, 2000 Phil Schwan, Joe Shaw, Mike Kestner, the Puffin
 *                          Group, Inc., Linuxcare, Inc., and Helix Code, Inc.
 *
 * Authors: Phil Schwan <pschwan@off.net>
 *          Joe Shaw <joe@off.net>
 *          Mike Kestner <mkestner@ameritech.com>
 *
 */

#include <glib.h>
#include <gnome-xml/tree.h>
#include <gtktext/gtktext.h>
#include <gtktext/gtktextiter.h>
#include "embeddable-slide-view.h"

#include "achobject.h"
#include "imageobject.h"
#include "lineobject.h"
#include "ovalobject.h"
#include "reobject.h"
#include "rectobject.h"
#include "textobject.h"

#include "xml-io.h"

static void
_xml_set_prop(xmlNode *node, const xmlChar *name, const char *format, ...)
{
	gchar *buffer;
	va_list args;

	va_start(args, format);
	buffer = g_strdup_vprintf(format, args);
	va_end(args);
	xmlSetProp(node, name, buffer);

	g_free(buffer);
} /* _xml_set_prop */

static void
_xml_new_child(xmlNode *node, const xmlChar *name, const char *format, ...)
{
	gchar *buffer;
	va_list args;

	va_start(args, format);
	buffer = g_strdup_vprintf(format, args);
	va_end(args);
	xmlNewChild(node, NULL, name, buffer);

	g_free(buffer);
} /* _xml_set_prop */

/* This needs to be abstracted, but we might as well go to the new development
 * platform before doing all of that work. -phil */
static void
_xml_save_line(xmlNode *node, GtkTextBuffer *buffer, int line_num)
{
	gchar *text;
	GSList *tags, *t;

	g_return_if_fail(node);
	g_return_if_fail(buffer);

	text = gtk_text_buffer_get_text_from_line(buffer, line_num, 
						  0, -1, TRUE);
	if (text) {
		xmlNewChild(node, NULL, "text", text);
		g_free(text);
	} else
		printf("hmm, empty text.\n");

	/* Build a tag list */
	tags = gtk_text_buffer_get_tags_for_line(buffer, line_num);
	
	for (t = tags; t; t = t->next) {
		GtkTextTag *tag = t->data;
		xmlNode *tagnode;

		if (!tag) {
			g_warning("NULL tag in _xml_save_line\n");
			continue;
		}

		tagnode = xmlNewChild(node, NULL, "tag", tag->name);
		_xml_set_prop(tagnode, "priority", "%u", tag->priority);
		_xml_set_prop(tagnode, "affects_size",
			      "%u", tag->affects_size);

		if (tag->bg_color_set) {
			GdkColor *c;

			gtk_object_get(GTK_OBJECT(tag), "ARG_BACKGROUND_GDK",
				       &c, NULL);
			_xml_new_child(tagnode, "bg_color", "%u", c->pixel);
			g_free(c);
		}
		if (tag->fg_color_set) {
			GdkColor *c;

			gtk_object_get(GTK_OBJECT(tag), "ARG_FOREGROUND_GDK",
				       &c, NULL);
			_xml_new_child(tagnode, "fg_color", "%u", c->pixel);
			g_free(c);
		}
#if 0
		/* FIXME: requires tktext patch */
		if (tag->border_width_set) {
			
			_xml_new_child(tagnode, "border_width",
				       "%u", tag->values->border_width);
#endif
		/* missing: relief */
		/* missing: stipple */
		/* FIXME: font */
		/* missing: fg_stipple */
		if (tag->justify_set) {
			GtkJustification justify;

			gtk_object_get(GTK_OBJECT(tag), "justify", &justify,
				       NULL);
			if (justify == GTK_JUSTIFY_LEFT)
				xmlNewChild(tagnode, NULL, "justify", "left");
			else if (justify == GTK_JUSTIFY_RIGHT)
				xmlNewChild(tagnode, NULL, "justify", "right");
			else if (justify == GTK_JUSTIFY_CENTER)
				xmlNewChild(tagnode, NULL, "justify","center");
			else if (justify == GTK_JUSTIFY_FILL)
				xmlNewChild(tagnode, NULL, "justify", "fill");
			else
				g_warning("Invalid justification.\n");
		}
		if (tag->left_margin_set) {
			gint m;
			gtk_object_get(GTK_OBJECT(tag), "left_margin", &m,
				       NULL);
			_xml_new_child(tagnode, "left_margin", "%d", m);
		}
		if (tag->right_margin_set) {
			gint m;
			gtk_object_get(GTK_OBJECT(tag), "right_margin", &m,
				       NULL);
			_xml_new_child(tagnode, "right_margin", "%d", m);
		}
		/* missing intentionally: left_wrapped_line_margin */
		if (tag->offset_set) {
			gint offset;
			gtk_object_get(GTK_OBJECT(tag), "offset", &offset,
				       NULL);
			_xml_new_child(tagnode, "offset", "%d", offset);
		}
		if (tag->pixels_above_lines_set) {
			gint pixels;
			gtk_object_get(GTK_OBJECT(tag), "pixels_above_lines",
				       &pixels, NULL);
			_xml_new_child(tagnode, "pixels_above_lines",
				       "%d", pixels);
		}
		if (tag->pixels_below_lines_set) {
			gint pixels;
			gtk_object_get(GTK_OBJECT(tag), "pixels_below_lines",
				       &pixels, NULL);
			_xml_new_child(tagnode, "pixels_below_lines",
				       "%d", pixels);
		}
		/* missing: tab array */
		/* missing intentionally: pixels inside wrap */
		/* missing intentionally: wrap mode */
		if (tag->underline_set) {
			gboolean set;
			gtk_object_get(GTK_OBJECT(tag), "underline",
				       &set, NULL);
			_xml_new_child(tagnode, "underline", "%d", set);
		}
		if (tag->overstrike_set) {
			gboolean set;
			gtk_object_get(GTK_OBJECT(tag), "overstrike",
				       &set, NULL);
			_xml_new_child(tagnode, "overstrike", "%d", set);
		}
		/* missing: elide */
		if (tag->bg_full_height_set) {
			gboolean set;
			gtk_object_get(GTK_OBJECT(tag), "bg_full_height",
				       &set, NULL);
			_xml_new_child(tagnode, "bg_full_height", "%d", set);
		}
		/* missing intentionally: editable */
	}
}

static void
_xml_item_save(AchObject *obj, xmlNode *slide)
{
	xmlNode *object = NULL;

	g_return_if_fail(obj);
	g_return_if_fail(slide);

	if (IS_RECTOBJECT(obj)) { /* RECTANGLE */
		object = xmlNewChild(slide, NULL, "RECT", NULL);
		_xml_new_child(object, "fill_color", "%u",
			       REOBJECT(obj)->fill_color);
		_xml_new_child(object, "outline_color", "%u",
			       REOBJECT(obj)->outline_color);
	} else if (IS_OVALOBJECT(obj)) { /* ELLIPSE */
		object = xmlNewChild(slide, NULL, "OVAL", NULL);
		_xml_new_child(object, "fill_color", "%u",
			       REOBJECT(obj)->fill_color);
		_xml_new_child(object, "outline_color", "%u",
			       REOBJECT(obj)->outline_color);
	} else if (IS_IMAGEOBJECT(obj)) { /* IMAGE */
 		/* This will eventually have to either save the image files
		 * itself or at least reference their location. */
		object = xmlNewChild(slide, NULL, "IMAGE", NULL);
		xmlNewChild(object, NULL, "filename",
			    IMAGEOBJECT(obj)->filename);
	} else if (IS_TEXTOBJECT(obj)) { /* TEXT */
		xmlNode *layout, *line;
		guint lines, n;
		TextObject *tobj = TEXTOBJECT(obj);

		object = xmlNewChild(slide, NULL, "TEXT", NULL);
		layout = xmlNewChild(object, NULL, "layout", NULL);

		lines = gtk_text_buffer_get_line_count(tobj->buffer);
		for (n = 0; n < lines; n++) {
			line = xmlNewChild(layout, NULL, "line", NULL);
			_xml_save_line(line, tobj->buffer, n);
		}
	} else if (IS_LINEOBJECT(obj)) { /* LINE */
		object = xmlNewChild(slide, NULL, "LINE", NULL);
		_xml_new_child(object, "color", "%u",
			       LINEOBJECT(obj)->color);
		_xml_new_child(object, "width", "%u",
			       LINEOBJECT(obj)->width);
		_xml_new_child(object, "arrow", "%u",
			       LINEOBJECT(obj)->arrow);
	} else
		g_return_if_fail(NULL);

	/* coordinates are common to all objects */
	_xml_new_child(object, "x1", "%f", obj->x1);
	_xml_new_child(object, "y1", "%f", obj->y1);
	_xml_new_child(object, "x2", "%f", obj->x2);
	_xml_new_child(object, "y2", "%f", obj->y2);
} /* _xml_item_save */

static void
save_slide(Slide *slide, xmlNode *node)
{
	GList *l;
	xmlNode *child;

	g_return_if_fail(slide);
	g_return_if_fail(node);

	/* Slide index is implied by the order within the XML document.
	 * We don't need a field. -- phil */

	/* Furthermore, object ordering on the slide is implied by the
	 * order within ->contents.  We want to save them, however, in
	 * reverse order because object creation always puts the new
	 * item on top, and ->contents starts with the top item. */

	if(slide->contents) {
		child = xmlNewChild(node, NULL, "ITEMS", NULL);

		for (l = g_list_last(slide->contents); l; l = l->prev)
			_xml_item_save(ACHOBJECT(l->data), child);
	}
}	

static void
_xml_save_slides(GList *slides, xmlNode *node)
{
	xmlNode *child;

	for (; slides; slides = slides->next) {
		child = xmlNewChild(node, NULL, "SLIDE", NULL);
		save_slide(SLIDE(slides->data), child);
	}
}


static void
_xml_save_page_info(SlideShowPageInfo *pi, xmlNode *docnode)
{
	xmlNode *node;

	g_return_if_fail(pi);
	g_return_if_fail(docnode);

	node = xmlNewChild(docnode, NULL, "PAGE_INFO", NULL);

        xmlNewChild(node, NULL, "paper", pi->paper);

	_xml_new_child(node, "starting_slide", "%u", pi->start_page);

        if (pi->inch_units)
                xmlNewChild(node, NULL, "units", "inch");
        else
                xmlNewChild(node, NULL, "units", "cm");

        if (pi->notes_portrait)
                xmlNewChild(node, NULL, "notes_orientation", "Portrait");
	else
                xmlNewChild(node, NULL, "notes_orientation", "Landscape");

        if (!strcmp(pi->paper, "Custom")) {
		_xml_new_child(node, "page_height", "%u", pi->height);
		_xml_new_child(node, "page_width", "%u", pi->width);

        } else {
                if (pi->slide_portrait)
                        xmlNewChild(node, NULL, "slide_orientation", "Portrait");
                else
                        xmlNewChild(node, NULL, "slide_orientation", "Landscape");
        }
} /* _xml_save_page_info */

static void
_xml_save_slideshow_defaults(xmlNode *node, SlideShow *ss)
{
	xmlNode *defaults;

	defaults = xmlNewChild(node, NULL, "DEFAULTS", NULL);

	_xml_new_child(defaults, "default_fill_color",
		      "%u", ss->defaults.fill_color);
	_xml_new_child(defaults, "default_line_color",
		      "%u", ss->defaults.line_color);
	_xml_new_child(defaults, "default_background_color",
		      "%u", ss->defaults.background_color);
	_xml_new_child(defaults, "default_text_color",
		      "%u", ss->defaults.text_color);
}

static xmlDoc *
build_xml_tree(SlideShow *ss)
{
	xmlDoc *doc;
	xmlNode *slide;

	g_return_val_if_fail(ss, NULL);

	doc = xmlNewDoc("1.0");
	doc->root = xmlNewDocNode(doc, NULL, "SLIDESHOW", NULL);
	xmlSetProp(doc->root, "title", ss->name);

	_xml_save_slideshow_defaults(doc->root, ss);

	_xml_save_page_info(&(ss->page_info), doc->root);

	slide = xmlNewChild(doc->root, NULL, "MASTER_SLIDE", NULL);
	save_slide(ss->master_slide, slide);

	slide = xmlNewChild(doc->root, NULL, "SLIDES", NULL);
	_xml_save_slides(ss->slides, slide);

	return doc;
} /* build_xml_tree */

gboolean
achtung_xml_save_file(SlideShow *ss, const char *filename)
{
	xmlDoc *doc;

	g_return_val_if_fail(ss, FALSE);

	doc = build_xml_tree(ss);
	g_return_val_if_fail(doc, FALSE);

	/* xmlSetCompressMode(9); */
	xmlSaveFile(filename, doc);
	xmlFreeDoc(doc);

	return TRUE;
} /* xml_save_file */
