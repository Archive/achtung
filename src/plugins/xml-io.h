#ifndef __ACHTUNG_XML_IO_H
#define __ACHTUNG_XML_IO_H

#include <glib.h>

#include "slideshow.h"

gboolean achtung_xml_save_file(SlideShow *ss, const char *filename);
gboolean achtung_xml_load_file(SlideShow *ss, const char *filename);

#endif /* __ACHTUNG_XML_IO_H */
