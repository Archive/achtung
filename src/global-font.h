#ifndef __ACHTUNG_GLOBAL_FONT_H
#define __ACHTUNG_GLOBAL_FONT_H

#include <glib.h>

extern GList *global_font_list;
extern int global_font_sizes[];

void global_font_prepare_list(void);
int global_font_index(const char *font_name);

#endif /* __ACHTUNG_GLOBAL_FONT_H */
