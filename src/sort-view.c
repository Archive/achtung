/*
 * sort-view.c: Implements the slide sorting view.
 *
 * Copyright (c) 1999, 2000 Joe Shaw, Mike Kestner, and Helix Code, Inc.
 *
 * Author: Joe Shaw <joe@helixcode.com>
 * 	   Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <glib.h>
#include <gnome.h>
#include "achtung.h"
#include "slideshow.h"
#include "slide-view.h"
#include "sort-view.h"
#include "achobject.h"
#include "achtung-embeddable.h"

#define SLIDE_SIZE 100.0

typedef struct {
	GnomeCanvasGroup 	*group;
	GNOME_Achtung_Slide 	bview;
	BonoboViewFrame		*vframe;
	GtkWidget		*wrapper;
	gdouble			curr_x;
	gdouble			curr_y;
	gint			idx;
	SortView		*view;
} MiniSlide;

GtkTargetEntry te[] = {
	{"achtung/sort-view", 0, 0}
};

static void
get_minislide_dimensions(GNOME_Achtung_Slideshow server, 
			 gdouble *w, gdouble *h)
{
	CORBA_Environment ev;
	gdouble ratio;

	ACHTUNG_ENTRY;

	CORBA_exception_init(&ev);

	ratio = GNOME_Achtung_Slideshow_getAspectRatio(server, &ev);

	if(ratio >= 1.0) {
		*w = SLIDE_SIZE;
		*h = SLIDE_SIZE / ratio;
	} else {
		*w = SLIDE_SIZE / ratio;
		*h = SLIDE_SIZE;
	}

	ACHTUNG_EXIT;
}

static ViewClass *parent_class;

static void
sort_view_refresh(Slide *slide, SortView *view)
{
	View *v = VIEW(view);

	ACHTUNG_ENTRY;

	(* v->lost_focus) (v);
	(* v->got_focus) (v);

	ACHTUNG_EXIT;
}

static void
sort_view_remove_slide(Slide *slide, SortView *view)
{
	View *v;
	gint old_idx, new_idx = 0;

	g_return_if_fail(slide);
	g_return_if_fail(view);
	g_return_if_fail(slide->slideshow);

	ACHTUNG_ENTRY;

	v = VIEW(view);

	old_idx = slideshow_get_index(slide->slideshow, slide);

	if (old_idx < g_list_length(slide->slideshow->slides))
		new_idx = old_idx + 1;
	else if (old_idx > 0)
		new_idx = old_idx - 1;
	else
		g_return_if_fail(NULL);

	view->deleting = TRUE;
	sort_view_refresh(slide, view);	
	view->deleting = FALSE;
	v->current_slide = slideshow_get_slide(slide->slideshow, new_idx);

	ACHTUNG_EXIT;
}

static void
slide_added_cb(SlideShow *ss, Slide *slide, SortView *view)
{
	ACHTUNG_ENTRY;

	sort_view_refresh(slide, view);

	gtk_signal_connect(GTK_OBJECT(slide), "slide_moved",
			GTK_SIGNAL_FUNC(sort_view_refresh), view); 
	gtk_signal_connect(GTK_OBJECT(slide), "slide_removed",
			GTK_SIGNAL_FUNC(sort_view_remove_slide), view); 

	ACHTUNG_EXIT;
}

static void
sort_view_destroy(GtkObject *object)
{
	SortView *view;
	CORBA_Environment ev;
	GList *l;

	g_return_if_fail(object);
	g_return_if_fail(IS_SORT_VIEW(object));

	ACHTUNG_ENTRY;

	view = SORT_VIEW(object);

	CORBA_exception_init(&ev);

	for(l = view->minislides; l; l = l->next) {
		MiniSlide *ms = (MiniSlide *)l->data;
		Bonobo_Unknown_unref (ms->bview, &ev);
		bonobo_object_unref (BONOBO_OBJECT (ms->vframe));
		g_free(ms);
	}

	g_list_free(view->minislides);

	gtk_object_destroy(GTK_OBJECT(VIEW(view)->view_widget));

	Bonobo_Unknown_unref(view->server, &ev); 

	bonobo_object_unref (BONOBO_OBJECT (view->client_site));

	gtk_signal_disconnect_by_func(GTK_OBJECT(VIEW(view)->slideshow),
				GTK_SIGNAL_FUNC(slide_added_cb), view);

	for(l = VIEW(view)->slideshow->slides; l; l = l->next) {
		GtkObject *slide = GTK_OBJECT(l->data);
		gtk_signal_disconnect_by_func(slide,
				GTK_SIGNAL_FUNC(sort_view_remove_slide), view);
		gtk_signal_disconnect_by_func(slide,
				GTK_SIGNAL_FUNC(sort_view_refresh), view);
	}

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy) (object);

	ACHTUNG_EXIT;
} 

static void
sort_view_class_init(SortViewClass *klass)
{
	GtkObjectClass *view_class;

	ACHTUNG_ENTRY;

	view_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class(view_get_type());

	view_class->destroy = sort_view_destroy;

	ACHTUNG_EXIT;
} 

static GtkWidget*
sort_view_activate(View *view)
{
	ACHTUNG_ENTRY;

	g_return_val_if_fail(view, NULL);

	ACHTUNG_EXIT;

	return view->view_widget;
} 

static void
sort_view_deactivate(View *view)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	sort_view_destroy(GTK_OBJECT(view));

	ACHTUNG_EXIT;
} 

static gint
ms_event_cb(GnomeCanvasItem *item, GdkEventButton *event, MiniSlide *ms)
{
	ACHTUNG_ENTRY;


	switch(event->button) {

	case 1:

		view_set_slide_idx(VIEW(ms->view), 
				   g_list_index(ms->view->minislides, ms));
		
		break;

	default:

		break;
	}

	ACHTUNG_EXIT;

	return FALSE;

}

static GnomeCanvasItem *
draw_target(MiniSlide *ms)
{
	gdouble width, height;
	GnomeCanvasPoints *pts;
	GnomeCanvasItem *item;

	ACHTUNG_ENTRY;

	get_minislide_dimensions(ms->view->server, &width, &height);

	pts = gnome_canvas_points_new(2);
	pts->coords[0] = 1.25 * width;
	pts->coords[1] = 0.0;
	pts->coords[2] = 1.25 * width;
	pts->coords[3] = height + 4.0;
	
	item = gnome_canvas_item_new(ms->group, 
				     gnome_canvas_line_get_type(),
				     "points", pts,
				     "fill_color", "black",
				     NULL);

	ms->view->target_idx = ms->idx;

	ACHTUNG_EXIT;

	return item;
}

static void
highlight_minislide(MiniSlide *ms)
{
	gdouble width, height;

	get_minislide_dimensions(ms->view->server, &width, &height);

	ms->view->highlight = gnome_canvas_item_new(ms->group, 
						 gnome_canvas_rect_get_type(),
						 "x1", 0.0, "y1", 0.0,
						 "x2", width + 4.0,
						 "y2", height + 4.0,
						 "fill_color", NULL,
						 "outline_color", "black",
						 "width_pixels", 3,
						 NULL);
}

static void 
drag_begin(GtkWidget *widget, GdkDragContext *ctx, MiniSlide *ms)
{	
	ms->view->target = draw_target(ms);
	if(ms->view->highlight) {
		gtk_object_destroy(GTK_OBJECT(ms->view->highlight));
		ms->view->highlight = NULL;
	} 
}

static void 
drag_end(GtkWidget *widget, GdkDragContext *ctx, MiniSlide *ms)
{
	gtk_object_destroy(GTK_OBJECT(ms->view->target));
	ms->view->target = NULL;
	ms->view->target_idx = -1;
}

static gboolean 
ms_drag_motion(GtkWidget *widget, GdkDragContext *ctx, int x, int y, 
	    int time, MiniSlide *ms)
{
	if(ms->idx != ms->view->target_idx) {
		gtk_object_destroy(GTK_OBJECT(ms->view->target));
		ms->view->target = draw_target(ms);
	}

	return TRUE;
}

static gboolean 
drag_drop(GtkWidget *widget, GdkDragContext *ctx, int x, int y, 
	  int time, MiniSlide *ms)
{
	return TRUE;
}

static void 
drag_data_get(GtkWidget *widget, GdkDragContext *ctx, 
	      GtkSelectionData *data, int info, int time, MiniSlide *ms)
{
	gtk_selection_data_set(data, data->target, 8, (const guchar *)ms,
			       sizeof(MiniSlide));
}

static void 
drag_data_received(GtkWidget *widget, GdkDragContext *ctx, int x, int y,
		   GtkSelectionData *data, int info, int time, MiniSlide *ms)
{
	MiniSlide *drop_ms = (MiniSlide *)data->data;
	gint idx;

	if((ms->idx == drop_ms->idx) || (ms->idx == drop_ms->idx - 1)) {
		highlight_minislide(ms);
		return;
	}

	if(drop_ms->idx < ms->idx) {
		idx = ms->idx;
	} else {
		idx = ms->idx + 1;
	}

	slideshow_move(VIEW(ms->view)->slideshow, drop_ms->idx, idx);
}

static void
view_frame_sys_exception_cb(BonoboViewFrame *view_frame, CORBA_Object cobject,
 			    CORBA_Environment *ev, SortView *view)
{
	gtk_object_unref(GTK_OBJECT(view));
}

static void
user_activate_request_cb(BonoboViewFrame *view_frame, SortView *view)
{
	View *v = VIEW(view);
	Window *win = v->window;
	SlideView *new_view;

	ACHTUNG_ENTRY;

	new_view = slide_view_new(win, v->current_slide);
	window_remove_view(win, v);
	window_set_view(win, VIEW(new_view));

	ACHTUNG_EXIT;
}

static BonoboViewFrame*
create_view_frame(SortView *view, BonoboUIContainer *uic)
{
	BonoboViewFrame *view_frame;

	g_return_val_if_fail(view, NULL);
	g_return_val_if_fail(uic, NULL);

	ACHTUNG_ENTRY;

	view_frame = bonobo_client_site_new_view(view->client_site,
			bonobo_object_corba_objref(BONOBO_OBJECT(uic)));

	if (view_frame) {
		gtk_signal_connect(GTK_OBJECT(view_frame), "system_exception",
			   GTK_SIGNAL_FUNC(view_frame_sys_exception_cb), view);
		gtk_signal_connect(GTK_OBJECT(view_frame), "user_activate",
			   GTK_SIGNAL_FUNC(user_activate_request_cb), view);
	}

	ACHTUNG_EXIT;

	return view_frame;
}

static MiniSlide*
create_minislide(SortView *view, BonoboUIContainer *uic, gint idx)
{
	GnomeCanvasItem *group, *item;
	GtkWidget *wrapper, *ebox;
	BonoboViewFrame *vf;
	MiniSlide *minislide;
	gdouble x, y, width, height;
	CORBA_Environment ev;
	GNOME_Achtung_Slide bv;
	gchar *num;

	ACHTUNG_ENTRY;

	get_minislide_dimensions(view->server, &width, &height);

	x = 1.5 * width * (idx % view->cols) + 0.5 * width;
	y = 1.5 * height * ((gint)(idx / view->cols)) + 0.5 * height;
		 
	vf = create_view_frame(view, uic);

	wrapper = bonobo_view_frame_get_wrapper(vf);

	ebox = gtk_event_box_new();

	gtk_container_add(GTK_CONTAINER(ebox), wrapper);

	gtk_widget_show_all(GTK_WIDGET(ebox));

	gtk_drag_source_set(ebox, GDK_BUTTON1_MASK, te, 1, GDK_ACTION_MOVE);

	gtk_drag_dest_set(ebox, GTK_DEST_DEFAULT_ALL, te, 1, GDK_ACTION_MOVE);

	CORBA_exception_init(&ev);
	bv = (GNOME_Achtung_Slide) Bonobo_Unknown_queryInterface(
					bonobo_view_frame_get_view(vf), 
					"IDL:GNOME/Achtung/Slide:1.0", &ev);
	if (bv == CORBA_OBJECT_NIL) {
		CORBA_exception_free(&ev);
		bonobo_object_unref(BONOBO_OBJECT(vf));
		return NULL;
	}
	GNOME_Achtung_Slide_setSlide(bv, idx, &ev);
	CORBA_exception_free(&ev);

	group = gnome_canvas_item_new(gnome_canvas_root(view->canvas), 
				      gnome_canvas_group_get_type(),
				      "x", x, "y", y, NULL);

	item = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group),
				     gnome_canvas_widget_get_type(),
				     "x", 2.0, "y", 2.0,
				     "widget", ebox,
				     "width", width, "height", height,
				     NULL);

	num = g_strdup_printf("%d", idx);
	item = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group),
				     gnome_canvas_text_get_type(),
				     "x", width + 2.0, "y", height + 6.0,
				     "anchor", GTK_ANCHOR_NORTH_EAST,
				     "text", num,
				     NULL);
	g_free(num);

	minislide = g_new0(MiniSlide, 1);

	minislide->group = GNOME_CANVAS_GROUP(group);
	minislide->bview = bv;
	minislide->vframe = vf;
	minislide->wrapper = ebox;
	minislide->curr_x = x;
	minislide->curr_y = y;
	minislide->idx = idx;
	minislide->view = view;

	gtk_signal_connect(GTK_OBJECT(ebox), "button_press_event",
			   GTK_SIGNAL_FUNC(ms_event_cb), minislide);
	gtk_signal_connect(GTK_OBJECT(ebox), "drag-data-get",
			   GTK_SIGNAL_FUNC(drag_data_get), minislide);
	gtk_signal_connect(GTK_OBJECT(ebox), "drag-begin",
			   GTK_SIGNAL_FUNC(drag_begin), minislide);
	gtk_signal_connect(GTK_OBJECT(ebox), "drag-end",
			   GTK_SIGNAL_FUNC(drag_end), minislide);
	gtk_signal_connect(GTK_OBJECT(ebox), "drag-motion",
			   GTK_SIGNAL_FUNC(ms_drag_motion), minislide);
	gtk_signal_connect(GTK_OBJECT(ebox), "drag-drop",
			   GTK_SIGNAL_FUNC(drag_drop), minislide);
	gtk_signal_connect(GTK_OBJECT(ebox), "drag-data-received",
			   GTK_SIGNAL_FUNC(drag_data_received), minislide);

	ACHTUNG_EXIT;

	return minislide;
}

static void
sort_view_got_focus(View *view)
{
	gint idx;
	MiniSlide *ms;
	SortView *sview;
	GList *sl, *ml;
	CORBA_Environment ev;

	g_return_if_fail(view);
	sview = SORT_VIEW(view);
	g_return_if_fail(view->current_slide);
	g_return_if_fail(view->slideshow);

	ACHTUNG_ENTRY;

	sl = view->slideshow->slides;
	ml = sview->minislides;
	idx = 0;

	CORBA_exception_init(&ev);

	while(ml) {

		ms = (MiniSlide *)ml->data;

		if(sl) {
			if(view->current_slide == SLIDE(sl->data)) {
				if(SORT_VIEW(view)->deleting) {
					sl = sl->next;
					idx++;
					continue;
				}
				highlight_minislide(ms);
			}
			GNOME_Achtung_Slide_setSlide(ms->bview, idx++, &ev);
			ml = ml->next;
			sl = sl->next;
		} else {
			Bonobo_Unknown_unref(ms->bview, &ev);
			bonobo_object_unref (BONOBO_OBJECT (ms->vframe));
			gtk_object_destroy(GTK_OBJECT(ms->group));
			ml = ml->next;
			sview->minislides = g_list_remove(sview->minislides, ms);
			g_free(ms);
		}
	}

	while(sl) {
		ms = create_minislide(sview, view->window->ui_container, idx++);
		sview->minislides = g_list_append(sview->minislides, ms);
		if(view->current_slide == SLIDE(sl->data))
			highlight_minislide(ms);
		sl = sl->next;
	}

	/* FIXME: Add canvas scrolling for slides out of the viewport */

	ACHTUNG_EXIT;
} 

static void
sort_view_lost_focus(View *view)
{
	SortView *sview;

	g_return_if_fail(view);
	sview = SORT_VIEW(view);

	ACHTUNG_ENTRY;

	if (sview->highlight) {
		gtk_object_destroy(GTK_OBJECT(sview->highlight));
		sview->highlight = NULL;
	}

	ACHTUNG_EXIT;
} 

static void
sort_view_init(GtkObject *object)
{
	SortView *sort_view;
	View *view;

	g_return_if_fail(object);

	ACHTUNG_ENTRY;

	sort_view = SORT_VIEW(object);
	view = VIEW(object);

	sort_view->client_site = NULL;
	sort_view->server = NULL;
	sort_view->minislides = NULL;
	sort_view->target = NULL;
	sort_view->highlight = NULL;
	sort_view->canvas = NULL;
	sort_view->cols = 3;
	sort_view->deleting = FALSE;

	view->activate = sort_view_activate;
	view->deactivate = sort_view_deactivate;
	view->got_focus = sort_view_got_focus;
	view->lost_focus = sort_view_lost_focus;

	ACHTUNG_EXIT;
} 

GtkType
sort_view_get_type(void)
{
	static GtkType sort_view_type = 0;

	ACHTUNG_ENTRY;

	if (!sort_view_type) {
		static const GtkTypeInfo sort_view_info = {
			"SortView",
			sizeof(SortView),
			sizeof(SortViewClass),
			(GtkClassInitFunc) sort_view_class_init,
			(GtkObjectInitFunc) sort_view_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		sort_view_type = gtk_type_unique(view_get_type(),
						  &sort_view_info);
	}

	ACHTUNG_EXIT;

	return sort_view_type;
} 

static void
client_site_sys_exception_cb(BonoboObject *client, CORBA_Object cobject,
			     CORBA_Environment *ev, SortView *view)
{
	gtk_object_unref(GTK_OBJECT(view));
}

static gboolean
sort_view_embed_slideshow(SortView *view, Window *win)
{
	BonoboObjectClient *client;
	gchar *cs_name;
	static gint count = 0;

	g_return_val_if_fail(view, FALSE);
	g_return_val_if_fail(win, FALSE);

	ACHTUNG_ENTRY;

	view->client_site = bonobo_client_site_new(win->container);

	view->server = bonobo_object_corba_objref(BONOBO_OBJECT(
			achtung_embeddable_new(VIEW(view)->slideshow)));

	client = bonobo_object_client_from_corba(view->server);

	if(client == NULL) {
		gtk_object_unref(GTK_OBJECT(view));
		return FALSE;
	}

	if(!bonobo_client_site_bind_embeddable(view->client_site, client)) {
		gtk_object_unref(GTK_OBJECT(view));
		return FALSE;
	}

	cs_name = g_strdup_printf("Sort%d", count++);
	bonobo_item_container_add(win->container, cs_name,
				  BONOBO_OBJECT(view->client_site));
	g_free(cs_name);

	gtk_signal_connect(GTK_OBJECT(view->client_site), "system_exception",
			   GTK_SIGNAL_FUNC(client_site_sys_exception_cb), view);

	ACHTUNG_EXIT;

	return TRUE;
}

static GnomeCanvas*
create_canvas(void)
{
	GtkWidget *canvas;

	ACHTUNG_ENTRY;

	gtk_widget_push_visual(gdk_rgb_get_visual());
	gtk_widget_push_colormap(gdk_rgb_get_cmap());
	canvas = gnome_canvas_new();
	gtk_widget_pop_colormap();
	gtk_widget_pop_visual();
	gtk_widget_show(canvas);
	gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas), 0.0, 0.0,
				       gdk_screen_width(),
				       gdk_screen_height());

	ACHTUNG_EXIT;

	return GNOME_CANVAS(canvas);
}

static void
update_column_layout(SortView *view)
{
	gdouble width, height;
	GList *l;
	gint idx;

	ACHTUNG_ENTRY;

	get_minislide_dimensions(view->server, &width, &height);

	for (idx = 0, l = view->minislides; l; l = l->next, idx++) {
		gdouble x = width * (0.5 + 1.5 * (idx % view->cols));
		gdouble y = height * (0.5 + 1.5 * ((gint)(idx / view->cols)));
		MiniSlide *ms = (MiniSlide *)l->data;

		gnome_canvas_item_move(GNOME_CANVAS_ITEM(ms->group), 
				       x - ms->curr_x, y - ms->curr_y);
		ms->curr_x = x;
		ms->curr_y = y;
	}

	ACHTUNG_EXIT;
}

static void
size_alloc_cb(GtkWidget *w, GtkAllocation *alloc, SortView *view)
{
	gdouble width, height;
	gint cols, count;

	ACHTUNG_ENTRY;

	get_minislide_dimensions(view->server, &width, &height);

	cols = (gint) (((gdouble)alloc->width - 0.5 * width) / width / 1.5);

	if ((cols > 0) && (cols != view->cols)) {
		view->cols = cols;
		update_column_layout(view);
	} 

	count = g_list_length(view->minislides);

	gnome_canvas_set_scroll_region(view->canvas, 0, 0, gdk_screen_width(),
			(gint)((count / view->cols + 1) * 1.5 + 0.5) * height);


	ACHTUNG_EXIT;
}

SortView *
sort_view_new(Window *win, Slide *slide)
{
	SortView *sview;
	View *view;
	GtkWidget *sw;
	GList *l;

	ACHTUNG_ENTRY;

	g_return_val_if_fail(win, NULL);
	g_return_val_if_fail(slide, NULL);

	sview = gtk_type_new(sort_view_get_type());
	view = VIEW(sview);

	view->slideshow = slide->slideshow;
	view->current_slide = slide;

	if(!sort_view_embed_slideshow(sview, win))
		return NULL;

	sview->canvas = create_canvas();

	sw = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
				       GTK_POLICY_NEVER,
				       GTK_POLICY_AUTOMATIC);

	gtk_container_add(GTK_CONTAINER(sw), GTK_WIDGET(sview->canvas));

	gtk_signal_connect(GTK_OBJECT(sview), "destroy",
			   GTK_SIGNAL_FUNC(sort_view_destroy), NULL);

	gtk_signal_connect(GTK_OBJECT(sw), "size_allocate",
			   GTK_SIGNAL_FUNC(size_alloc_cb), sview);

	gtk_signal_connect(GTK_OBJECT(view->slideshow), "slide_added", 
			   GTK_SIGNAL_FUNC(slide_added_cb), sview);

	for(l = view->slideshow->slides; l; l = l->next) {
		Slide *s = SLIDE(l->data);
		gtk_signal_connect(GTK_OBJECT(s), "slide_removed",
			GTK_SIGNAL_FUNC(sort_view_remove_slide), sview);
		gtk_signal_connect(GTK_OBJECT(s), "slide_moved",
			GTK_SIGNAL_FUNC(sort_view_refresh), sview);
	}

	view->view_widget = sw;

	ACHTUNG_EXIT;

	return sview;
}


