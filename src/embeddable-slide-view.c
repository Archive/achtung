/*
 * embeddable-slide-view.c: Implementation of the SlideView class and the 
 * standard individual slide view.
 *
 * Copyright (c) 1999-2001 Joe Shaw, Phil Schwan, Mike Kestner, 
 *	Ximian, Inc., the Puffin Group, Inc., and Linuxcare, Inc.
 *
 * Authors: Joe Shaw <joe@helixcode.com>
 *          Phil Schwan <pschwan@off.net>
 *	    Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>

#include "idl/Achtung.h"
#include "achtung.h"
#include "achtung-options.h"
#include "achtung-util.h"
#include "background.h"
#include "menu.h"
#include "slide.h"
#include "slideshow.h"
#include "embeddable-slide-view.h"
#include "embeddable-slide-view-toolbar.h"
#include "slide-view-popup.h"

#define PARENT_TYPE BONOBO_X_OBJECT_TYPE

static void
update_embeddable_slide_view_background(Slide *slide, 
					EmbeddableSlideView *view)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	if (view->background)
		gtk_object_destroy(GTK_OBJECT(view->background));

	view->background = background_canvas_item(view,
					gnome_canvas_root(view->canvas));

	gtk_signal_connect(GTK_OBJECT(view->background), "event",
			   GTK_SIGNAL_FUNC(background_handle_event),
			   view);

	ACHTUNG_EXIT
} /* update_embeddable_slide_view_background */

static void
embeddable_slide_view_remove_slide(Slide *slide, EmbeddableSlideView *view)
{
	Slide *new_slide;
	gint idx;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	g_return_if_fail(slide);

	idx = slideshow_get_index(slide->slideshow, slide);

	if (idx < 0) {
		g_warning("Tried to Remove Master Slide!");
		return;
	}

	new_slide = slideshow_get_slide(slide->slideshow, idx + 1);

	if (!new_slide && idx > 0)
		new_slide = slideshow_get_slide(slide->slideshow, idx - 1);

	if (new_slide)
		embeddable_slide_view_set_slide(view, new_slide);

	ACHTUNG_EXIT;
}

static void
embeddable_slide_view_got_focus(EmbeddableSlideView *view)
{
	Slide *slide, *master_slide;
	GNOME_Draw_Shapes shapes;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	slide = view->slide;
	g_return_if_fail(slide);
	g_return_if_fail(slide->slideshow);
	master_slide = slide->slideshow->master_slide;

	if (view->background)
		gtk_object_destroy(GTK_OBJECT(view->background));
	view->background = background_canvas_item(view,
					gnome_canvas_root(view->canvas));
	gtk_signal_connect(GTK_OBJECT(view->background), "event",
			   GTK_SIGNAL_FUNC(background_handle_event), view);

	if (!view->master_layer) {
		shapes = bonobo_object_corba_objref(
				BONOBO_OBJECT(slide->shapes));
		view->master_layer = gdraw_shapes_layer_new(
				shapes, view->canvas, view->corba_uic);
	}

	if (view->activated && (slide == master_slide))
		gdraw_shapes_layer_activate(view->master_layer);
	else
		gdraw_shapes_layer_deactivate(view->master_layer);

	if (slide != master_slide) {
		shapes = bonobo_object_corba_objref(
				BONOBO_OBJECT(master_slide->shapes));
		view->shapes_layer = gdraw_shapes_layer_new(
				shapes, view->canvas, view->corba_uic);
		if (view->activated)
			gdraw_shapes_layer_activate(view->shapes_layer);
	}

	gtk_signal_connect(
		GTK_OBJECT(view->slide), "background_changed",
		GTK_SIGNAL_FUNC(update_embeddable_slide_view_background),
		view);
	gtk_signal_connect(
		GTK_OBJECT(view->slide), "slide_removed",
		GTK_SIGNAL_FUNC(embeddable_slide_view_remove_slide),
		view);

	ACHTUNG_EXIT;
} /* embeddable_slide_view_got_focus */

static void
embeddable_slide_view_lost_focus(EmbeddableSlideView *view)
{
	g_return_if_fail(view);

	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	if (view->shapes_layer) {
		gtk_object_destroy(GTK_OBJECT(view->shapes_layer));
		view->shapes_layer = NULL;
	}

	if (view->background) {
		gtk_object_destroy(GTK_OBJECT(view->background));
		view->background = NULL;
	}

	gtk_signal_disconnect_by_func(
		GTK_OBJECT(view->slide),
		GTK_SIGNAL_FUNC(update_embeddable_slide_view_background),
		view);
	gtk_signal_disconnect_by_func(
		GTK_OBJECT(view->slide),
		GTK_SIGNAL_FUNC(embeddable_slide_view_remove_slide),
		view);

	ACHTUNG_EXIT;
}

static void
embeddable_slide_view_destroy(GtkObject *object)
{
	EmbeddableSlideView *view;

	ACHTUNG_ENTRY;

	g_return_if_fail(object);
 	g_return_if_fail(IS_EMBEDDABLE_SLIDE_VIEW(object));

	view = EMBEDDABLE_SLIDE_VIEW(object);

	embeddable_slide_view_lost_focus(view);

	gtk_widget_destroy(view->table);

	if (view->master_layer)
		gtk_object_destroy(GTK_OBJECT(view->master_layer));

	if (view->shapes_layer)
		gtk_object_destroy(GTK_OBJECT(view->shapes_layer));

	if (view->activated)
		bonobo_object_unref(BONOBO_OBJECT(view->ui_component));

	bonobo_object_release_unref(view->corba_uic, NULL);

	ACHTUNG_EXIT;
} /* embeddable_slide_view_destroy */

static inline EmbeddableSlideView *
esv_from_servant(PortableServer_Servant servant)
{
	return EMBEDDABLE_SLIDE_VIEW(bonobo_object_from_servant(servant));
}	   
   
static void 
Slide_set_slide(PortableServer_Servant servant,
	        const CORBA_long slidenum,
	        CORBA_Environment * ev)
{
	EmbeddableSlideView *view = esv_from_servant(servant);
	Slide *slide = slideshow_get_slide(view->slide->slideshow, slidenum);

	ACHTUNG_ENTRY;

	if(view->slide == slide)
		return;

	if (slide)
		embeddable_slide_view_set_slide(view, slide);
	else
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION, 
				    ex_GNOME_Achtung_Slide_InvSlide, 
				    NULL);

	ACHTUNG_EXIT;
}

static void
Slide_set_present_mode(PortableServer_Servant serv, CORBA_Environment * ev)
{
	EmbeddableSlideView *view = esv_from_servant(serv);

	ACHTUNG_ENTRY;

	view->present_mode = TRUE;

	slide_background_changed(view->slide);

	ACHTUNG_EXIT;
}

static void
embeddable_slide_view_class_init(GtkObjectClass *klass)
{
	EmbeddableSlideViewClass *esvc = (EmbeddableSlideViewClass *) klass;
	POA_GNOME_Achtung_Slide__epv *epv = &esvc->epv;

	ACHTUNG_ENTRY;

	epv->setSlide = Slide_set_slide;
	epv->setPresentMode = Slide_set_present_mode;

	klass->destroy = embeddable_slide_view_destroy;

	ACHTUNG_EXIT;
} /* embeddable_slide_view_class_init */

/* EmbeddableSlideView intialization function */
static void
embeddable_slide_view_init(GtkObject *object)
{
	EmbeddableSlideView *embeddable_slide_view;

	ACHTUNG_ENTRY;

	g_return_if_fail(object);

	embeddable_slide_view = EMBEDDABLE_SLIDE_VIEW(object);

	embeddable_slide_view->slide = NULL;
	embeddable_slide_view->table = NULL;
	embeddable_slide_view->canvas = NULL;
	embeddable_slide_view->hscroll = NULL;
	embeddable_slide_view->vscroll = NULL;
	embeddable_slide_view->hruler = NULL;
	embeddable_slide_view->vruler = NULL;
	embeddable_slide_view->background = NULL;
	embeddable_slide_view->zoom_factor = 100;
	embeddable_slide_view->auto_zoom = TRUE;
	embeddable_slide_view->activated = FALSE;
	embeddable_slide_view->present_mode = FALSE;
	embeddable_slide_view->shapes_layer = NULL;
	embeddable_slide_view->master_layer = NULL;
	embeddable_slide_view->bview = NULL;
	embeddable_slide_view->container = NULL;
	embeddable_slide_view->ui_component = NULL;
	embeddable_slide_view->corba_uic = CORBA_OBJECT_NIL;

	ACHTUNG_EXIT;
} /* embeddable_slide_view_init */

BONOBO_X_TYPE_FUNC_FULL(EmbeddableSlideView, GNOME_Achtung_Slide,
			PARENT_TYPE, embeddable_slide_view);

static void
hadjustment_value_changed(GtkAdjustment *adj, EmbeddableSlideView *view)
{
	float zoom_divisor = view->zoom_factor / 100.0;
	float lower = adj->value / zoom_divisor;
	float upper = (adj->value + adj->page_size) / zoom_divisor;

	ACHTUNG_ENTRY;

	if (upper - lower > SCROLL_WIDTH) {
		float pad = ((upper - lower) - SCROLL_WIDTH) / 2.0;
		lower = lower - pad;
		upper = upper - pad;
	}

	/* FIXME this ruler range is broke. */
	if (view->hruler)
		gtk_ruler_set_range(GTK_RULER(view->hruler), lower, upper,
				    0.0, 100);

	ACHTUNG_EXIT;
} /* hadjustment_value_changed */

static void
vadjustment_value_changed(GtkAdjustment *adj, EmbeddableSlideView *view)
{
	float zoom_divisor = view->zoom_factor / 100.0;
	float lower = adj->value / zoom_divisor;
	float upper = (adj->value + adj->page_size) / zoom_divisor;

	ACHTUNG_ENTRY;

	if (upper - lower > SCROLL_HEIGHT) {
		float pad = ((upper - lower) - SCROLL_WIDTH) / 2.0;
		lower = lower - pad;
		upper = upper - pad;
	}

	/* FIXME this ruler range is broke. */
	if (view->vruler)
		gtk_ruler_set_range(GTK_RULER(view->vruler), lower, upper,
				    0.0, 100);

	ACHTUNG_EXIT;
} /* vadjustment_value_changed */

static void
update_rulers(GtkWidget *canvas, GdkEvent *event, EmbeddableSlideView *view)
{
	GtkRuler *hr, *vr;
	float zoom_divisor = view->zoom_factor / 100.0;

	g_return_if_fail(view);

	hr = GTK_RULER(view->hruler);
	vr = GTK_RULER(view->vruler);

	/* FIXME these ruler ranges are broke. */
	if (hr)
		gtk_object_set(GTK_OBJECT(hr),
			       "position",
			       hr->lower + (event->motion.x / zoom_divisor),
			       NULL);
	if (vr)
		gtk_object_set(GTK_OBJECT(vr),
			       "position",
			       vr->lower + (event->motion.y / zoom_divisor),
			       NULL);
} /* update_rulers */

static void
attach_rulers(EmbeddableSlideView *view)
{
	AchtungOptions *options;
	Slide *slide;
	SlideShowPageInfo *pi;
	GtkAdjustment *va, *ha;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	slide = view->slide;
	g_return_if_fail(IS_SLIDE(slide));
	g_return_if_fail(SLIDE(slide)->slideshow);

	ha = gtk_layout_get_hadjustment(GTK_LAYOUT(view->canvas));
	va = gtk_layout_get_vadjustment(GTK_LAYOUT(view->canvas));
	g_return_if_fail(ha);
	g_return_if_fail(va);

	options = achtung_get_options();
	pi = &(slide->slideshow->page_info);

	if (options->horz_ruler) {
		view->hruler = gtk_hruler_new();
		if (pi->inch_units) {
			/* FIXME these ruler ranges are broke. */
			gtk_ruler_set_metric(GTK_RULER(view->hruler), 
					     GTK_INCHES);
			gtk_ruler_set_range(GTK_RULER(view->hruler), ha->value,
				    	    ha->value + gdk_screen_width(), 
					    0.0, 100.0);
		} else {
			gtk_ruler_set_metric(GTK_RULER(view->hruler), 
					     GTK_CENTIMETERS);
			gtk_ruler_set_range(GTK_RULER(view->hruler), ha->value,
				    	    ha->value + gdk_screen_width(), 
					    0.0, 100.0);
		}
		gtk_table_attach(GTK_TABLE(view->table), view->hruler, 
				 1, 2, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
	}

	if (options->vert_ruler) {
		view->vruler = gtk_vruler_new();
		if (pi->inch_units) {
			gtk_ruler_set_metric(GTK_RULER(view->vruler), 
					     GTK_INCHES);
			gtk_ruler_set_range(GTK_RULER(view->vruler), va->value,
				    	    va->value + gdk_screen_width(), 
					    0.0, 100.0);
		} else {
			gtk_ruler_set_metric(GTK_RULER(view->vruler), 
					     GTK_CENTIMETERS);
			gtk_ruler_set_range(GTK_RULER(view->vruler), va->value,
				    	    va->value + gdk_screen_width(), 
					    0.0, 100.0);
		}
		gtk_table_attach(GTK_TABLE(view->table), view->vruler, 
				 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	}

	ACHTUNG_EXIT;
}

static void
attach_scrollbars(EmbeddableSlideView *view)
{
	GtkAdjustment *va, *ha;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	g_return_if_fail(view->table);

	ha = gtk_layout_get_hadjustment(GTK_LAYOUT(view->canvas));
	va = gtk_layout_get_vadjustment(GTK_LAYOUT(view->canvas));

	g_return_if_fail(ha);
	g_return_if_fail(va);

	/* FIXME: I don't think that I should have to do this */
	ha->step_increment = 10;
	va->step_increment = 10;

	view->hscroll = gtk_hscrollbar_new(ha);
	view->vscroll = gtk_vscrollbar_new(va);

	gtk_signal_connect(GTK_OBJECT(ha), "value_changed",
			   GTK_SIGNAL_FUNC(hadjustment_value_changed),
			   view);
	gtk_signal_connect(GTK_OBJECT(ha), "changed",
			   GTK_SIGNAL_FUNC(hadjustment_value_changed),
			   view);
	gtk_signal_connect(GTK_OBJECT(va), "value_changed",
			   GTK_SIGNAL_FUNC(vadjustment_value_changed), 
			   view);
	gtk_signal_connect(GTK_OBJECT(va), "changed",
			   GTK_SIGNAL_FUNC(vadjustment_value_changed),
			   view);

	gtk_table_attach(GTK_TABLE(view->table), view->vscroll, 2, 3, 1, 2,
			 GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	gtk_table_attach(GTK_TABLE(view->table), view->hscroll, 1, 2, 2, 3,
			 GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

	ACHTUNG_EXIT;
}

static void
attach_canvas(EmbeddableSlideView *view, SlideShowPageInfo *pi)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(view);
	g_return_if_fail(view->table);

	if (view->canvas)
		gtk_object_unref(GTK_OBJECT(view->canvas));

	gtk_widget_push_visual(gdk_rgb_get_visual());
	gtk_widget_push_colormap(gdk_rgb_get_cmap());
	view->canvas = GNOME_CANVAS(gnome_canvas_new());
	gtk_widget_pop_colormap();
	gtk_widget_pop_visual();
	gtk_widget_show(GTK_WIDGET(view->canvas));

	gnome_canvas_set_scroll_region(view->canvas, 
				       - pi->pswidth, - pi->psheight,
				       pi->pswidth, pi->psheight);

	gtk_table_attach(GTK_TABLE(view->table), GTK_WIDGET(view->canvas),
			 1, 2, 1, 2, GTK_EXPAND | GTK_FILL,
			 GTK_EXPAND | GTK_FILL, 0, 0);

	ACHTUNG_EXIT;
}

void
embeddable_slide_view_set_slide(EmbeddableSlideView *view, Slide *slide)
{
	g_return_if_fail(view);
	g_return_if_fail(slide);

	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	embeddable_slide_view_lost_focus(view);
	view->slide = slide;
	embeddable_slide_view_got_focus(view);

	ACHTUNG_EXIT;
}

static void
set_full_page_zoom(EmbeddableSlideView *view)
{
	gint zoom;
	SlideShowPageInfo *pi;
	GtkAdjustment *ha, *va;
	gfloat x, y;
	GtkAllocation *alloc;

	g_return_if_fail(view);
	g_return_if_fail(view->canvas);
	alloc = &(GTK_WIDGET(view->canvas)->allocation);
	g_return_if_fail(view->slide);
	g_return_if_fail(view->slide->bg_info);
	pi = view->slide->bg_info->page_info;
	g_return_if_fail(pi);

	ACHTUNG_ENTRY;

	zoom = (gint) (alloc->width / (pi->pswidth + 10) * 100);

	if (((gint16)((pi->psheight + 10) * zoom / 100)) > alloc->height)
		zoom = (gint)(alloc->height / (pi->psheight + 10) * 100);

	if (zoom > 0) {
		ha = gtk_layout_get_hadjustment(GTK_LAYOUT(view->canvas));
		va = gtk_layout_get_vadjustment(GTK_LAYOUT(view->canvas));

		x = (ha->upper - (gfloat) alloc->width) / 2;
		y = (va->upper - (gfloat) alloc->height) / 2;

		gtk_adjustment_set_value(GTK_ADJUSTMENT(ha), x);
		gtk_adjustment_set_value(GTK_ADJUSTMENT(va), y);

		embeddable_slide_view_zoom(view, zoom);
	}

	ACHTUNG_EXIT;
}

static void
canvas_size_alloc(GtkWidget *canvas, GtkAllocation *alloc, 
		  EmbeddableSlideView *view)
{
	g_return_if_fail(view);

	ACHTUNG_ENTRY;

	if (view->auto_zoom)
		set_full_page_zoom(view);

	ACHTUNG_EXIT;
}

EmbeddableSlideView *
embeddable_slide_view_new(Slide *slide)
{
	EmbeddableSlideView *view;

	ACHTUNG_ENTRY;

	g_return_val_if_fail(slide, NULL);

	view = gtk_type_new(embeddable_slide_view_get_type());
	view->slide = slide;

	view->table = gtk_table_new(3, 3, FALSE);

	attach_canvas(view, &slide->slideshow->page_info);

	view->container = bonobo_item_container_new();

	gtk_signal_connect(GTK_OBJECT(view->canvas), "size_allocate",
			   GTK_SIGNAL_FUNC(canvas_size_alloc), view);

	gtk_widget_show_all(GTK_WIDGET(view->table));

	ACHTUNG_EXIT;

	return view;
} /* embeddable_slide_view_new */	

gboolean
background_handle_event(GnomeCanvasItem *background, GdkEvent *event,
			EmbeddableSlideView *view)
{
	g_return_val_if_fail(event, FALSE);
	g_return_val_if_fail(view, FALSE);

	if (event->type != GDK_BUTTON_PRESS)
		return FALSE;

#if 0
	if (event->button.button == 3) {
		popup_canvasroot_menu(event, view);
		return TRUE;
	}
#endif

	return FALSE;
} /* background_handle_event */

void
embeddable_slide_view_zoom(EmbeddableSlideView *view, int percent)
{
	GtkAdjustment *ha, *va;

	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	gnome_canvas_set_pixels_per_unit(view->canvas, ((float)percent)/100.0);
	view->zoom_factor = percent;

	ha = gtk_layout_get_hadjustment(GTK_LAYOUT(view->canvas));
	va = gtk_layout_get_vadjustment(GTK_LAYOUT(view->canvas));
	gtk_signal_emit_by_name(GTK_OBJECT(ha), "changed");
	gtk_signal_emit_by_name(GTK_OBJECT(va), "changed");

	ACHTUNG_EXIT;
} /* embeddable_slide_view_zoom */

void
embeddable_slide_view_auto_zoom(EmbeddableSlideView *view)
{
	set_full_page_zoom(view);
}

static void
view_activate(BonoboView *bview, gboolean activate, EmbeddableSlideView *view)
{
	ACHTUNG_ENTRY;

	g_return_if_fail(view);

	view->activated = activate;

	if (activate) {
		attach_rulers(view);
		attach_scrollbars(view);
		gtk_widget_set_usize(view->table, 200, 200);
		gtk_widget_show_all(view->table);

		gtk_signal_connect(GTK_OBJECT(view->canvas), 
				   "motion_notify_event",
				   GTK_SIGNAL_FUNC(update_rulers), view);

		gtk_widget_grab_focus(GTK_WIDGET(view->canvas));

		bonobo_view_activate_notify(bview, activate);

		embeddable_slide_view_create_ui_component(view);

		if (view->slide == view->slide->slideshow->master_slide)
			gdraw_shapes_layer_activate(view->master_layer);
		else
			gdraw_shapes_layer_activate(view->shapes_layer);

	} else {
		gtk_object_destroy(GTK_OBJECT(view->hscroll));
		view->hscroll = NULL;
		gtk_object_destroy(GTK_OBJECT(view->vscroll));
		view->vscroll = NULL;
  
                if(view->hruler) {
                        gtk_object_destroy(GTK_OBJECT(view->hruler));
			view->hruler = NULL;
		}
                if(view->vruler) {
                        gtk_object_destroy(GTK_OBJECT(view->vruler));
			view->vruler = NULL;
		}
  
                gtk_signal_disconnect_by_func(
			GTK_OBJECT(view->canvas),
			GTK_SIGNAL_FUNC(update_rulers), view);

		bonobo_object_unref(BONOBO_OBJECT(view->ui_component));
		view->ui_component = NULL;

		gdraw_shapes_layer_deactivate(view->master_layer);
		gdraw_shapes_layer_deactivate(view->shapes_layer);
        }

	ACHTUNG_EXIT;

} /* view_activate */

BonoboView *
embeddable_slide_view_factory(BonoboEmbeddable *embeddable,
			      Bonobo_ViewFrame view_frame, SlideShow *ss)
{
	EmbeddableSlideView *view;
	BonoboView *bview;

	ACHTUNG_ENTRY;

	view = embeddable_slide_view_new(SLIDE(ss->slides->data));

	g_return_val_if_fail(view, NULL);

	bview = bonobo_view_new(view->table);
	bonobo_view_set_view_frame(bview, view_frame);
	bonobo_view_set_embeddable(bview, embeddable);

	bonobo_object_add_interface(BONOBO_OBJECT(view), BONOBO_OBJECT(bview));

	view->bview = bview;
	view->corba_uic = bonobo_view_get_remote_ui_container(bview);

	gtk_signal_connect(GTK_OBJECT(view->bview), "activate",
			   GTK_SIGNAL_FUNC(view_activate), view);

	embeddable_slide_view_got_focus(view);

	gtk_widget_show_all(view->table);

	ACHTUNG_EXIT;

	return BONOBO_VIEW(bview);
}
