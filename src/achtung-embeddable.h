/*
 * achtung-embeddable.h: Interface for the Slideshow bonobo aggregation object
 *
 * Copyright (c) 2000-2001 Mike Kestner, Joe Shaw, and Ximian Inc
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *         Joe Shaw <joe@ximian.com>
 */

#ifndef __ACHTUNG_EMBEDDABLE_H
#define __ACHTUNG_EMBEDDABLE_H

#include <bonobo/bonobo-xobject.h>
#include "idl/Achtung.h"
#include "slideshow.h"

typedef struct _AchtungEmbeddable      AchtungEmbeddable;
typedef struct _AchtungEmbeddableClass AchtungEmbeddableClass;

#define ACHTUNG_EMBEDDABLE_TYPE        (achtung_embeddable_get_type())
#define ACHTUNG_EMBEDDABLE(o)          (GTK_CHECK_CAST((o), ACHTUNG_EMBEDDABLE_TYPE, AchtungEmbeddable))
#define ACHTUNG_EMBEDDABLE_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ACHTUNG_EMBEDDABLE_TYPE, AchtungEmbeddableClass))
#define IS_ACHTUNG_EMBEDDABLE(o)       (GTK_CHECK_TYPE((o), ACHTUNG_EMBEDDABLE_TYPE))
#define IS_ACHTUNG_EMBEDDABLE_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), ACHTUNG_EMBEDDABLE_TYPE))

struct _AchtungEmbeddable {
	BonoboXObject		base;

	BonoboEmbeddable 	*embeddable;
	SlideShow 		*slideshow;
};
	
struct _AchtungEmbeddableClass {
	BonoboXObjectClass parent_class;

	POA_GNOME_Achtung_Slideshow__epv epv;
};

GtkType achtung_embeddable_get_type(void);
AchtungEmbeddable *achtung_embeddable_new(SlideShow *ss);
void achtung_embeddable_factory_init(void);

#endif /* __ACHTUNG_EMBEDDABLE_H */
