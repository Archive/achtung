/*
 * achtung-options.c: Application options related functions.
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <gnome.h>
#include "achtung-options.h"

static AchtungOptions ach_options;

void 
achtung_load_options()
{
	gboolean was_default;
	gfloat gran;

	gnome_config_push_prefix("Achtung/Options/");

	ach_options.inch_units = gnome_config_get_bool_with_default(
					"InchUnits=True", &was_default);
	if (was_default)
		gnome_config_set_bool("InchUnits", TRUE);

	ach_options.vert_ruler = gnome_config_get_bool_with_default(
					"VerticalRuler=True", &was_default);
	if (was_default)
		gnome_config_set_bool("VerticalRuler", TRUE);

	ach_options.horz_ruler = gnome_config_get_bool_with_default(
					"HorizontalRuler=True", &was_default);
	if (was_default)
		gnome_config_set_bool("HorizontalRuler", TRUE);

	gran = gnome_config_get_float_with_default(
					"GridGranularity=0.25", &was_default);
	if (was_default)
		gnome_config_set_float("GridGranularity", 0.25);

	if(ach_options.inch_units) {
		ach_options.grid_gran = (gint) (gran * 72);
	} else {
		ach_options.grid_gran = (gint) (gran * 72 / 2.54);
	}

	if(ach_options.grid_gran < 1)
		ach_options.grid_gran = 1;

	gnome_config_pop_prefix();

	ach_options.dlg_info = NULL;
}

AchtungOptions*
achtung_get_options()
{
	return &ach_options;
}

void
achtung_store_options()
{
	gnome_config_push_prefix("Achtung/Options/");

	gnome_config_set_bool("InchUnits", ach_options.inch_units);

	gnome_config_set_bool("VerticalRuler", ach_options.vert_ruler);

	gnome_config_set_bool("HorizontalRuler", ach_options.horz_ruler);

	if(ach_options.inch_units) {
		gnome_config_set_float("GridGranularity", 
					ach_options.grid_gran / 72.0);
	} else {
		gnome_config_set_float("GridGranularity", 
					ach_options.grid_gran / 72.0 * 2.54);
	}

	gnome_config_pop_prefix();

	gnome_config_sync();
}
	
